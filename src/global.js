
const ENV = 'DEV' // Develop
// const ENV = 'LIVE' //Live

let servingURL
if (ENV === 'DEV') servingURL = 'http://13.127.196.89:5002'
else servingURL = 'http://13.127.196.89:5002'

export const serverURL = servingURL


// to remove white space on input field
export function removeSpaces (string) {
  if (string === undefined) return string
  else return string.split(' ').join('')
}

// State list of india.
export function stateDetails () {
  const stateDetails = [
    { 'code': 'AN', 'name': 'Andaman and Nicobar Islands' },
    { 'code': 'AP', 'name': 'Andhra Pradesh' },
    { 'code': 'AR', 'name': 'Arunachal Pradesh' },
    { 'code': 'AS', 'name': 'Assam' },
    { 'code': 'BR', 'name': 'Bihar' },
    { 'code': 'CG', 'name': 'Chandigarh' },
    { 'code': 'CH', 'name': 'Chhattisgarh' },
    { 'code': 'DH', 'name': 'Dadra and Nagar Haveli' },
    { 'code': 'DD', 'name': 'Daman and Diu' },
    { 'code': 'DL', 'name': 'Delhi' },
    { 'code': 'GA', 'name': 'Goa' },
    { 'code': 'GJ', 'name': 'Gujarat' },
    { 'code': 'HR', 'name': 'Haryana' },
    { 'code': 'HP', 'name': 'Himachal Pradesh' },
    { 'code': 'JK', 'name': 'Jammu and Kashmir' },
    { 'code': 'JH', 'name': 'Jharkhand' },
    { 'code': 'KA', 'name': 'Karnataka' },
    { 'code': 'KL', 'name': 'Kerala' },
    { 'code': 'LD', 'name': 'Lakshadweep' },
    { 'code': 'MP', 'name': 'Madhya Pradesh' },
    { 'code': 'MH', 'name': 'Maharashtra' },
    { 'code': 'MN', 'name': 'Manipur' },
    { 'code': 'ML', 'name': 'Meghalaya' },
    { 'code': 'MZ', 'name': 'Mizoram' },
    { 'code': 'NL', 'name': 'Nagaland' },
    { 'code': 'OR', 'name': 'Odisha' },
    { 'code': 'PY', 'name': 'Puducherry' },
    { 'code': 'PB', 'name': 'Punjab' },
    { 'code': 'RJ', 'name': 'Rajasthan' },
    { 'code': 'SK', 'name': 'Sikkim' },
    { 'code': 'TN', 'name': 'Tamil Nadu' },
    { 'code': 'TS', 'name': 'Telangana' },
    { 'code': 'TR', 'name': 'Tripura' },
    { 'code': 'UK', 'name': 'Uttarakhand' },
    { 'code': 'UP', 'name': 'Uttar Pradesh' },
    { 'code': 'WB', 'name': 'West Bengal' }
  ]
  return stateDetails
}

export function validateEmail (obj) {
  // Use commented regex if below uncommented not working.
  if (/^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/.test(obj)) return true
  else return false
}
