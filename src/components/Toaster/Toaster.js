import React, { Component } from 'react'
import './Toaster.css'

export class Toaster extends Component {
  render () {
    return (
      <div id='toaster-block'>
        <div className='toaster-left'>
          <img className='white-tick-img' src='../../assets/images/ic_tick-success.svg' alt='tick' />
        </div>
        <div className='toaster-right'>
          <p className='toaster-title'>{this.props.title} </p>
        </div>
      </div>
    )
  }
}

export default Toaster
