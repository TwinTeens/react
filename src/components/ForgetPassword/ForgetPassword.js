import React, { Component } from 'react'
import axios from 'axios'
import { Link, withRouter } from 'react-router-dom'
import { validateEmail, serverURL } from '../../global'

import './ForgetPassword.css'

class ForgetPassword extends Component {
  constructor (props) {
    super(props)
    this.state = {
			accountType: localStorage.getItem('avatarType'),
			errMsg: '',
			disablebtn: 'outer-btn',
			formFields: {
				username: ''
			}
    }
		this.handleInputChange = this.handleInputChange.bind(this)
		// this.handleEmptyFormCheck = this.handleEmptyFormCheck.bind(this)
		this.handleSubmit = this.handleSubmit.bind(this)
		this.handleForms = this.handleForms.bind(this)
	}

	handleForms (obj) { if (obj === 'signup') this.props.history.push('/signup') }

  // Input change event
  handleInputChange (e) {
		const { name, value } = e.target

		if(this.state.errMsg.length > 0) this.setState({ errMsg: '' })

		const formFields = this.state.formFields
		formFields[name] = value
		this.setState({ formFields })
	}

  // to check all fields are entered or not.
  // handleEmptyFormCheck () {
	// 	let valid = true
	// 	const formFields = this.state.formFields // this.state.formFields
	// 	Object.keys(formFields).forEach(key => {
	// 		let value = formFields[key]
	// 		if (value === null || value.length <= 0) {
	// 			valid = false
	// 			this.setState({ errMsg: '*Please enter your registered email address' })
	// 		}
	// 	})
  //   return valid
  // }	

	// to handle submit
	handleSubmit () {
		let { username } = this.state.formFields
		// let isFormValid = true

		this.setState({ disablebtn: 'outer-btn disablebtn' })

		if (username === '') {
			this.setState({ errMsg: '*Please enter your registered email address' })
			this.setState({ disablebtn: 'outer-btn' })
			return
		} else if (username !== '') {			
			// email validation
			if (!validateEmail(username)) {
				this.setState({ errMsg: '*Invalid email! Please enter valid email address' })
				this.setState({ disablebtn: 'outer-btn' })
				return
			}
		}

		// to get usernamd and password
		const reqBody = {
			email: this.state.formFields.username
		}
		const reqURL = serverURL + '/user/forgetpassword'

		axios.post(reqURL, reqBody).then(res => {
			let data = res.data
			if (data.status.toLowerCase() === 'success') {
				localStorage.setItem('isResetPwd', 'false')
				this.props.history.push('/thank-you')
			} else if (data.status.toLowerCase() === 'error') {
				this.setState({ errMsg: '*Please enter the registered email address' })
				this.setState({ disablebtn: 'outer-btn' })
			}
		})
	}

  render () {	
		let { username, password} = this.state
		return (
			<div id='forget-pwd-form'>
				{/* Header */}
				<div className='hdr-block img-home-bg' />
				<Link to='/'><div className='brand-logo' /></Link>
				
				{/* Main content */}
				<div className='login-container'>
					<div className='login-block'>
						<h1 className='login-title'>Forget your Password?</h1>
						{/* Login details block */}
						<div className='login-fields-block'>
							{/* Username */}
							<div className='input-container'>
								<div className='input-groups'>
									<label className='field-caption'>Email*</label>
									<div className='out-ex-size'>
										<input type='text' className='form-field' autoComplete='off' name='username' value={username} placeholder='Enter your email address' onChange={this.handleInputChange} maxLength='50' />
									</div>
								</div>
							</div>
						</div>
						
						{/* Error message */}
						{this.state.errMsg.length > 0 && <div className='cls-err-msg'>{this.state.errMsg}&nbsp;</div>}

						{/* Submit button */}
						<div className='form-submit-section'>
							<div className={this.state.disablebtn}>
								<div className='btn' onClick={this.handleSubmit}>Continue</div>
							</div>
						</div>

						{/* Back */}
						<div className='login-back'><a href='/login'>Back</a></div>
					</div>
				</div>
			</div>
    )
  }
}
export default withRouter(ForgetPassword)
