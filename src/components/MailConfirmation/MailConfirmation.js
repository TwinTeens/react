import React from 'react'
import { withRouter } from 'react-router-dom'
import axios from 'axios'
import { serverURL } from '../../global'
import './MailConfirmation.css'

class MailConfirmation extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      isMailSuccess: true
    }
    this.handleRedirect = this.handleRedirect.bind(this)
  }

  handleRedirect () {
    localStorage.clear()
    this.props.history.push('/')  
  }

  componentDidMount () {
    const reqURL = serverURL + '/user/confirm/' + this.props.match.params.id
    axios.get(reqURL).then(res => {
      console.log('data ', res.data)
      if (res.data.status.toLowerCase() === 'success') {
        this.setState({ isMailSuccess: true })
      } else  if (res.data.status.toLowerCase() === 'error') {
				this.setState({ errMsg: '*' + res.data.message })
			}
		})
  }

  render () {
    return (
      <div id='mail-confirmation-block'>
        {this.state.isMailSuccess  &&
          <div className='mail-confirmation-box'>
            <div className='ic-success' />
            <div className='mail-confirmation-succ-title'>Confirmation Success</div>
            <div className='mail-confirmation-succ-sub-title'>Thank you! Your account has been activated successfully. </div>
            <div className='mail-confirmation-go-back'>Please go back to <span onClick={this.handleRedirect}>Home</span> to login your account.</div>
          </div>
        }
      </div>
    )
  }
}

export default withRouter(MailConfirmation)
