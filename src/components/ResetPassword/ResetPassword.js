import React, { Component } from 'react'
import axios from 'axios'
import { Link, withRouter } from 'react-router-dom'
import { serverURL } from '../../global'

import './ResetPassword.css'

class ResetPassword extends Component {
  constructor (props) {
    super(props)
    this.state = {
			errMsg: '',
			disablebtn: 'outer-btn',
			accountType: localStorage.getItem('avatarType'),
			isFormValidationSuccess: true,
			formFields: {
				newPassword: '',
				confirmNewPassword: ''
			},
			formErrors: {
				newPassword: '',
				confirmNewPassword: ''
			}
    }
		this.handleInputFocusExit = this.handleInputFocusExit.bind(this)
		this.handleInputChange = this.handleInputChange.bind(this)
		this.handleEmptyFormCheck = this.handleEmptyFormCheck.bind(this)
		this.handleUpdate = this.handleUpdate.bind(this)
	}

	// Input focus exit
	handleInputFocusExit (e, objType) {
		const { name, value } = e.target
		// const dataType = e.target.getAttribute('data-type')

		this.setState({ disablebtn: 'outer-btn' })
		
		const formFields = this.state.formFields
		const formErrors = this.state.formErrors

		if (objType === 'NP' || objType === 'CNP') {
			if (!(/^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{8,16}$/.test(value))) {
				formErrors[name] = '*New password must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters.'
				this.setState({ isFormValidationSuccess: false })
			} else {
				if (formFields.newPassword.length >= 8 && formFields.confirmNewPassword.length >= 8 ) {
					if (formFields.newPassword !== formFields.confirmNewPassword) {
						formErrors[name] = '*New password and confirmation new password do not match!'
						this.setState({ isFormValidationSuccess: false })	
					} else this.setState({ isFormValidationSuccess: true })
				}
			}
		}
	}

  // Input change event
  handleInputChange (e) {
		const { name, value } = e.target

		if(this.state.errMsg.length > 0) this.setState({ errMsg: '' })

		const formFields = this.state.formFields
		const formErrors = this.state.formErrors
		formErrors[name] = '' // Error handling
		formFields[name] = value  // Form handling
		this.setState({ formFields, formErrors })
	}

  // to check all fields are entered or not.
  handleEmptyFormCheck () {
		let valid = true
		const formFields = this.state.formFields
		const errorFields = this.state.formErrors
		Object.keys(formFields).forEach(key => {
			let value = formFields[key]
			if (value === null || value.length <= 0) {
				errorFields[key] = 'error'
				valid = false
			}
		})
		this.setState({ errorFields })
    return valid
  }	

	// to handle submit
	handleUpdate () {
		let { newPassword } = this.state.formFields
		let isFormValid = true

		this.setState({ disablebtn: 'outer-btn disablebtn' })

		if (!this.handleEmptyFormCheck()) isFormValid = false

		if (isFormValid && this.state.isFormValidationSuccess) {

			// to get user details
			const reqBody = {
				'newPassword': newPassword
			}
			
			const reqURL = serverURL + '/user/password/change?e=' + this.props.match.params.id
			axios.post(reqURL, reqBody).then(res => {
				console.log('res', res)
				let data = res.data
				if (data.status.toLowerCase() === 'success') {
					localStorage.setItem('isResetPwd', 'true')
					this.props.history.push('/thank-you')
				}
			})
		} else this.setState({ disablebtn: 'outer-btn' })
	}

  render () {	
		let formFields = this.state.formFields
		let formErrors = this.state.formErrors
		return (
			<div id='reset-pwd-form'>
				{/* Header */}
				<div className='hdr-block img-home-bg' />
				<Link to='/'><div className='brand-logo' /></Link>
				
				{/* Main content */}
				<div className='reset-pwd-container'>
					<div className='reset-pwd-block'>
						{/* Change password details block */}
						<h1 className='reset-pwd-title'>Reset Password</h1>
						<div className='reset-pwd-fields-block'>
							{/* Current Password */}
							{/* <div className='input-container'>
								<div className='input-groups'>
									<label className='field-caption'>Current Password*</label>
									<div className='out-ex-size'>
										<input type='password' className={formErrors.currentPassword.length > 0 ? 'form-field error': 'form-field'} autoComplete='off' name='currentPassword' value={formFields.currentPassword} placeholder='Enter your current password' onChange={this.handleInputChange} onBlur={(e) => this.handleInputFocusExit(e, 'CP')} maxLength='20' />
										{formErrors.currentPassword.length > 0 && <div className='error-title'>{formErrors.currentPassword.length > 5 ? formErrors.currentPassword : '*Please enter current password'}</div>}																		
									</div>								
								</div>
							</div> */}
							{/* Password */}
							<div className='input-container'>
								<div className='input-groups'>
									<label className='field-caption'>New Password*</label>
									<div className='out-ex-size'>
										<input type='password' className={formErrors.newPassword.length > 0 ? 'form-field error': 'form-field'} autoComplete='off' name='newPassword' value={formFields.newPassword} placeholder='Enter your new password' onChange={this.handleInputChange} onBlur={(e) => this.handleInputFocusExit(e, 'NP')} maxLength='20' />
										{formErrors.newPassword.length > 0 && <div className='error-title'>{formErrors.newPassword.length > 5 ? formErrors.newPassword : '*Please enter new password'}</div>}																		
									</div>								
								</div>
							</div>
							{/* Confirm Password */}
							<div className='input-container'>
								<div className='input-groups'>
									<label className='field-caption'>Confirm New Password*</label>
									<div className='out-ex-size'>
										<input type='password' className={formErrors.confirmNewPassword.length > 0 ? 'form-field error': 'form-field'} autoComplete='off' name='confirmNewPassword' value={formFields.confirmNewPassword} placeholder='Enter your confirm new password' onChange={this.handleInputChange} onBlur={(e) => this.handleInputFocusExit(e, 'CNP')} maxLength='20' />
										{formErrors.confirmNewPassword.length > 0 && <div className='error-title'>{formErrors.confirmNewPassword.length > 5 ? formErrors.confirmNewPassword : '*Please enter confirm new password'}</div>}																											
									</div>								
								</div>
							</div>
						</div>
			
						{/* Error message */}
						{this.state.errMsg.length > 0 && <div className='cls-err-msg'>{this.state.errMsg}&nbsp;</div>}

						{/* Submit button */}
						<div className='form-submit-section'>
							<div className={this.state.disablebtn}>
								<div className='btn' onClick={this.handleUpdate}>Submit</div>
							</div>
						</div>

						{/* Back */}
						<div className='reset-pwd-back'><a href={'/' + localStorage.getItem('avatarType') + '/dashboard'}>Back</a></div>
					</div>
				</div>
			</div>
    )
  }
}
export default withRouter(ResetPassword)
