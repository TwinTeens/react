import React from 'react'
import { withRouter } from 'react-router-dom'
import axios from 'axios'
import moment from 'moment'
import Header from '../Header/Header'
import { toast } from 'react-toastify'
import Toaster from '../Toaster/Toaster'
import { serverURL } from '../../global'
import './BuyerDashboard.css'
import 'react-toastify/dist/ReactToastify.css'

class BuyerDashboard extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      accountType: localStorage.getItem('avatarType'),
      profileName: '',
      profileIcon: '',
      rfpDataList: []
      // data: [
      //   { itemName: 'Laptop', description: '15 Inch 1TB Laptop', quantity: '100', bidStDt: '20/04/2020 02:00 PM', bidClDt: '20/04/2020 02:00 PM', isExpired: false  },
      //   { itemName: 'Hard Disk', description: '15 Inch 1TB Laptop', quantity: '500', bidStDt: '20/04/2020 02:00 PM', bidClDt: '20/04/2020 02:00 PM', isExpired: true  },
      //   { itemName: 'Pendrive', description: '15 Inch 1TB Laptop', quantity: '500', bidStDt: '20/04/2020 02:00 PM', bidClDt: '20/04/2020 02:00 PM', isExpired: false  },
      //   { itemName: 'Pendrive', description: '15 Inch 1TB Laptop', quantity: '500', bidStDt: '20/04/2020 02:00 PM', bidClDt: '20/04/2020 02:00 PM', isExpired: false  },
      //   { itemName: 'Pendrive', description: '15 Inch 1TB Laptop', quantity: '500', bidStDt: '20/04/2020 02:00 PM', bidClDt: '20/04/2020 02:00 PM', isExpired: true  },
      //   { itemName: 'Pendrive', description: '15 Inch 1TB Laptop', quantity: '500', bidStDt: '20/04/2020 02:00 PM', bidClDt: '20/04/2020 02:00 PM', isExpired: false  },
      //   { itemName: 'Pendrive', description: '15 Inch 1TB Laptop', quantity: '500', bidStDt: '20/04/2020 02:00 PM', bidClDt: '20/04/2020 02:00 PM', isExpired: true  },
      //   { itemName: 'Pendrive', description: '15 Inch 1TB Laptop', quantity: '500', bidStDt: '20/04/2020 02:00 PM', bidClDt: '20/04/2020 02:00 PM', isExpired: false  },
      //   { itemName: 'Pendrive', description: '15 Inch 1TB Laptop', quantity: '500', bidStDt: '20/04/2020 02:00 PM', bidClDt: '20/04/2020 02:00 PM', isExpired: false  },
      //   { itemName: 'Pendrive', description: '15 Inch 1TB Laptop', quantity: '500', bidStDt: '20/04/2020 02:00 PM', bidClDt: '20/04/2020 02:00 PM', isExpired: false  },
      //   { itemName: 'Mouse', description: '15 Inch 1TB Laptop', quantity: '1000', bidStDt: '20/04/2020 02:00 PM', bidClDt: '20/04/2020 02:00 PM', isExpired: true  },
      // ]
    }
    this.handleSubmit = this.handleSubmit.bind(this)
    this.handleView = this.handleView.bind(this)
  }

  handleView (objRFPId) { this.props.history.push('/buyer/bid-info', { rfpId: objRFPId }) }

  handleSubmit (obj) { if (obj === 'RFP') this.props.history.push('rfp-form') }

  componentDidMount () {
    let userInfo = JSON.parse(localStorage.getItem('userInfo'))
    if (userInfo) {
      if (this.state.accountType !== 'buyer') this.props.history.push('/')
      else if (this.state.accountType === 'buyer') {
        this.setState({ profileName: userInfo.firstName + ' ' + userInfo.lastName })
        this.setState({ profileIcon: userInfo.firstName.charAt(0) + userInfo.lastName.charAt(0) })
      }
    } else this.props.history.push('/')

    // to show toaster after new RFP created.
    if (localStorage.getItem('isSuccess')) {
      localStorage.removeItem('isSuccess')
      toast.dismiss()
      toast(<Toaster title='RFP creation success' />)
    } else if (localStorage.getItem('isPwdSuccess')) {
      localStorage.removeItem('isPwdSuccess')
      toast.dismiss()
      toast(<Toaster title='Password Updated' />)
    }

		// to get RFP list
    const reqURL = serverURL + '/rfp/list'
    const options = {
      headers: { 
        'Access-Control-Allow-Origin': "*",
        'access_key': userInfo.access_key,
      }
    }
		axios.get(reqURL, options).then(res => {
			console.log('data ', res.data.data)
			let data = res.data.data
			if (data.length > 0) {
				const rfpDataList = data.map((value, sidx) => {
          this.state.rfpDataList.push(value)
          return { ...value, value }
        })
        this.setState({ rfpDataList })
			}
    })
  }

  render () {
    console.log('this.state.rfpDataList ', this.state.rfpDataList)
    return (
      <div id='bdb-container'>
        {/* Header */}
				<Header profileName={this.state.profileName} profileIcon={this.state.profileIcon} />

        {/* Main content */}
        <div className='main-container'>
          <div className='db-title'>Welcome to Buyer Dashboard</div>
          <div className='db-flx-box'>
            <div className='db-flx-left'>
              <div className='db-subtitle'>RFP History</div>
            </div>
            <div className='db-flx-right'>
              {/* Create RFP button */}
              <div className='btn-rfp-block'>
                <div className='outer-btn'>
                  <div className='btn' onClick={() => this.handleSubmit('RFP')}><span>+</span>&nbsp;Create RFP</div>
                </div>
              </div>
            </div>
          </div>
          {/* Table content */}
          <div className='db-tbl-block'>
            {/* Table header */}
            <div className='tbl-header-block'>
              <div className='tbl-head col-1'>Sl.No</div>
              <div className='tbl-head col-2'>Item Name</div>
              <div className='tbl-head col-3'>Description</div>
              <div className='tbl-head col-4'>Quantity</div>
              <div className='tbl-head col-5'>Bid Start Date</div>
              <div className='tbl-head col-6'>Bid Close Date</div>
              <div className='tbl-head col-7'>Action</div>
            </div>
            {/* Table body */}
            <div className='tbl-body-container'>
              {this.state.rfpDataList.length === 0
                ? <div className='tbl-empty-row'>RFP data not found. Please click on "+ Create RFP" button to create new proposal.</div>
                : this.state.rfpDataList.map((value, idx) => (
                  <div className='tbl-body-block' key={idx}>
                    <div className='tbl-body col-1'>{idx+1}</div>
                    <div className='tbl-body col-2'>{value.itemName}</div>
                    <div className='tbl-body col-3'>{value.itemDesc.length > 0 ? value.itemDesc : '-'}</div>
                    <div className='tbl-body col-4'>{value.quantity.length > 0 ? value.quantity : '-'}</div>
                    <div className='tbl-body col-5'>{moment(new Date((parseInt(value.bidStartDate) * 1000))).format('DD/MM/YYYY')}</div>
                    <div className='tbl-body col-6'>{moment(new Date((parseInt(value.bidEndDate) * 1000))).format('DD/MM/YYYY hh:mm A')}</div>
                    <div className='tbl-body col-7'>
                      {/* {!value.isExpiry ? ( */}
                        {/* Create RFP button */}
                      <div className='btn-rfp-block btn-action-box'>
                        <div className='outer-btn btn-action'>
                          <div className='btn btn-act-stl' onClick={() => this.handleView(value.rfp_id)}>View</div>
                        </div>
                      </div>
                      {/* ):(
                        <div className='btn-stl-dis'>Bidding Closed</div>
                      )} */}
                    </div>
                  </div>
                ))
              }
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default withRouter(BuyerDashboard)
