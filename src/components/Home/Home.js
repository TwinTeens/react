import React from 'react'
import { withRouter } from 'react-router-dom'
import 'react-responsive-carousel/lib/styles/carousel.min.css'
import { Carousel } from 'react-responsive-carousel'
import './Home.css'

class Home extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      isPaymentSuccess: false
    }
    this.handleLogin = this.handleLogin.bind(this)
  }

  handleLogin (objType) {
    if (objType === 'B') {
      this.props.history.push('/login')
      localStorage.setItem('avatarType', 'buyer')
    } else if (objType === 'V') {
      this.props.history.push('/login')
      localStorage.setItem('avatarType', 'vendor')
    } else if (objType === 'R') {
      this.props.history.push('/signup')
      localStorage.setItem('avatarType', 'register')
    }
  }

  componentDidMount () {
    let userInfo = JSON.parse(localStorage.getItem('userInfo'))
    if (userInfo) {
      if (localStorage.getItem('avatarType') === 'buyer') this.props.history.push('/buyer/dashboard')
      else if (localStorage.getItem('avatarType') === 'vendor') this.props.history.push('/vendor/dashboard')
      else this.props.history.push('/')
    }
  }

  render () {
    return (
      <div id='home-container'>
        {/* Carousel block */}
        <div className='carousel-block'>
          <Carousel autoPlay={true} infiniteLoop={true} transitionTime={1000} interval={5000} stopOnHover={false} showArrows={false} showThumbs={false} showIndicators={false} showStatus={false}>
            <div><img src='/assets/images/home-slide-1.jpg' alt='Slider logo' /></div>
            <div><img src='/assets/images/home-slide-2.jpg' alt='Slider logo' /></div>
          </Carousel>
        </div>
        {/* Main container */}
        <div className='top-toaster'>
          {/* lms logo */}
          <div className='brand-logo' />
          {/* login block */}
          <div className='login-block'>
            <div className='flx-block'>
              {/* lms logo */}
              <div className='lms-logo' />
              {/* admin login button */}
              <div className='login-section'>
                <div className='outer-btn'>
                  <div className='btn' onClick={() => this.handleLogin('B')}>Buyer Login</div>
                </div>
              </div>
              {/* member login button */}
              <div className='login-section'>
                <div className='outer-btn'>
                  <div className='btn' onClick={() => this.handleLogin('V')}>Vendor Login</div>
                </div>
              </div>
              {/* member register button */}
              <div className='login-section'>
                <div className='outer-btn'>
                  <div className='btn' onClick={() => this.handleLogin('R')}>SIGN UP</div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default withRouter(Home)
