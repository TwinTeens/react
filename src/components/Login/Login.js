import React, { Component } from 'react'
import axios from 'axios'
import { Link, withRouter } from 'react-router-dom'
import { validateEmail, serverURL } from '../../global'

import './Login.css'

class Login extends Component {
  constructor (props) {
    super(props)
    this.state = {
			accountType: localStorage.getItem('avatarType'),
			errMsg: '',
			disablebtn: 'outer-btn',
			formFields: {
				username: '',
				password: '',
			}
    }
		this.handleInputChange = this.handleInputChange.bind(this)
		this.handleEmptyFormCheck = this.handleEmptyFormCheck.bind(this)
		this.handleSubmit = this.handleSubmit.bind(this)
		this.handleForms = this.handleForms.bind(this)
	}

	handleForms (obj) { 
		if (obj === 'signup') this.props.history.push('/signup')
		else if (obj === 'forget-password') this.props.history.push('/forget-password')
	}

  // Input change event
  handleInputChange (e) {
		const { name, value } = e.target

		if(this.state.errMsg.length > 0) this.setState({ errMsg: '' })

		const formFields = this.state.formFields
		formFields[name] = value
		this.setState({ formFields })
	}

  // to check all fields are entered or not.
  handleEmptyFormCheck () {
		let valid = true
		const formFields = this.state.formFields // this.state.formFields
		Object.keys(formFields).forEach(key => {
			let value = formFields[key]
			if (value === null || value.length <= 0) {
				valid = false
				this.setState({ errMsg: '*Please enter username and password' })
			}
		})
    return valid
  }	

	// to handle submit
	handleSubmit () {
		let { username, password } = this.state.formFields

		this.setState({ disablebtn: 'outer-btn disablebtn' })

		if (username === '' && password === '') {
			this.setState({ errMsg: '*Please enter username and password' })
			this.setState({ disablebtn: 'outer-btn' })
			return
		} else {
			if (username !== '') {
				// email validation
				if (!validateEmail(username)) {
					this.setState({ errMsg: '*Invalid email! Please enter valid email address' })
					this.setState({ disablebtn: 'outer-btn' })
					return
				}
			} else {
				this.setState({ errMsg: '*Please enter username' })
				this.setState({ disablebtn: 'outer-btn' })
				return
			}

			if (password === '') {
				this.setState({ errMsg: '*Please enter password' })
				this.setState({ disablebtn: 'outer-btn' })
				return
			}
		}

		const reqURL = serverURL + '/user/login'
		const reqBody = {
			username: this.state.formFields.username,
			password: this.state.formFields.password,
			accountType: this.state.accountType
		}

		axios.post(reqURL, reqBody).then(res => {
			console.log('data ', res.data)
			if (res.data.status.toLowerCase() === 'success') {
				localStorage.setItem('userInfo', JSON.stringify(res.data.data))
				if (this.state.accountType === 'buyer') this.props.history.push('/buyer/dashboard')
				else this.props.history.push('/vendor/dashboard')
			} else  if (res.data.status.toLowerCase() === 'error') {
				this.setState({ errMsg: '*' + res.data.message })
				this.setState({ disablebtn: 'outer-btn' })
			}
		})
}

  render () {	
		let { username, password} = this.state
		return (
			<div id='login-form'>
				{/* Header */}
				<div className='hdr-block img-home-bg' />
				<Link to='/'><div className='brand-logo' /></Link>
				
				{/* Main content */}
				<div className='login-container'>
					<div className='login-block'>
						<h1 className='login-title'>{this.state.accountType} Login</h1>
						{/* Login details block */}
						<div className='login-fields-block'>
							{/* Username */}
							<div className='input-container'>
								<div className='input-groups'>
									<label className='field-caption'>Username*</label>
									<div className='out-ex-size'>
										<input type='text' className='form-field' autoComplete='off' name='username' value={username} placeholder='Enter your email address' onChange={this.handleInputChange} maxLength='50' />
									</div>
								</div>
							</div>
							{/* Password */}
							<div className='input-container'>
								<div className='input-groups'>
									<label className='field-caption'>Password*</label>
									<div className='out-ex-size'>
										<input type='password' className='form-field' autoComplete='off' name='password' value={password} placeholder='Enter your password' onChange={this.handleInputChange} maxLength='20' />
									</div>								
								</div>
							</div>
						</div>
						{/* Forget Password */}
						<div className='login-forget-pwd'><span onClick={() => this.handleForms('forget-password')}>Forget Password?</span></div>
						
						{/* Error message */}
						{this.state.errMsg.length > 0 && <div className='cls-err-msg'>{this.state.errMsg}&nbsp;</div>}

						{/* Submit button */}
						<div className='form-submit-section'>
							<div className={this.state.disablebtn}>
								<div className='btn' onClick={this.handleSubmit}>LOGIN</div>
							</div>
						</div>

						{/* Signup */}
						<div className='sign-up-block'>
							<div className='sign-up-title'>Don't have an account? <span onClick={() => this.handleForms('signup')}>Signup</span></div>
						</div>

						{/* Back */}
						<div className='login-back'><a href='/'>Back</a></div>
					</div>
				</div>
			</div>
    )
  }
}
export default withRouter(Login)
