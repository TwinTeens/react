import React, { Component } from 'react'
import { withRouter, Link } from 'react-router-dom'
import axios from 'axios'
import Datetime from 'react-datetime'
import 'react-datetime/css/react-datetime.css'
import moment from 'moment'
import { Multiselect } from 'multiselect-react-dropdown'
import { validateEmail, serverURL } from '../../global'
import Header from '../Header/Header'
import FileSaver from 'file-saver'

import './BuyerRFPDetails.css'

class BuyerRFPDetails extends Component {
	constructor(props) {
		super(props)
		this.state = {
			accountType: localStorage.getItem('avatarType'),
			isHide: true,
			isDisabled: true,
			errMsg: '',
			disablebtn: 'outer-btn',
			isFormValidationSuccess: true,
			// vendorsList: [
			// 	{ id: 1, name: 'HP-Mumbai', email: 'test@gmail.com', isChecked: '' },
			// 	{ id: 2, name: 'Apple-US', email: 'test1@gmail.com', isChecked: '' },
			// 	{ id: 3, name: 'INTEL-US', email: 'test1@gmail.com', isChecked: '' },
			// 	{ id: 4, name: 'Lenovo-Mumbai', email: 'test1@gmail.com', isChecked: '' },
			// 	{ id: 5, name: 'Dell-JAPAN', email: 'test1@gmail.com', isChecked: '' },
			// 	{ id: 6, name: 'HP-Mumbai', email: 'test@gmail.com', isChecked: '' },
			// 	{ id: 7, name: 'Apple-US', email: 'test1@gmail.com', isChecked: '' },
			// 	{ id: 8, name: 'INTEL-US', email: 'test1@gmail.com', isChecked: '' },
			// 	{ id: 9, name: 'Lenovo-Mumbai', email: 'test1@gmail.com', isChecked: '' },
			// 	{ id: 10, name: 'Dell-JAPAN', email: 'test1@gmail.com', isChecked: '' }
			// ],
			vendorsList: [],
			compareVendorsList: [],
			newVendorsList: [], // to get newly added vendors from input fields
			tempBidEndDate: '', // to get selected bid close date and time in IST format 'Tue Apr 14 2020 20:55:01 GMT+0530 (IST)'
			// to handle Form Fields
			formFields: {
				itemCode: '',
				itemName: '',
				HSNCode: '',
				UNSPSCCode: '',
				itemDesc: '',
				quantity: '',
				bidType: '',
				bidStartDate: '',
				bidEndDate: '',
				oldVendorsList: [],
				newVendorsList: [],
				shipAddress: '',
				shipCity: '',
				shipPincode: '',
				shipMobile: '',
				shipAltCont: ''
			},
			// to handle Form Errors
			formErrors: {
				itemCode: '',
				itemName: '',
				HSNCode: '',
				UNSPSCCode: '',
				itemDesc: '',
				quantity: '',
				bidType: '',
				bidEndDate: '',
				oldVendorsList: '',
				shipAddress: '',
				shipCity: '',
				shipPincode: '',
				shipMobile: '',
				shipAltCont: ''
			},
			isRFPExpired: false,
			rfpFile: ''
		}
		this.handleInputChange = this.handleInputChange.bind(this)
		this.handleInputFocusExit = this.handleInputFocusExit.bind(this)
		this.handleBidTypeOnChange = this.handleBidTypeOnChange.bind(this)
		this.handleDateChange = this.handleDateChange.bind(this)
		this.handleEmptyFormCheck = this.handleEmptyFormCheck.bind(this)
		this.handleNewVendorInputChange = this.handleNewVendorInputChange.bind(this)
		this.handleNewVendorOnBlurChange = this.handleNewVendorOnBlurChange.bind(this)
		this.handleAddNewVendor = this.handleAddNewVendor.bind(this)
		this.handleRemoveNewVendor = this.handleRemoveNewVendor.bind(this)
		this.onSelectVendorsFromList = this.onSelectVendorsFromList.bind(this)
		this.onRemoveVendorsFromList = this.onRemoveVendorsFromList.bind(this)
		this.handleSubmit = this.handleSubmit.bind(this)
		this.handleCheckBox = this.handleCheckBox.bind(this)
		this.handleButton = this.handleButton.bind(this)
		this.handleDownload = this.handleDownload.bind(this)
		this.handleExpiredRFPDownload = this.handleExpiredRFPDownload.bind(this)
	}

	handleExpiredRFPDownload (objVendorId, objFileName) {
		try {
			let rfpId = this.props.location.state.rfpId
			const reqBody = { 'rfp_id': rfpId, 'user_id': objVendorId }
			const reqURL = serverURL + '/bid/file/download'
			let userInfo = JSON.parse(localStorage.getItem('userInfo'))
		
			const options = {
				headers: { 
					'Access-Control-Allow-Origin': "*",
					'Content-Type': 'application/json',
					'access_key': userInfo.access_key
				},
				responseType: 'arraybuffer'
			}
			axios.post(reqURL, JSON.stringify(reqBody), options).then(res => {
				// console.log('response', res.data)
				let fileName = objFileName.split('.')
				if (fileName[1].toLowerCase() === 'pdf') { // PDF
					var blob = new Blob([res.data], { type: 'application/pdf' })
				} else if (fileName[1].toLowerCase() === 'xlsx') { // XLSX
					var blob = new Blob([res.data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', })
				}
				FileSaver.saveAs(blob, objFileName)
			})
		} catch (error) {
			console.log('try catch failure')
		}
	}

	// to download already uploaded document.
	handleDownload () {
		try {
			let rfpId = this.props.location.state.rfpId
			const reqBody = { 'rfp_id': rfpId }
			const reqURL = serverURL + '/rfp/file/download'
			let userInfo = JSON.parse(localStorage.getItem('userInfo'))
		
			const options = {
				headers: {
					'Access-Control-Allow-Origin': "*",
					'Content-Type': 'application/json',
					'access_key': userInfo.access_key
				},
				responseType: 'arraybuffer'
			}
			axios.post(reqURL, JSON.stringify(reqBody), options).then(res => {
				// console.log('response', res.data)
				let fileName = this.state.rfpFile.split('.')
				if (fileName[1].toLowerCase() === 'pdf') { // PDF
					var blob = new Blob([res.data], { type: 'application/pdf' })
				} else if (fileName[1].toLowerCase() === 'xlsx') { // XLSX
					var blob = new Blob([res.data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' })
				}
				FileSaver.saveAs(blob, this.state.rfpFile)
			})
		} catch (error) {
			console.log('try catch failure')
		}
	}	

	handleButton (obj) {
		if (obj === 'B') this.props.history.push('dashboard')

		else if (obj === 'D') {
			const newVendorsList = this.state.vendorsList.map((value, sidx) => {
				if(value.isChecked === 'checked') this.state.compareVendorsList.push(value.id)
				return { ...value, value }
			})
			this.setState({ newVendorsList })
			this.handleSubmit()
		}
	}

	handleCheckBox (e, idx) {
		// console.log('inside', idx)
		const newVendorsList = this.state.vendorsList.map((value, sidx) => {
			if (idx !== sidx) return value
			if(value.isChecked === '') return { ...value, isChecked: 'checked' }
			else return { ...value, isChecked: '' }
		})
		this.setState({ vendorsList: newVendorsList })
	}

	// to handle onblur event for new vendors email address fields.
	handleNewVendorOnBlurChange (e, idx) {
		const newVendors = this.state.newVendorsList.map((value, sidx) => {
			if (idx !== sidx) return value

			let errorMessage = ''

			// email validation
			if (!validateEmail(e.target.value)) errorMessage = 'Invalid email! Please enter valid vendor email address'
			else errorMessage = ''

			this.setState({ isFormValidationSuccess: errorMessage.length > 0 ? false : true })
			return { ...value, newVendorEmail: e.target.value, newVendorEmailError: errorMessage }
		})
		this.setState({ newVendorsList: newVendors })
	}

	// to handle new vendor input field 
	handleNewVendorInputChange (e, idx) {
		const newVendors = this.state.newVendorsList.map((value, sidx) => {
			if (idx !== sidx) return value			
			return { ...value, newVendorEmail: e.target.value, newVendorEmailError: '' }
		})
		this.setState({ newVendorsList: newVendors })
	}

	// to add new vendor
	handleAddNewVendor () {
		this.setState({ newVendorsList: this.state.newVendorsList.concat([{ newVendorEmail: '', newVendorEmailError: '' }]) })
	}
	
	// to remove new vendor
	handleRemoveNewVendor (e, idx) {
		this.setState({ newVendorsList: this.state.newVendorsList.filter((s, sidx) => idx !== sidx) })
  }

	// Date onChange event
	handleDateChange(objDate) {
		const formFieldsState = this.state.formFields
		const formErrorsState = this.state.formErrors

		if (this.state.errMsg.length > 0) this.setState({ errMsg: '' })

		formFieldsState['bidEndDate'] = moment(objDate).format('DD/MM/YYYY')
		formErrorsState['bidEndDate'] = ''

		this.setState({ formFieldsState, formErrorsState, tempBidEndDate: objDate._d })
	}

	// to form selected vendors from dropdown list
	onSelectVendorsFromList (selectedList, selectedItem) {
		let formErrors = this.state.formErrors
		formErrors['oldVendorsList'] = ''
		this.setState({ formErrors })
		this.state.formFields.oldVendorsList.push(selectedItem)
	}

	// to remove and form selected vendors from dropdown list
	onRemoveVendorsFromList (selectedList, removedItem) {
		this.state.formFields.oldVendorsList.splice( this.state.formFields.oldVendorsList.indexOf(removedItem), 1 )
	}

	// Radio button onchange event
	handleBidTypeOnChange(objName) {
		const formFieldsState = this.state.formFields
		const formErrorsState = this.state.formErrors

		if (this.state.errMsg.length > 0) this.setState({ errMsg: '' })

		formFieldsState['bidType'] = objName
		formErrorsState['bidType'] = ''
		this.setState({ formFieldsState, formErrorsState })
	}

	// Input focus exit
	handleInputFocusExit(e) {
		const { name, value } = e.target
		const dataType = e.target.getAttribute('data-type')

		let formErrorState = this.state.formErrors

		this.setState({ disablebtn: 'outer-btn' })
		this.setState({ isFormValidationSuccess: true })

		if (dataType === 'mobile') {
			// mobile/phone validation
			if (!(/^(\+(([0-9]){1,2})[-.])?((((([0-9]){2,3})[-.]){1,2}([0-9]{4,10}))|([0-9]{10}))$/.test(value))) {
				formErrorState[name] = 'Invalid mobile number! Please enter 10 digit mobile number'
				this.setState({ isFormValidationSuccess: false })
			}
		}
	}

	// Input change event
	handleInputChange(e) {
		const { name, value } = e.target
		const dataType = e.target.getAttribute('data-type')
		let isvalidValue = false

		let formFieldsState = this.state.formFields
		let formErrorState = this.state.formErrors

		if (this.state.errMsg.length > 0) this.setState({ errMsg: '' })

		// Form Handling
		if (dataType === 'number' || dataType === 'pincode' || dataType === 'mobile') {
			// to allow only numbers
			if (!isNaN(value)) isvalidValue = true
		} else if (dataType === 'city') {
			// to allow only alphabets and space.
			if (/^[a-zA-Z ]*$/.test(value)) isvalidValue = true
		} else isvalidValue = true

		if (isvalidValue) {
			formErrorState[name] = '' // Error handling
			formFieldsState[name] = value // Form field 
			this.setState({ formFieldsState, formErrorState })
		}
	}

	// to check all fields are entered or not.
	handleEmptyFormCheck() {
		let valid = true
		const formFields = this.state.formFields
		const errorFields = this.state.formErrors
		Object.keys(formFields).forEach(key => {
			let value = formFields[key]
			if (value === null || value.length <= 0) {
				if (Object.keys(errorFields).indexOf(key) >= 0) {
					if (key !== 'oldVendorsList' && key !== 'shipAltCont') {
						errorFields[key] = 'error'
						valid = false
					}
					// this.setState({ errMsg: '*Please enter the mandatory fields.' })
				}
			}
		})
		this.setState({ errorFields })

		// to display error message when vendors not selected.
		if (this.state.newVendorsList.length === 0) {
			if (formFields.oldVendorsList.length === 0 ) {
				errorFields['oldVendorsList'] = 'Please select vendors from list'
				this.setState({ errorFields })
				valid = false
			}
		}

		// validation for newly added vendors via input fields.
		if (this.state.newVendorsList.length > 0) {
			const newVendors = this.state.newVendorsList.map((value, sidx) => {
				let errorMessage = ''
				// empty validation
				if (value.newVendorEmail.length === 0) {
					errorMessage = 'Please enter vendor email address'
					valid = false
				}
				else errorMessage = ''
				return { ...value, newVendorEmail: value.newVendorEmail, newVendorEmailError: errorMessage }
			})
			this.setState({ newVendorsList: newVendors })
		}

		return valid
	}

	// to handle submit
	handleSubmit() {
		this.setState({ disablebtn: 'outer-btn disablebtn' })

		// fileDownload('data', this.state.bidDocument.name)

		if (this.state.compareVendorsList.length === 0 ) {
			this.setState({ errMsg: '*Please select vendors to compare', disablebtn: 'outer-btn' })
			return
		}

		try {
			const tempData = {
				'rfpId': this.props.location.state.rfpId,
				'vendors' : this.state.compareVendorsList
			}
			const formData = new FormData()
			formData.append('data', JSON.stringify(tempData))

			const reqURL = serverURL + '/rfp/bid/compare'
			let userInfo = JSON.parse(localStorage.getItem('userInfo'))
		
			const options = {
				headers: {
					'Access-Control-Allow-Origin': "*",
					'Content-Disposition': "attachment; filename=template.xlsx",
					'Content-Type': 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
					'access_key': userInfo.access_key
				},
				responseType: 'arraybuffer'
			}

			axios.post(reqURL, formData, options).then(res => {
				console.log('response', res.data)
				var dateNow = new Date().getTime()
				var blob = new Blob([res.data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' })
				FileSaver.saveAs(blob, 'Compare_Vendors_Bid_'+dateNow+'.xlsx')			
			})
		} catch (error) {
			console.log('try catch failure')
		}
	}

	componentDidMount () {
		let userInfo = JSON.parse(localStorage.getItem('userInfo'))
    if (userInfo) {
      if (this.state.accountType !== 'buyer') this.props.history.push('/')
      else if (this.state.accountType === 'buyer') {
        this.setState({ profileName: userInfo.firstName + ' ' + userInfo.lastName })
        this.setState({ profileIcon: userInfo.firstName.charAt(0) + userInfo.lastName.charAt(0) })
      }
		} else this.props.history.push('/')
		
		try {
			// console.log('this.props', this.props.location.state.rfpId)
			let rfpId = this.props.location.state.rfpId
			const reqURL = serverURL + '/rfp/details/' + rfpId
			const options = {
				headers: {
					'Access-Control-Allow-Origin': "*",
					'access_key': userInfo.access_key,
				}
			}
			axios.get(reqURL, options).then(res => {
				console.log('rk data ', res.data)
				let data = res.data.data
				if (data) {
					const formFieldsState = this.state.formFields
					formFieldsState['itemCode'] = data.itemCode
					formFieldsState['itemName'] = data.itemName
					formFieldsState['HSNCode'] = data.HSNCode
					formFieldsState['UNSPSCCode'] = data.UNSPSCCode
					formFieldsState['itemDesc'] = data.itemDesc
					formFieldsState['quantity'] = data.quantity
					formFieldsState['bidType'] = data.bidType

					// formFieldsState['bidStartDate'] = data.bidStartDate
					this.refs.bidStartDate.state.inputValue = moment(new Date((parseInt(data.bidStartDate) * 1000))).format('DD/MM/YYYY')

					// formFieldsState['bidEndDate'] = (new Date(parseInt(data.bidEndDate) * 1000)).toString()
					this.refs.bidEndDate.state.inputValue = moment(new Date((parseInt(data.bidEndDate) * 1000))).format('DD/MM/YYYY hh:mm A')

					let vendorsList = this.state.vendorsList
					if (data.vendorsList.length > 0) {
						const resVendorsList = data.vendorsList.map((value, sidx) => {
							// this.state.vendorsList.push(value)
							return { ...value, id: value.id, name: value.name, email: value.email, isChecked: '' }
						})
						this.setState({ vendorsList: resVendorsList, isRFPExpired: data.isExpiry })
					}
					
					// formFields.oldVendorsList: [],
					// formFields.newVendorsList: [],
					formFieldsState['shipAddress'] = data.shipAddress
					formFieldsState['shipCity'] = data.shipCity
					formFieldsState['shipPincode'] = data.shipPincode
					formFieldsState['shipMobile'] = data.shipMobile
					formFieldsState['shipAltCont'] = data.shipAltCont
					this.setState({ formFieldsState, rfpFile: data.rfpFile })	
				}
			})
		} catch (error) {
			console.log('try catch failure')
		}
	}

	render() {	
		// form fields
		let { itemCode, itemName, HSNCode, UNSPSCCode, itemDesc, quantity, bidType, shipAddress, shipCity, shipPincode, shipMobile, shipAltCont } = this.state.formFields
		// form errors
		let formErrors = this.state.formErrors

		// console.log('vendorsList', this.state.vendorsList)

		return (
			<div id='brfp-det-form'>
        {/* Header */}
				<Header profileName={this.state.profileName} profileIcon={this.state.profileIcon} />
				
				<div className='rfp-container'>
					<div className='rfp-block'>
						<Link to='/buyer/dashboard'><img src='/assets/images/ic-back.png' /></Link>
						<h1 className='rfp-title'>RFP Details</h1>

						{/* RFP details block */}
						<div className='comp-det-block'>
							<div className='flex-box'>
								{/* Item Code */}
								<div className='input-container'>
									<div className='input-groups'>
										<label className='field-caption'>Item Code*</label>
										<div className='out-ex-size'>
											<input type='text' className={formErrors.itemCode.length > 0 ? 'form-field error' : 'form-field'} autoComplete='off' data-type='string' name='itemCode' value={itemCode} placeholder='Enter Item Code' onChange={this.handleInputChange} maxLength='20' disabled={this.state.isDisabled} />
											{formErrors.itemCode.length > 0 && <div className='error-title'>Please enter item code</div>}
										</div>
									</div>
								</div>
								{/* Item Name */}
								<div className='input-container'>
									<div className='input-groups'>
										<label className='field-caption'>Item Name*</label>
										<div className='out-ex-size'>
											<input type='text' className={formErrors.itemName.length > 0 ? 'form-field error' : 'form-field'} autoComplete='off' data-type='string' name='itemName' value={itemName} placeholder='Enter Item Name' onChange={this.handleInputChange} maxLength='30' disabled={this.state.isDisabled} />
											{formErrors.itemName.length > 0 && <div className='error-title'>Please enter item name</div>}
										</div>
									</div>
								</div>
							</div>
							{/* HSN Code */}
							<div className='input-container'>
								<div className='input-groups'>
									<label className='field-caption'>HSN Code*</label>
									<div className='out-ex-size'>
										<input type='text' className={formErrors.HSNCode.length > 0 ? 'form-field ex-size error' : 'form-field ex-size'} autoComplete='off' data-type='string' name='HSNCode' value={HSNCode} title={HSNCode} placeholder='Enter HSN code' onChange={this.handleInputChange} maxLength='20' disabled={this.state.isDisabled} />
										{formErrors.HSNCode.length > 0 && <div className='error-title'>Please enter HSNCode code</div>}
									</div>
								</div>
							</div>
							{/* UNBPSC Code */}
							<div className='input-container'>
								<div className='input-groups'>
									<label className='field-caption'>UNSPSC Code*</label>
									<div className='out-ex-size'>
										<input type='text' className={formErrors.UNSPSCCode.length > 0 ? 'form-field ex-size error' : 'form-field ex-size'} autoComplete='off' data-type='string' name='UNSPSCCode' value={UNSPSCCode} title={UNSPSCCode} placeholder='Enter UNSPSC code' onChange={this.handleInputChange} maxLength='20' disabled={this.state.isDisabled} />
										{formErrors.UNSPSCCode.length > 0 && <div className='error-title'>Please enter UNSPSC code</div>}
									</div>
								</div>
							</div>

							{/* Download RFP from uploaded by buyer while RFP creation */}
							{this.state.rfpFile.length > 0
								? <div className='doc-title'>*Please click <span onClick={this.handleDownload}>here</span> to download buyer proposal document.</div>
								: <>
									{/* Description */}
									<div className='input-container textarea-sec'>
										<div className='input-groups input-textarea'>
											<label className='field-caption note-filed'>Description*</label>
											<div className='out-ex-size'>
												<textarea className={formErrors.itemDesc.length > 0 ? 'cls-notes error' : 'cls-notes'} autoComplete='false' data-type='string' name='itemDesc' value={itemDesc} placeholder='Enter Description' onChange={this.handleInputChange} maxLength='250' readOnly={this.state.isDisabled} />
												{formErrors.itemDesc.length > 0 && <div className='error-title'>Please enter description</div>}
											</div>
										</div>
									</div>
									<div className='flex-box'>
										{/* Quantity  */}
										<div className='input-container'>
											<div className='input-groups'>
												<label className='field-caption'>Quantity*</label>
												<div className='out-ex-size'>
													<input type='text' className={formErrors.quantity.length > 0 ? 'form-field error' : 'form-field'} autoComplete='off' data-type='number' name='quantity' value={quantity} placeholder='Enter Quantity' onChange={this.handleInputChange} maxLength='10' disabled={this.state.isDisabled} />
													{formErrors.quantity.length > 0 && <div className='error-title'>{formErrors.quantity.length > 5 ? formErrors.quantity : 'Please enter quantity'}</div>}
												</div>
											</div>
										</div>
									</div>
								</>
							}
						</div>

						{/* Bidding details */}
						<h2 className='rfp-sub-title'>Bidding Details</h2>
						<div className='comp-det-block'>
							{/* Bid Type */}
							<div className='input-container radio-sec mar-bot'>
								<div className='input-groups radio-field'>
									<label className='field-caption no-pad'>Bid Type*</label>
									<div className='radio-input-section'>
										{bidType === 'open' && <label className='radio-container' onClick={() => this.handleBidTypeOnChange('open')}><input type='radio' className='form-field-radio' value='open' name='bidType' checked={ bidType.toLowerCase() === 'open' ? true : false} />Open<span className={'checkmark ' + formErrors.bidType} /></label>}
										{bidType === 'close' && <label className='radio-container' onClick={() => this.handleBidTypeOnChange('close')}><input type='radio' className='form-field-radio' value='close' name='bidType' checked={ bidType.toLowerCase() === 'close' ? true : false} />Close<span className={'checkmark ' + formErrors.bidType} /></label>}
									</div>
								</div>
							</div>
							<div className='out-radio-ex-size'>
								{formErrors.bidType.length > 0 && <div className='error-title'>Please choose bit type</div>}
							</div>
							<div className='flex-box'>
								{/* Bid start date */}
								<div className='input-container'>
									<div className='input-groups'>
										<label className='field-caption'>Bid Start Date*</label>
										<div className='out-ex-size'>
											<Datetime
												ref = 'bidStartDate'
												dateFormat={'DD/MM/YYYY'}
												// timeFormat={false}
												className='date form-field'
												name='bidStartDate'
												defaultValue={moment(new Date())}
												closeOnSelect
												closeOnTab
												inputProps={{ placeholder: 'DD/MM/YYYY', disabled: this.state.isDisabled}}
												onChange={this.handleDateChange} />
										</div>
									</div>
								</div>
								{/* Bid Close date */}
								<div className='input-container'>
									<div className='input-groups'>
										<label className='field-caption'>Bid Close Date &amp; Time*</label>
										<div className='out-ex-size'>
											<Datetime 
												ref = 'bidEndDate'
												dateFormat={'DD/MM/YYYY'}
												className={formErrors.bidEndDate.length > 0 ? 'date form-field error' : 'date form-field'}
												name='bidEndDate'
												defaultValue={moment(new Date())}
												closeOnSelect
												closeOnTab
												inputProps={{ placeholder: 'DD/MM/YYYY', readOnly: true, disabled: this.state.isDisabled}}
												onChange={this.handleDateChange} />
											{formErrors.bidEndDate.length > 0 && <div className='error-title'>Please select bid close date &amp; time</div>}
										</div>
									</div>
								</div>
							</div>
							{/* Bidding vendors details */}
							<h3 className='bid-vendor-title'>Vendor's Details</h3>
							
							{bidType.toLowerCase() === 'open'
								?
									// {/* Table content */}
									<div className='db-tbl-block'>
										{/* Table header */}
										<div className='tbl-header-block'>
											<div className='tbl-head col-11'>Sl.No</div>
											<div className='tbl-head col-22'>Vendor Name</div>
											<div className='tbl-head col-33'>Description</div>
											<div className='tbl-head col-44'>Quantity</div>
											<div className='tbl-head col-55'>Unit Price</div>
											<div className='tbl-head col-66'>Total</div>
											<div className='tbl-head col-77'>Compare</div>
										</div>
										{/* Table body */}
										<div className='tbl-body-container'>
											{this.state.vendorsList.length === 0
											? <div className='tbl-empty-row'>Vendors data not found to compare</div>
											:	this.state.vendorsList.map((value, idx) => (
													<div className='tbl-body-block' key={idx}>
														<div className='tbl-body col-11'>{idx+1}</div>
														<div className='tbl-body col-22' title={value.name + '[' + value.email + ']'}>{value.name}</div>
														<div className='tbl-body col-33'>{value.bidDesc}</div>
														<div className='tbl-body col-44'>{value.bidQty}</div>
														<div className='tbl-body col-55'>${value.bidUnitPrice}</div>
														<div className='tbl-body col-66'>${value.bidTotal}</div>
														<div className='tbl-body col-77'>
															<label className='chk-box-block'>
																<input type='checkbox' checked={value.isChecked} />
																<span className='checkbox-checkmark' onClick={(e) => this.handleCheckBox(e, idx)}>&nbsp;</span>
															</label>
														</div>
													</div>
												))
											}
										</div>
									</div>								
								:
									// {/* Table content */}
									<div className='db-tbl-block'>
										{/* Table header */}
										<div className='tbl-header-block'>
											<div className='tbl-head col-1'>Sl.No</div>
											<div className='tbl-head col-2'>Vendor Name</div>
											<div className='tbl-head col-3'>Vendor Email</div>
											<div className='tbl-head col-4'>Compare</div>
											{this.state.isRFPExpired &&
												<div className='tbl-head col-5'>Download</div>
											}
										</div>
										{/* Table body */}
										<div className='tbl-body-container'>
											{this.state.vendorsList.length === 0
											? <div className='tbl-empty-row'>Vendors data not found to compare</div>
											:	this.state.vendorsList.map((value, idx) => (
													<div className='tbl-body-block' key={idx}>
														<div className='tbl-body col-1'>{idx+1}</div>
														<div className='tbl-body col-2'>{value.name}</div>
														<div className='tbl-body col-3'>{value.email}</div>
														<div className='tbl-body col-4'>
															<label className='chk-box-block'>
																<input type='checkbox' checked={value.isChecked} />
																<span className='checkbox-checkmark' onClick={(e) => this.handleCheckBox(e, idx)}>&nbsp;</span>
															</label>
														</div>
														{this.state.isRFPExpired &&
															<div className='tbl-body col-5' onClick={() => this.handleExpiredRFPDownload(value.id, value.uploadFilename)}>
																<div className='ic-download' title="Download vendor's expired RFP details" />
															</div>
														}
													</div>
												))
											}
										</div>
									</div>
							}

							{!this.state.isHide &&
								// {/* Bid to be send */}
								<div className='input-container'>
									<div className='input-groups'>
										<label className='field-caption'>Select Vendor's*</label>
										<div className='out-ex-size'>
											<Multiselect
												options={this.state.vendorsList} // Options to display in the dropdown
												selectedValues={this.state.selectedValue} // Preselected value to persist in dropdown
												onSelect={this.onSelectVendorsFromList} // Function will trigger on select event
												onRemove={this.onRemoveVendorsFromList} // Function will trigger on remove event
												displayValue='name' // Property name to display in the dropdown options
												showCheckbox={true}
												avoidHighlightFirstOption={true}
												style={
													{ multiselectContainer: { width: '102%', fontFamily: 'OpenSans-Regular', fontSize: '14px', lineHeight: '19px', letterSpacing: '.2px' },
														searchBox: { border: formErrors.oldVendorsList.length > 0 ? '1px solid rgba(255,0,0,0.35)' : '1px solid #e3e3e3', borderRadius: '2px', padding: '10px' },
														chips: { margin: '0px 5px 0px 0px', background: '#cac6c6' },
														inputField: { fontFamily: 'OpenSans-Regular', fontSize: '14px', lineHeight: '19px', letterSpacing: '.2px', margin: '0px' },
														option: { fontFamily: 'OpenSans-Regular', fontSize: '14px', lineHeight: '19px', letterSpacing: '.2px', color: 'rgba(0,0,0,0.8)', display: 'flex', alignItems: 'center' }
													}
												}
											/>
											{formErrors.oldVendorsList.length > 0 && <div className='error-title'>{formErrors.oldVendorsList.length > 5 ? formErrors.oldVendorsList : 'Please select vendors from list'}</div>}
										</div>
									</div>
								</div>
							}

							{!this.state.isHide &&
								// {/* New Vendor Email */}
								this.state.newVendorsList.map((value, idx) => (
									<div className='input-container'>
										<div className='input-groups'>
											<label className='field-caption'>New Vendor Email*</label>
											<div className='out-ex-size no-wrap'>
												<input type='text' className={value.newVendorEmailError.length > 0 ? 'form-field new-ven-ex-size error' : 'form-field new-ven-ex-size'} autoComplete='off' data-type='email' name='newVendorEmail' value={value.newVendorEmail} placeholder='Enter New Vendor Email Address' onChange={(e) => this.handleNewVendorInputChange(e, idx)} onBlur={(e) => this.handleNewVendorOnBlurChange(e, idx)} maxLength='50' /><span className='close-icon' onClick={(e) => this.handleRemoveNewVendor(e, idx)}>X</span>
												{value.newVendorEmailError.length > 0 && <div className='error-title'>{value.newVendorEmailError.length > 5 ? value.newVendorEmailError : 'Please enter vendor address'}</div>}
											</div>
										</div>
									</div>
								))
							}

							{/* Download your price comparison */}
							{this.state.vendorsList.length > 0 &&
								<div className='form-submit-section btn-right'>
									<div className='outer-btn btn-add'>
										<div className='btn register-booking' onClick={() => this.handleButton('D')}>Download your price comparison</div>
									</div>
								</div>
							}
						</div>

						{!this.state.isHide &&
							// {/* Shipping details */}
							// <h2 className='rfp-sub-title'>Shipping Details</h2>
							<div className='comp-det-block'>
								{/* Address */}
								<div className='input-container textarea-sec'>
									<div className='input-groups input-textarea'>
										<label className='field-caption note-filed'>Shipping Address*</label>
										<div className='out-ex-size'>
											<textarea className={formErrors.shipAddress.length > 0 ? 'cls-notes error' : 'cls-notes'} autoComplete='false' data-type='string' name='shipAddress' value={shipAddress} placeholder='Enter Shipping Address' onChange={this.handleInputChange} maxLength='250' />
											{formErrors.shipAddress.length > 0 && <div className='error-title'>Please enter shipping address</div>}
										</div>
									</div>
								</div>
								<div className='flex-box'>
									{/* City */}
									<div className='input-container'>
										<div className='input-groups'>
											<label className='field-caption'>City*</label>
											<div className='out-ex-size'>
												<input type='text' className={formErrors.shipCity.length > 0 ? 'form-field error' : 'form-field'} autoComplete='off' data-type='city' name='shipCity' value={shipCity} placeholder='Enter City' onChange={this.handleInputChange} maxLength='30' />
												{formErrors.shipCity.length > 0 && <div className='error-title err-width'>Please enter city</div>}
											</div>
										</div>
									</div>
									{/* Pincode */}
									<div className='input-container'>
										<div className='input-groups'>
											<label className='field-caption'>Pincode*</label>
											<div className='out-ex-size'>
												<input type='text' className={formErrors.shipPincode.length > 0 ? 'form-field error' : 'form-field'} autoComplete='off' data-type='pincode' name='shipPincode' value={shipPincode} placeholder='Enter Pincode' onChange={this.handleInputChange} maxLength='10' />
												{formErrors.shipPincode.length > 0 && <div className='error-title err-width'>Please enter pincode</div>}
											</div>
										</div>
									</div>
								</div>
								<div className='flex-box'>
									{/* Mobile */}
									<div className='input-container'>
										<div className='input-groups'>
											<label className='field-caption'>Mobile*</label>
											<div className='out-ex-size'>
												<input type='text' className={formErrors.shipMobile.length > 0 ? 'form-field error' : 'form-field'} autoComplete='off' data-type='mobile' name='shipMobile' value={shipMobile} placeholder='Enter Mobile Number' onChange={this.handleInputChange} onBlur={this.handleInputFocusExit} maxLength='15' />
												{formErrors.shipMobile.length > 0 && <div className='error-title err-width'>{formErrors.shipMobile.length > 5 ? formErrors.shipMobile : 'Please enter mobile number'}</div>}
											</div>
										</div>
									</div>
									{/* Alternate Contact Number */}
									<div className='input-container'>
										<div className='input-groups'>
											<label className='field-caption'>Alternate Contact No.</label>
											<div className='out-ex-size'>
												<input type='text' className={formErrors.shipAltCont.length > 5 ? 'form-field error' : 'form-field'} autoComplete='off' data-type='mobile' name='shipAltCont' value={shipAltCont} placeholder='Enter Alternate Contact Number' onChange={this.handleInputChange} onBlur={this.handleInputFocusExit} maxLength='15' />
												{formErrors.shipAltCont.length > 5 && <div className='error-title err-width'>{formErrors.shipAltCont}</div>}
											</div>
										</div>
									</div>
								</div>
							</div>
						}
						{!this.state.isHide &&
							// {/* Error message */}
							<div className='cls-err-msg'>{this.state.errMsg}&nbsp;</div>
						}
						{!this.state.isHide &&
							// {/* Submit button */}
							<div className='form-submit-section'>
								<div className={this.state.disablebtn}>
									<div className='btn register-booking' onClick={this.handleSubmit}>DOWNLOAD CSV</div>
								</div>
							</div>
						}
					</div>
				</div>
			</div>
		)
	}
}
export default withRouter(BuyerRFPDetails)
