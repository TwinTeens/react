import React, { Component } from 'react'
import axios from 'axios'
import { Link, withRouter } from 'react-router-dom'
import { validateEmail, serverURL } from '../../global'

import './Signup.css'

class Signup extends Component {
  constructor (props) {
    super(props)
    this.state = {
			errMsg: '',
			disablebtn: 'outer-btn',
			accountType: 'buyer',
			isFormValidationSuccess: true,
			formFields: {
				accountType: '',
				firstName: '',
				lastName: '',
				email: '',
				password: '',
				confirmPassword: ''
			},
			formErrors: {
				accountType: '',
				firstName: '',
				lastName: '',
				email: '',
				password: '',
				confirmPassword: ''
			}
    }
		this.handleInputFocusExit = this.handleInputFocusExit.bind(this)
		this.handleInputChange = this.handleInputChange.bind(this)
		this.handleEmptyFormCheck = this.handleEmptyFormCheck.bind(this)
		this.handleSubmit = this.handleSubmit.bind(this)
		this.accountTypeOnChange = this.accountTypeOnChange.bind(this)
		this.handleForms = this.handleForms.bind(this)
	}

	// to redirect home page
	handleForms (obj) { if (obj === 'login') this.props.history.push('/') }

  // Radio button onchange event
  accountTypeOnChange (objName) {
		const formFieldsState = this.state.formFields
		const formErrorsState = this.state.formErrors
		
		if(this.state.errMsg.length > 0) this.setState({ errMsg: '' })

		formFieldsState['accountType'] = objName
		formErrorsState['accountType'] = ''
		this.setState({ formFieldsState, formErrorsState })
		// to change account type based on user selection.
		this.setState({ accountType: objName })
	}

	// Input focus exit
	handleInputFocusExit (e, objType) {
		const { name, value } = e.target
		// const dataType = e.target.getAttribute('data-type')

		this.setState({ disablebtn: 'outer-btn' })
		
		const formFields = this.state.formFields
		const formErrors = this.state.formErrors

		if (objType === 'E') {
			// email validation
			if (!validateEmail(value)) {
				formErrors[name] = '*Invalid email! Please enter valid email address'
				this.setState({ isFormValidationSuccess: false })
			} else this.setState({ isFormValidationSuccess: true })
		} else if (objType === 'P' || objType === 'CP') {
			if (!(/^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{8,16}$/.test(value))) {
				formErrors[name] = '*Password must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters.'
				this.setState({ isFormValidationSuccess: false })
			} else {
				if (formFields.password.length >= 8 && formFields.confirmPassword.length >= 8 ) {
					if (formFields.password !== formFields.confirmPassword) {
						formErrors[name] = '*Password and confirmation password do not match!'
						this.setState({ isFormValidationSuccess: false })	
					} else this.setState({ isFormValidationSuccess: true })
				}
			}
		}
	}

  // Input change event
  handleInputChange (e) {
		const { name, value } = e.target

		if(this.state.errMsg.length > 0) this.setState({ errMsg: '' })

		const formFields = this.state.formFields
		const formErrors = this.state.formErrors
		formErrors[name] = '' // Error handling
		formFields[name] = value  // Form handling
		this.setState({ formFields, formErrors })
	}

  // to check all fields are entered or not.
  handleEmptyFormCheck () {
		let valid = true
		const formFields = this.state.formFields
		const errorFields = this.state.formErrors
		Object.keys(formFields).forEach(key => {
			let value = formFields[key]
			if (value === null || value.length <= 0) {
				errorFields[key] = 'error'
				valid = false
				// this.setState({ errMsg: '*Please enter mandatory fields.' })
			}
		})
		this.setState({ errorFields })
    return valid
  }	

	// to handle submit
	handleSubmit () {
		let isFormValid = true

		this.setState({ disablebtn: 'outer-btn disablebtn' })

		if (!this.handleEmptyFormCheck()) isFormValid = false

		if (isFormValid && this.state.isFormValidationSuccess) {

			const reqURL = serverURL + '/user/create'
			const reqBody = {
				'accountType': this.state.formFields.accountType,
				'firstName': this.state.formFields.firstName,
				'lastName': this.state.formFields.lastName,
				'email': this.state.formFields.email,
				'password': this.state.formFields.password
			}
			
			axios.post(reqURL, reqBody).then(res => {
				// console.log('data ', res.data)
				if (res.data.status.toLowerCase() === 'success') {
					this.props.history.push('/thank-you')
				} else if (res.data.status.toLowerCase() === 'error') {
					this.setState({ errMsg: '*User already registered in ths email address' })
					this.setState({ disablebtn: 'outer-btn' })
				}
			})
		} else this.setState({ disablebtn: 'outer-btn' })
	}

  render () {	
		let formFields = this.state.formFields
		let formErrors = this.state.formErrors
		return (
			<div id='signup-form'>
				{/* Header */}
				<div className='hdr-block img-home-bg' />
				<Link to='/'><div className='brand-logo' /></Link>
				
				{/* Main content */}
				<div className='signup-container'>
					<div className='signup-block'>
						<h1 className='signup-title'>Sign Up</h1>
						{/* Login details block */}
						<div className='signup-fields-block'>
							{/* Account Type */}
							<div className={formErrors.accountType.length > 0 ? 'input-container radio-sec mar-bot' : 'input-container radio-sec'}>
								<div className='input-groups radio-field'>
									<label className='field-caption no-pad'>Account Type*</label>
									<div className='radio-input-section'>
										<label className='radio-container' onClick={() => this.accountTypeOnChange('buyer')}><input type='radio' className='form-field-radio' value='buyer' name='accountType' checked={formFields.accountType === 'buyer' ? true : false} />Buyer<span className={'checkmark ' + formErrors.accountType} /></label>
										<label className='radio-container' onClick={() => this.accountTypeOnChange('vendor')}><input type='radio' className='form-field-radio' value='vendor' name='accountType' checked={formFields.accountType === 'vendor' ? true : false} />Vendor<span className={'checkmark ' + formErrors.accountType} /></label>
									</div>
								</div>
							</div>
							{formErrors.accountType.length > 0 && 
								<div className='out-radio-ex-size'>
									<div className='error-title'>*Please choose account type</div>
								</div>       
							}        
							{/* Firstname */}
							<div className='input-container'>
								<div className='input-groups'>
									<label className='field-caption'>First Name*</label>
									<div className='out-ex-size'>
										<input type='text' className={formErrors.firstName.length > 0 ? 'form-field error': 'form-field'} autoComplete='off' name='firstName' value={formFields.firstName} placeholder='Enter your first name' onChange={this.handleInputChange} maxLength='30' />
										{formErrors.firstName.length > 0 && <div className='error-title'>*Please enter your first name</div>}
									</div>
								</div>
							</div>
							{/* Lastname */}
							<div className='input-container'>
								<div className='input-groups'>
									<label className='field-caption'>Last Name*</label>
									<div className='out-ex-size'>
										<input type='text' className={formErrors.lastName.length > 0 ? 'form-field error': 'form-field'} autoComplete='off' name='lastName' value={formFields.lastName} placeholder='Enter your last name' onChange={this.handleInputChange} maxLength='30' />
										{formErrors.lastName.length > 0 && <div className='error-title'>*Please enter your last name</div>}									
									</div>
								</div>
							</div>
							{/* Email */}
							<div className='input-container'>
								<div className='input-groups'>
									<label className='field-caption'>Email*</label>
									<div className='out-ex-size'>
										<input type='text' className={formErrors.email.length > 0 ? 'form-field error': 'form-field'} autoComplete='off' name='email' value={formFields.email} placeholder='Enter your email address' onChange={this.handleInputChange} onBlur={(e) => this.handleInputFocusExit(e, 'E')} maxLength='50' />
										{formErrors.email.length > 0 && <div className='error-title'>{formErrors.email.length > 5 ? formErrors.email : '*Please enter email address'}</div>}									
									</div>
								</div>
							</div>
							{/* Password */}
							<div className='input-container'>
								<div className='input-groups'>
									<label className='field-caption'>Password*</label>
									<div className='out-ex-size'>
										<input type='password' className={formErrors.password.length > 0 ? 'form-field error': 'form-field'} autoComplete='off' name='password' value={formFields.password} placeholder='Enter your password' onChange={this.handleInputChange} onBlur={(e) => this.handleInputFocusExit(e, 'P')} maxLength='20' />
										{formErrors.password.length > 0 && <div className='error-title'>{formErrors.password.length > 5 ? formErrors.password : '*Please enter password'}</div>}																		
									</div>								
								</div>
							</div>
							{/* confirm Password */}
							<div className='input-container'>
								<div className='input-groups'>
									<label className='field-caption'>Confirm Password*</label>
									<div className='out-ex-size'>
										<input type='password' className={formErrors.confirmPassword.length > 0 ? 'form-field error': 'form-field'} autoComplete='off' name='confirmPassword' value={formFields.confirmPassword} placeholder='Enter your confirm password' onChange={this.handleInputChange} onBlur={(e) => this.handleInputFocusExit(e, 'CP')} maxLength='20' />
										{formErrors.confirmPassword.length > 0 && <div className='error-title'>{formErrors.confirmPassword.length > 5 ? formErrors.confirmPassword : '*Please enter confirm password'}</div>}																											
									</div>								
								</div>
							</div>
						</div>

						{/* Signin */}
						<div className='sign-up-block'>
							<div className='sign-up-title'>Already have an account? <span onClick={() => this.handleForms('login')}>Login</span></div>
						</div>
						
						{/* Error message */}
						{this.state.errMsg.length > 0 && <div className='cls-err-msg'>{this.state.errMsg}&nbsp;</div>}

						{/* Submit button */}
						<div className='form-submit-section'>
							<div className={this.state.disablebtn}>
								<div className='btn' onClick={this.handleSubmit}>SIGNUP</div>
							</div>
						</div>

						{/* Terms and conditions */}
						<div className='sign-up-terms'>
							<div className='sign-up-title'> By clicking the Sign up button, you agree to our<br />  <span>Terms and Conditions,</span> and <span>Privacy Policy.</span></div>
						</div>

						{/* Back */}
						<div className='signup-back'><a href='/'>Back</a></div>
					</div>
				</div>
			</div>
    )
  }
}
export default withRouter(Signup)
