import React from 'react'
import { withRouter } from 'react-router-dom'
import './Header.css'

class Header extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      // isPaymentSuccess: false
    }
    this.handleDropdownMenu = this.handleDropdownMenu.bind(this)
    this.handleMenuItems = this.handleMenuItems.bind(this)
  }

  handleMenuItems (obj) {
    if (obj === 'CP') this.props.history.push('/change-password')
    else if (obj === 'L') {
      localStorage.clear()
      this.props.history.push('/')
    }
  }

  // to show dropdown menu while click
  handleDropdownMenu () {
    document.getElementById('myDropdown').classList.toggle('show')
  }

  // to hide menu items while click on outside of dropdown
  handleClickOutside (event) {
    if (!event.target.matches('.dropbtn')) {
      let dropdowns = document.getElementsByClassName('dropdown-content')
      let i
      for (i = 0; i < dropdowns.length; i++) {
        let openDropdown = dropdowns[i]
        if (openDropdown.classList.contains('show')) {
          openDropdown.classList.remove('show')
        }
      }
    }
  }
  
  componentDidMount () {
    document.addEventListener('click', this.handleClickOutside)
  }
  
  render () {
    return (
				<div id='header'>
					<div className='hdr-block'>
						<div className='hdr-flex-box'>
							<div className='flx-left-box'>
								<div className='brand-logo' />
							</div>
							<div className='flx-right-box'>
								{/* Dropdown menu */}
								<div className='dropdown'>
                  <div className='dropbtn-title'>{this.props.profileName}<button className='dropbtn' onClick={this.handleDropdownMenu}>{this.props.profileIcon}</button></div>
									<div id='myDropdown' className='dropdown-content'>
										<div onClick={() => this.handleMenuItems('CP')}><a>Change Password</a></div>
										<div onClick={() => this.handleMenuItems('L')}><a>Logout</a></div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
      )
  }
}

export default withRouter(Header)
