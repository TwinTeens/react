import React, { Component } from 'react'
import { withRouter } from 'react-router-dom'
import Datetime from 'react-datetime'
import 'react-datetime/css/react-datetime.css'
import moment from 'moment'
import axios from 'axios'
import { validateEmail, serverURL } from '../../global'
import Header from '../Header/Header'

import './VendorRegForm.css'

class VendorRegForm extends Component {
  constructor (props) {
    super(props)
    this.state = {
			accountType: localStorage.getItem('avatarType'),
			errMsg: '',
			pleaseWait: false,
			disablebtn: 'outer-btn',
			isFormValidationSuccess: true,
			// to handle Form Fields
			formFields: {
				companyDetails: {
					companyName: '',
					legalStatus: '',
					businessNature: '',
					regNumber: '',
					regAddress: '',
					regCity: '',
					regPincode: '',
					regPhone: '',
					regFax: '',
					regEmail: '',
					regWebsite: ''
				},
				commDetails: {
					commAddress: '',
					commCity: '',
					commPhone: '',
					commPincode: '',
					commEmail: '',
					commWebsite: '',
					commFax: '',
					commWorkingDays: '',
					commWorkingHours: ''
				},
				bankDetails: {
					bankName: '',
					bankCustName: '',
					bankAccountNumber: '',
					bankAccountType: '',
					bankTransMode: '',
					bankMircCode: '',
					bankAddress: '',
					bankBranchCode: ''
				},
				statutoryDetails: {
					statPanNo: '',
					statGSTIn: '',
					statLUTDate: '',
					statLUTNo: '',
					statMSMERegNo: ''
				},
				partnerDetails: {
					partnerName: '',
					partnerDesig: '',
					partnerPhone: '',
					partnerMobile: '',
					partnerEmail: ''
				},
				repDetails: {
					repName: '',
					repDesig: '',
					repPhone: '',
					repMobile: '',
					repEmail: '',
					repWorkDays: '',
					repWorkHours: '',
					repWorkFromTime: '',
					repWorkToTime: '',
				},
				afRepDetails: {
					afRepName: '',
					afRepDesig: '',
					afRepPhone: '',
					afRepMobile: '',
					afRepEmail: '',
					afRepWorkDays: '',
					afRepWorkHours: '',
					afRepWorkFromTime: '',
					afRepWorkToTime: '',
				},
				crcmDetails: {
					crcmName: '',
					crcmDesig: '',
					crcmPhone: '',
					crcmMobile: '',
					crcmEmail: '',
					crcmWorkDays: '',
					crcmWorkHours: '',
					crcmWorkFromTime: '',
					crcmWorkToTime: '',
				},
				fileUploadDetails: {
					businessRegCert: null,
					panCard: null,
					chequeLeaf: null,
					gstCert: null,
					msmeRegCert: null,
					tdsCert: null
				},
				declaration: false
			},
			// to handle Form Errors
			formErrors: {
				companyDetails: {
					companyName: '',
					legalStatus: '',
					businessNature: '',
					regNumber: '',
					regAddress: '',
					regCity: '',
					regPincode: '',
					regPhone: '',
					regFax: '',
					regEmail: '',
					regWebsite: ''
				},
				commDetails: {
					commAddress: '',
					commCity: '',
					commPhone: '',
					commPincode: '',
					commEmail: '',
					commWebsite: '',
					commFax: '',
					commWorkingDays: '',
					commWorkingHours: ''
				},
				bankDetails: {
					bankName: '',
					bankCustName: '',
					bankAccountNumber: '',
					bankAccountType: '',
					bankTransMode: '',
					bankMircCode: '',
					bankAddress: '',
					bankBranchCode: ''
				},
				statutoryDetails: {
					statPanNo: '',
					statGSTIn: '',
					statLUTDate: '',
					statLUTNo: '',
					// statMSMERegNo: ''
				},
				partnerDetails: {
					partnerName: '',
					partnerDesig: '',
					partnerPhone: '',
					partnerMobile: '',
					partnerEmail: ''
				},
				repDetails: {
					repName: '',
					repDesig: '',
					repPhone: '',
					repMobile: '',
					repEmail: '',
					repWorkDays: '',
					repWorkHours: '',
					repWorkFromTime: '',
					repWorkToTime: '',
				},
				afRepDetails: {
					afRepName: '',
					afRepDesig: '',
					afRepPhone: '',
					afRepMobile: '',
					afRepEmail: '',
					afRepWorkDays: '',
					afRepWorkHours: '',
					afRepWorkFromTime: '',
					afRepWorkToTime: '',
				},
				crcmDetails: {
					crcmName: '',
					crcmDesig: '',
					crcmPhone: '',
					crcmMobile: '',
					crcmEmail: '',
					crcmWorkDays: '',
					crcmWorkHours: '',
					crcmWorkFromTime: '',
					crcmWorkToTime: '',
				},
				fileUploadDetails: {
					businessRegCert: null,
					panCard: null,
					chequeLeaf: null,
					gstCert: null
				}
			}
    }
		// this.setLocation = this.setLocation.bind(this)
		this.handleInputChange = this.handleInputChange.bind(this)
		this.handleInputFocusExit = this.handleInputFocusExit.bind(this)
		this.legalStatusOnChange = this.legalStatusOnChange.bind(this)
		this.handleCheckBoxChange = this.handleCheckBoxChange.bind(this)
		this.handleFileUpload = this.handleFileUpload.bind(this)
		this.handleDateChange = this.handleDateChange.bind(this)
		this.handleSubmit = this.handleSubmit.bind(this)
		this.handleEmptyFormCheck = this.handleEmptyFormCheck.bind(this)
  }

	// Upload file event
	handleFileUpload (e) {
		const { name } = e.target
		const formFieldsState = this.state.formFields.fileUploadDetails

		if(this.state.errMsg.length > 0) this.setState({ errMsg: '' })

		formFieldsState[name] = e.target.files[0]
		this.setState({ formFieldsState })
	}

	// Date onChange event
	handleDateChange (objDate) {
		const formFieldsState = this.state.formFields.statutoryDetails
		const formErrorsState = this.state.formErrors.statutoryDetails

		if(this.state.errMsg.length > 0) this.setState({ errMsg: '' })

		formFieldsState['statLUTDate'] = moment(objDate).format('DD/MM/YYYY')
		formErrorsState['statLUTDate'] = ''
		this.setState({ formFieldsState, formErrorsState })
	}

  // Radio button onchange event
  legalStatusOnChange (objName) {
		const formFieldsState = this.state.formFields.companyDetails
		const formErrorsState = this.state.formErrors.companyDetails
		
		if(this.state.errMsg.length > 0) this.setState({ errMsg: '' })

		formFieldsState['legalStatus'] = objName
		formErrorsState['legalStatus'] = ''
		this.setState({ formFieldsState, formErrorsState })
	}

	// checkbox onChange event
	handleCheckBoxChange (e) {
		const formFieldsState = this.state.formFields
		formFieldsState[e.target.name] = e.target.checked
		this.setState({ formFieldsState })
	}

	// Input focus exit
	handleInputFocusExit (e, formFieldsState, formErrorState) {
		const { name, value } = e.target
		const dataType = e.target.getAttribute('data-type')

		this.setState({ disablebtn: 'outer-btn' })
		this.setState({ isFormValidationSuccess: true })

		if (dataType === 'mobile' || dataType === 'phone') {
			// mobile/phone validation
			if (!(/^(\+(([0-9]){1,2})[-.])?((((([0-9]){2,3})[-.]){1,2}([0-9]{4,10}))|([0-9]{10}))$/.test(value))) {
				if (dataType === 'mobile') formErrorState[name] = '*Invalid mobile number! Please enter 10 digit mobile number'
				else if (dataType === 'phone') formErrorState[name] = '*Invalid phone number! Please enter 10 digit phone number'
				this.setState({ isFormValidationSuccess: false })
			}
		} else if (dataType === 'email') {
			// email validation
			if (!validateEmail(value)) {
				formErrorState[name] = '*Invalid email! Please enter valid email address'
				this.setState({ isFormValidationSuccess: false })
			}
		}
	}

  // Input change event
  handleInputChange (e, formFieldsState, formErrorState) {
		const { name, value } = e.target
		const dataType = e.target.getAttribute('data-type')
		let isvalidValue = false

		if(this.state.errMsg.length > 0) this.setState({ errMsg: '' })

		// Form Handling
		if (dataType === 'number' || dataType === 'fax' || dataType === 'pincode' || dataType === 'phone' || dataType === 'mobile') {
			// to allow only numbers
			if (!isNaN(value)) isvalidValue = true
		} else if (dataType === 'city'){
			// to allow only alphabets and space.
			if (/^[a-zA-Z ]*$/.test(value)) isvalidValue = true 
		} else isvalidValue = true

		if (isvalidValue) {
			formErrorState[name] = '' // Error handling
			formFieldsState[name] = value // Form field 
			this.setState({ formFieldsState, formErrorState })
		}
	}

  // to check all fields are entered or not.
  handleEmptyFormCheck (objFormFields, objFormErrors) {
		let valid = true
		const formFields = objFormFields // this.state.formFields
		const errorFields = objFormErrors // this.state.formErrors
		Object.keys(formFields).forEach(key => {
			let value = formFields[key]
			if (value === null || value.length <= 0) {
				if (Object.keys(objFormErrors).indexOf(key) >= 0) {
					errorFields[key] = 'error'
					valid = false
					if (key === 'businessRegCert' || key === 'panCard' || key === 'chequeLeaf' || key === 'gstCert') {
						this.setState({ errMsg: '*Please upload scanned copy of self attested certificates.' })
					} else this.setState({ errMsg: '*Please enter the mandatory fields.' })
				}
			}
		})
		this.setState({ errorFields })
    return valid
  }	

	// to handle submit
	handleSubmit () {
		let formFields = this.state.formFields // form fields state
		let formErrors = this.state.formErrors // form errors state

		let isEmptyFormFields = true

		this.setState({ disablebtn: 'outer-btn disablebtn' })

		// companyDetails
		if (!this.handleEmptyFormCheck(formFields.companyDetails, formErrors.companyDetails)) isEmptyFormFields = false
		// commDetails
		if (!this.handleEmptyFormCheck(formFields.commDetails, formErrors.commDetails)) isEmptyFormFields = false
		// bankDetails
		if (!this.handleEmptyFormCheck(formFields.bankDetails, formErrors.bankDetails)) isEmptyFormFields = false  
		// statutoryDetails
		if (!this.handleEmptyFormCheck(formFields.statutoryDetails, formErrors.statutoryDetails)) isEmptyFormFields = false
		// partnerDetails
		if (!this.handleEmptyFormCheck(formFields.partnerDetails, formErrors.partnerDetails)) isEmptyFormFields = false			
		// repDetails
		if (!this.handleEmptyFormCheck(formFields.repDetails, formErrors.repDetails)) isEmptyFormFields = false		
		// afRepDetails
		if (!this.handleEmptyFormCheck(formFields.afRepDetails, formErrors.afRepDetails)) isEmptyFormFields = false		
		// crcmDetails
		if (!this.handleEmptyFormCheck(formFields.crcmDetails, formErrors.crcmDetails)) isEmptyFormFields = false		

		if (isEmptyFormFields) {
			// File uploads validations
			if (!this.handleEmptyFormCheck(formFields.fileUploadDetails, formErrors.fileUploadDetails)) isEmptyFormFields = false
		}

		if (isEmptyFormFields && this.state.isFormValidationSuccess) {			
			// self declaration validation
			if (!formFields.declaration) {
				this.setState({ errMsg: '*Please select the self declaration to register' })
				this.setState({ disablebtn: 'outer-btn' })
				return
			}

			try {
				const formData = new FormData()
				const reqBody = formFields // form fields
				const fileData = reqBody.fileUploadDetails // upload file data
				
				Object.keys(fileData).map((type) => {
					if (fileData[type] != null) formData.append(type,fileData[type])
				})

				formData.append('data', JSON.stringify(reqBody))
				
				const reqURL = serverURL + '/vendor/register'
				let userInfo = JSON.parse(localStorage.getItem('userInfo'))
				const options = {
					headers: { 
						'Access-Control-Allow-Origin': "*",
						'access_key': userInfo.access_key,
					}
				}
				this.setState({ pleaseWait: true })
				axios.post(reqURL, formData, options).then(res => {
					console.log('response', res.data)
					localStorage.setItem('isSuccess', true)
					this.props.history.push('/vendor/dashboard')
				})
			} catch (error) {
				console.log('try catch failure')
			}      
		} else this.setState({ disablebtn: 'outer-btn' })
	}

	componentDidMount () {
    let userInfo = JSON.parse(localStorage.getItem('userInfo'))
    if (userInfo) {
      if (this.state.accountType !== 'vendor') this.props.history.push('/')
      else if (this.state.accountType === 'vendor') {
        this.setState({ profileName: userInfo.firstName + ' ' + userInfo.lastName })
        this.setState({ profileIcon: userInfo.firstName.charAt(0) + userInfo.lastName.charAt(0) })
      }
    } else this.props.history.push('/')
	}

  render () {
		let formFields = this.state.formFields // form fields state
		let formErrors = this.state.formErrors // form errors state
		
		// form fields state array of objects
		let { companyName, businessNature, regNumber, regAddress, regCity, regPincode, regPhone, regFax, regEmail, regWebsite } = formFields.companyDetails
		let { commAddress, commCity, commPhone, commPincode, commEmail, commWebsite, commFax, commWorkingDays, commWorkingHours } = formFields.commDetails
		let { bankName, bankCustName, bankAccountNumber, bankAccountType, bankTransMode, bankMircCode, bankAddress, bankBranchCode } = formFields.bankDetails
		let { statPanNo, statGSTIn, statLUTNo, statMSMERegNo } = formFields.statutoryDetails
		let { partnerName, partnerDesig, partnerPhone, partnerMobile, partnerEmail } = formFields.partnerDetails
		let { repName, repDesig, repPhone, repMobile, repEmail, repWorkDays, repWorkHours, repWorkFromTime, repWorkToTime } = formFields.repDetails
		let { afRepName, afRepDesig, afRepPhone, afRepMobile, afRepEmail, afRepWorkDays, afRepWorkHours, afRepWorkFromTime, afRepWorkToTime } = formFields.afRepDetails
		let { crcmName, crcmDesig, crcmPhone, crcmMobile, crcmEmail, crcmWorkDays, crcmWorkHours, crcmWorkFromTime, crcmWorkToTime } = formFields.crcmDetails

		return (
			<div id='ven-reg-form'>
				{/* Header */}
				<Header profileName={this.state.profileName} profileIcon={this.state.profileIcon} />

				{/* Main Content */}
				<div className='vrf-container'>
					<div className='vrf-block'>
						<h1 className='vrf-title'>Vendor Registration Form</h1>
						{/* Company details block */}
						<div className='comp-det-block'>
							{/* Company Name */}
							<div className='input-container'>
                <div className='input-groups'>
                  <label className='field-caption'>Company Name*</label>
									<div className='out-ex-size'>
										<input type='text' className={formErrors.companyDetails.companyName.length > 0 ? 'form-field ex-size error' : 'form-field ex-size'} autoComplete='off' data-type='string' name='companyName' value={companyName} placeholder='Enter Company Name' onChange={(e) => this.handleInputChange(e, formFields.companyDetails, formErrors.companyDetails)} maxLength='50' />
										{formErrors.companyDetails.companyName.length > 0 && <div className='error-title'>*Please enter company name</div>}
									</div>               
                </div>
              </div>
							{/* Legal status */}
							<div className='input-container radio-sec mar-bot'>
								<div className='input-groups radio-field'>
									<label className='field-caption no-pad'>Legal Status*</label>
									<div className='radio-input-section'>
										<label className='radio-container' onClick={() => this.legalStatusOnChange('Sole Proprietorship')}><input type='radio' className='form-field-radio' value='Sole Proprietorship' name='legalStatus' />Sole Proprietorship<span className={'checkmark ' + formErrors.companyDetails.legalStatus} /></label>
										<label className='radio-container' onClick={() => this.legalStatusOnChange('Limited Co')}><input type='radio' className='form-field-radio' value='Limited Co' name='legalStatus' />Limited Co.<span className={'checkmark ' + formErrors.companyDetails.legalStatus} /></label>
										<label className='radio-container' onClick={() => this.legalStatusOnChange('Partnership Firm')}><input type='radio' className='form-field-radio' value='Partnership Firm' name='legalStatus' />Partnership Firm<span className={'checkmark ' + formErrors.companyDetails.legalStatus} /></label>
									</div>
								</div>
							</div>
							<div className='out-radio-ex-size'>
								{formErrors.companyDetails.legalStatus.length > 0 && <div className='error-title'>*Please choose company legal status</div>}
							</div>               
							{/* Nature of Business */}
							<div className='input-container'>
                <div className='input-groups'>
                  <label className='field-caption'>Nature of Business*</label>
									<div className='out-ex-size'>
	                  <input type='text' className={formErrors.companyDetails.businessNature.length > 0 ? 'form-field ex-size error' : 'form-field ex-size'} autoComplete='off' data-type='string' name='businessNature' value={businessNature} placeholder='Enter Nature of Business' onChange={(e) => this.handleInputChange(e, formFields.companyDetails, formErrors.companyDetails)} maxLength='50' />
										{formErrors.companyDetails.businessNature.length > 0 && <div className='error-title'>*Please enter nature of business</div>}
									</div>                               
								</div>
              </div>
							{/* Business Registration No */}
							<div className='input-container'>
                <div className='input-groups'>
                  <label className='field-caption'>Business Registration No*</label>
									<div className='out-ex-size'>
	                  <input type='text' className={formErrors.companyDetails.regNumber.length > 0 ? 'form-field ex-size error' : 'form-field ex-size'} autoComplete='off' data-type='string' name='regNumber' value={regNumber} placeholder='Enter Business Registration No' onChange={(e) => this.handleInputChange(e, formFields.companyDetails, formErrors.companyDetails)} maxLength='50' />
										{formErrors.companyDetails.regNumber.length > 0 && <div className='error-title'>*Please enter business registration number</div>}
									</div>                               
                </div>
              </div>
							{/* Business Registration Address */}
							<div className='input-container textarea-sec'>
								<div className='input-groups input-textarea'>
									<label className='field-caption note-filed'>Business Registration<br />Address*</label>
									<div className='out-ex-size'>
										<textarea className={formErrors.companyDetails.regAddress.length > 0 ? 'cls-notes error' : 'cls-notes'} autoComplete='false' data-type='string' name='regAddress' value={regAddress} placeholder='Enter Business Registration Address' onChange={(e) => this.handleInputChange(e, formFields.companyDetails, formErrors.companyDetails)} maxLength='250' />
										{formErrors.companyDetails.regAddress.length > 0 && <div className='error-title'>*Please enter business registration address</div>}
									</div>                               								
								</div>
							</div>
							<div className='flex-box'>
								{/* City */}
								<div className='input-container'>
									<div className='input-groups'>
										<label className='field-caption'>City*</label>
										<div className='out-ex-size'>
											<input type='text' className={formErrors.companyDetails.regCity.length > 0 ? 'form-field error' : 'form-field'} autoComplete='off' data-type='city' name='regCity' value={regCity} placeholder='Enter City' onChange={(e) => this.handleInputChange(e, formFields.companyDetails, formErrors.companyDetails)} maxLength='30' />
											{formErrors.companyDetails.regCity.length > 0 && <div className='error-title'>*Please enter city</div>}
										</div>                               																	
									</div>
								</div>
								{/* Pincode */}
								<div className='input-container'>
									<div className='input-groups'>
										<label className='field-caption'>Pincode*</label>
										<div className='out-ex-size'>
											<input type='text' className={formErrors.companyDetails.regPincode.length > 0 ? 'form-field error' : 'form-field'} autoComplete='off' data-type='pincode' name='regPincode' value={regPincode} placeholder='Enter Pincode' onChange={(e) => this.handleInputChange(e, formFields.companyDetails, formErrors.companyDetails)} maxLength='10' />
											{formErrors.companyDetails.regPincode.length > 0 && <div className='error-title'>*Please enter pincode</div>}
										</div>                               																	
									</div>
								</div>
							</div>
							<div className='flex-box'>
								{/* Phone */}
								<div className='input-container'>
									<div className='input-groups'>
										<label className='field-caption'>Phone*</label>
										<div className='out-ex-size'>
											<input type='text' className={formErrors.companyDetails.regPhone.length > 0 ? 'form-field error' : 'form-field'} autoComplete='off' data-type='phone' name='regPhone' value={regPhone} placeholder='Enter Phone Number' onChange={(e) => this.handleInputChange(e, formFields.companyDetails, formErrors.companyDetails)} onBlur={(e) => this.handleInputFocusExit(e, formFields.companyDetails, formErrors.companyDetails)} maxLength='15' />
											{formErrors.companyDetails.regPhone.length > 0 && <div className='error-title'>{formErrors.companyDetails.regPhone.length > 5 ? formErrors.companyDetails.regPhone : '*Please enter phone number'}</div>}
										</div>
									</div>
								</div>
								{/* Fax */}
								<div className='input-container'>
									<div className='input-groups'>
										<label className='field-caption'>Fax*</label>
										<div className='out-ex-size'>
											<input type='text' className={formErrors.companyDetails.regFax.length > 0 ? 'form-field error' : 'form-field'} autoComplete='off' data-type='fax' name='regFax' value={regFax} placeholder='Enter Fax Number' onChange={(e) => this.handleInputChange(e, formFields.companyDetails, formErrors.companyDetails)} maxLength='15' />
											{formErrors.companyDetails.regFax.length > 0 && <div className='error-title'>*Please enter fax number</div>}
										</div>
									</div>
								</div>
							</div>
							<div className='flex-box'>
								{/* Email */}
								<div className='input-container'>
									<div className='input-groups'>
										<label className='field-caption'>Email*</label>
										<div className='out-ex-size'>
											<input type='text' className={formErrors.companyDetails.regEmail.length > 0 ? 'form-field error' : 'form-field'} autoComplete='off' data-type='email' name='regEmail' value={regEmail} placeholder='Enter Email Address' onChange={(e) => this.handleInputChange(e, formFields.companyDetails, formErrors.companyDetails)} onBlur={(e) => this.handleInputFocusExit(e, formFields.companyDetails, formErrors.companyDetails)} maxLength='50' />
											{formErrors.companyDetails.regEmail.length > 0 && <div className='error-title'>{formErrors.companyDetails.regEmail.length > 5 ? formErrors.companyDetails.regEmail : '*Please enter email address'}</div>}
										</div>
									</div>
								</div>
								{/* Website */}
								<div className='input-container'>
									<div className='input-groups'>
										<label className='field-caption'>Website*</label>
										<div className='out-ex-size'>
											<input type='text' className={formErrors.companyDetails.regWebsite.length > 0 ? 'form-field error' : 'form-field'} autoComplete='off' data-type='website' name='regWebsite' value={regWebsite} placeholder='Enter Website Address' onChange={(e) => this.handleInputChange(e, formFields.companyDetails, formErrors.companyDetails)} maxLength='50' />
											{formErrors.companyDetails.regWebsite.length > 0 && <div className='error-title'>*Please enter website address</div>}
										</div>								
									</div>
								</div>
							</div>
						</div>
						
						{/* Communication Address & Contact block */}
						<h2 className='vrf-sub-title'>Communication Address &amp; Contact Details </h2>
						<div className='comp-det-block'>
							{/* Address */}
							<div className='input-container textarea-sec'>
								<div className='input-groups input-textarea'>
									<label className='field-caption note-filed'>Business Registration<br />Address*</label>
									<div className='out-ex-size'>
										<textarea className={formErrors.commDetails.commAddress.length > 0 ? 'cls-notes error' : 'cls-notes'} autoComplete='false' data-type='string' name='commAddress' value={commAddress} placeholder='Enter Business Registration Address' onChange={(e) => this.handleInputChange(e, formFields.commDetails, formErrors.commDetails)} maxLength='250' />
										{formErrors.commDetails.commAddress.length > 0 && <div className='error-title'>*Please enter business registration address</div>}
									</div>								
								</div>
							</div>
							<div className='flex-box'>
								{/* City */}
								<div className='input-container'>
									<div className='input-groups'>
										<label className='field-caption'>City*</label>
										<div className='out-ex-size'>
											<input type='text' className={formErrors.commDetails.commCity.length > 0 ? 'form-field error' : 'form-field'} autoComplete='off' data-type='city' name='commCity' value={commCity} placeholder='Enter City' onChange={(e) => this.handleInputChange(e, formFields.commDetails, formErrors.commDetails)} maxLength='30' />
											{formErrors.commDetails.commCity.length > 0 && <div className='error-title'>*Please enter city</div>}
										</div>																	
									</div>
								</div>
								{/* Pincode */}
								<div className='input-container'>
									<div className='input-groups'>
										<label className='field-caption'>Pincode*</label>
										<div className='out-ex-size'>
											<input type='text' className={formErrors.commDetails.commPincode.length > 0 ? 'form-field error' : 'form-field'} autoComplete='off' data-type='pincode' name='commPincode' value={commPincode} placeholder='Enter Pincode' onChange={(e) => this.handleInputChange(e, formFields.commDetails, formErrors.commDetails)} maxLength='10' />
											{formErrors.commDetails.commPincode.length > 0 && <div className='error-title'>*Please enter pincode</div>}
										</div>																	
									</div>
								</div>
							</div>
							<div className='flex-box'>
								{/* Phone */}
								<div className='input-container'>
									<div className='input-groups'>
										<label className='field-caption'>Phone*</label>
										<div className='out-ex-size'>
											<input type='text' className={formErrors.commDetails.commPhone.length > 0 ? 'form-field error' : 'form-field'} autoComplete='off' data-type='phone' name='commPhone' value={commPhone} placeholder='Enter Phone Number' onChange={(e) => this.handleInputChange(e, formFields.commDetails, formErrors.commDetails)} onBlur={(e) => this.handleInputFocusExit(e, formFields.commDetails, formErrors.commDetails)} maxLength='15' />
											{formErrors.commDetails.commPhone.length > 0 && <div className='error-title'>{formErrors.commDetails.commPhone.length > 5 ? formErrors.commDetails.commPhone : '*Please enter phone number'}</div>}
										</div>																	
									</div>
								</div>
								{/* Fax */}
								<div className='input-container'>
									<div className='input-groups'>
										<label className='field-caption'>Fax*</label>
										<div className='out-ex-size'>
											<input type='text' className={formErrors.commDetails.commFax.length > 0 ? 'form-field error' : 'form-field'} autoComplete='off' data-type='fax' name='commFax' value={commFax} placeholder='Enter Fax' onChange={(e) => this.handleInputChange(e, formFields.commDetails, formErrors.commDetails)} maxLength='15' />
											{formErrors.commDetails.commFax.length > 0 && <div className='error-title'>*Please enter fax number</div>}
										</div>																	
									</div>
								</div>
							</div>
							<div className='flex-box'>
								{/* Email */}
								<div className='input-container'>
									<div className='input-groups'>
										<label className='field-caption'>Email*</label>
										<div className='out-ex-size'>
											<input type='text' className={formErrors.commDetails.commEmail.length > 0 ? 'form-field error' : 'form-field'} autoComplete='off' data-type='email' name='commEmail' value={commEmail} placeholder='Enter Email Address' onChange={(e) => this.handleInputChange(e, formFields.commDetails, formErrors.commDetails)} onBlur={(e) => this.handleInputFocusExit(e, formFields.commDetails, formErrors.commDetails)} maxLength='50' />
											{formErrors.commDetails.commEmail.length > 0 && <div className='error-title'>{formErrors.commDetails.commEmail.length > 5 ? formErrors.commDetails.commEmail : '*Please enter email address'}</div>}
										</div>																										
									</div>
								</div>
								{/* Website */}
								<div className='input-container'>
									<div className='input-groups'>
										<label className='field-caption'>Website*</label>
										<div className='out-ex-size'>
											<input type='text' className={formErrors.commDetails.commWebsite.length > 0 ? 'form-field error' : 'form-field'} autoComplete='off' data-type='website' name='commWebsite' value={commWebsite} placeholder='Enter Website Address' onChange={(e) => this.handleInputChange(e, formFields.commDetails, formErrors.commDetails)} maxLength='50' />
											{formErrors.commDetails.commWebsite.length > 0 && <div className='error-title'>*Please enter website address</div>}
										</div>																																			
									</div>
								</div>
							</div>
							<div className='flex-box'>
								{/* Working Days */}
								<div className='input-container'>
									<div className='input-groups'>
										<label className='field-caption'>Working Days*</label>
										<div className='out-ex-size'>
											<input type='text' className={formErrors.commDetails.commWorkingDays.length > 0 ? 'form-field error' : 'form-field'} autoComplete='off' data-type='number' name='commWorkingDays' value={commWorkingDays} placeholder='Enter Working Days' onChange={(e) => this.handleInputChange(e, formFields.commDetails, formErrors.commDetails)} maxLength='3' />
											{formErrors.commDetails.commWorkingDays.length > 0 && <div className='error-title'>*Please enter working days</div>}
										</div>																																												
									</div>
								</div>
								{/* Working Hours */}
								<div className='input-container'>
									<div className='input-groups'>
										<label className='field-caption'>Working Hours*</label>
										<div className='out-ex-size'>
											<input type='text' className={formErrors.commDetails.commWorkingHours.length > 0 ? 'form-field error' : 'form-field'} autoComplete='off' data-type='number' name='commWorkingHours' value={commWorkingHours} placeholder='Enter Working Hours' onChange={(e) => this.handleInputChange(e, formFields.commDetails, formErrors.commDetails)} maxLength='3' />
											{formErrors.commDetails.commWorkingHours.length > 0 && <div className='error-title'>*Please enter working hours</div>}
										</div>																																																					
									</div>
								</div>
							</div>
						</div>
						
						{/* Bank Details block */}
						<h2 className='vrf-sub-title'>Bank Details </h2>
						<div className='comp-det-block'>
							{/* Name of the Bank */}
							<div className='input-container'>
                <div className='input-groups'>
                  <label className='field-caption'>Name of the Bank*</label>
									<div className='out-ex-size'>
	                  <input type='text' className={formErrors.bankDetails.bankName.length > 0 ? 'form-field ex-size error' : 'form-field ex-size'} autoComplete='off' data-type='string' name='bankName' value={bankName} placeholder='Enter Name of the Bank' onChange={(e) => this.handleInputChange(e, formFields.bankDetails, formErrors.bankDetails)} maxLength='30' />
										{formErrors.bankDetails.bankName.length > 0 && <div className='error-title'>*Please enter name of the bank</div>}
									</div>																																																					                
								</div>
              </div>
							{/* Customer Name */}
							<div className='input-container'>
                <div className='input-groups'>
                  <label className='field-caption no-pad'>Customer Name*<br />(as per bank)</label>
									<div className='out-ex-size'>
	                  <input type='text' className={formErrors.bankDetails.bankCustName.length > 0 ? 'form-field ex-size error' : 'form-field ex-size'} autoComplete='off' data-type='string' name='bankCustName' value={bankCustName} placeholder='Enter Customer Name' onChange={(e) => this.handleInputChange(e, formFields.bankDetails, formErrors.bankDetails)} maxLength='50' />
										{formErrors.bankDetails.bankCustName.length > 0 && <div className='error-title'>*Please enter customer name</div>}
									</div>																																																					                
                </div>
              </div>
							<div className='flex-box'>
								{/* Account No  */}
								<div className='input-container'>
									<div className='input-groups'>
										<label className='field-caption'>Account No*</label>
										<div className='out-ex-size'>
											<input type='text' className={formErrors.bankDetails.bankAccountNumber.length > 0 ? 'form-field error' : 'form-field'} autoComplete='off' data-type='number' name='bankAccountNumber' value={bankAccountNumber} placeholder='Enter Account Number' onChange={(e) => this.handleInputChange(e, formFields.bankDetails, formErrors.bankDetails)} maxLength='20' />
											{formErrors.bankDetails.bankAccountNumber.length > 0 && <div className='error-title'>*Please enter bank account number</div>}
										</div>																																																					                								
									</div>
								</div>
								{/* Account Type */}
								<div className='input-container'>
									<div className='input-groups'>
										<label className='field-caption'>Account Type*</label>
										<div className='out-ex-size'>
											<input type='text' className={formErrors.bankDetails.bankAccountType.length > 0 ? 'form-field error' : 'form-field'} autoComplete='off' data-type='string' name='bankAccountType' value={bankAccountType} placeholder='Enter Account Type' onChange={(e) => this.handleInputChange(e, formFields.bankDetails, formErrors.bankDetails)} maxLength='20' />
											{formErrors.bankDetails.bankAccountType.length > 0 && <div className='error-title'>*Please enter bank account type</div>}
										</div>																																																					                								
									</div>
								</div>
							</div>
							<div className='flex-box'>
								{/* NEFT / RTGS / IFSC CODE */}
								<div className='input-container'>
									<div className='input-groups'>
										<label className='field-caption'>NEFT / RTGS / IFSC CODE*</label>
										<div className='out-ex-size'>
											<input type='text' className={formErrors.bankDetails.bankTransMode.length > 0 ? 'form-field error' : 'form-field'} autoComplete='off' data-type='string' name='bankTransMode' value={bankTransMode} placeholder='Enter NEFT / RTGS / IFSC Code' onChange={(e) => this.handleInputChange(e, formFields.bankDetails, formErrors.bankDetails)} maxLength='20' />
											{formErrors.bankDetails.bankTransMode.length > 0 && <div className='error-title'>*Please enter NEFT / RTGS / IFSC Code</div>}
										</div>																																																					                								
									</div>
								</div>
								{/* MIRC Code */}
								<div className='input-container'>
									<div className='input-groups'>
										<label className='field-caption'>MIRC Code*</label>
										<div className='out-ex-size'>
											<input type='text' className={formErrors.bankDetails.bankMircCode.length > 0 ? 'form-field error' : 'form-field'} autoComplete='off' data-type='string' name='bankMircCode' value={bankMircCode} placeholder='Enter MIRC Code' onChange={(e) => this.handleInputChange(e, formFields.bankDetails, formErrors.bankDetails)} maxLength='20' />
											{formErrors.bankDetails.bankMircCode.length > 0 && <div className='error-title'>*Please enter MIRC code</div>}
										</div>																																																					                																	
									</div>
								</div>
							</div>
							<div className='flex-box'>
								{/* Branch Address */}
								<div className='input-container'>
									<div className='input-groups'>
										<label className='field-caption'>Branch Address*</label>
										<div className='out-ex-size'>
											<input type='text' className={formErrors.bankDetails.bankAddress.length > 0 ? 'form-field error' : 'form-field'} autoComplete='off' data-type='string' name='bankAddress' value={bankAddress} placeholder='Enter Branch Address' onChange={(e) => this.handleInputChange(e, formFields.bankDetails, formErrors.bankDetails)} maxLength='250' />
											{formErrors.bankDetails.bankAddress.length > 0 && <div className='error-title'>*Please enter branch address</div>}
										</div>																																																					                																										
									</div>
								</div>
								{/* Branch Code */}
								<div className='input-container'>
									<div className='input-groups'>
										<label className='field-caption'>Branch Code*</label>
										<div className='out-ex-size'>
											<input type='text' className={formErrors.bankDetails.bankBranchCode.length > 0 ? 'form-field error' : 'form-field'} autoComplete='off' data-type='string' name='bankBranchCode' value={bankBranchCode} placeholder='Enter Branch Code' onChange={(e) => this.handleInputChange(e, formFields.bankDetails, formErrors.bankDetails)} maxLength='20' />
											{formErrors.bankDetails.bankBranchCode.length > 0 && <div className='error-title'>*Please enter branch code</div>}
										</div>																																																					                																										
									</div>
								</div>
							</div>
						</div>
						
						{/* Statutory Details block */}
						<h2 className='vrf-sub-title'>Statutory Details </h2>
						<div className='comp-det-block'>
							{/* PAN No.(Mandatory) */}
							<div className='input-container'>
                <div className='input-groups'>
                  <label className='field-caption'>PAN No*</label>
									<div className='out-ex-size'>
	                  <input type='text' className={formErrors.statutoryDetails.statPanNo.length > 0 ? 'form-field ex-size error' : 'form-field ex-size'} autoComplete='off' data-type='string' name='statPanNo' value={statPanNo} placeholder='Enter PAN Number' onChange={(e) => this.handleInputChange(e, formFields.statutoryDetails, formErrors.statutoryDetails)} maxLength='10' />
										{formErrors.statutoryDetails.statPanNo.length > 0 && <div className='error-title'>*Please enter pan number</div>}
									</div>																																																					                																	                
								</div>
              </div>
							<div className='flex-box'>
								{/* GSTIN  */}
								<div className='input-container'>
									<div className='input-groups'>
										<label className='field-caption'>GSTIN*</label>
										<div className='out-ex-size'>
											<input type='text' className={formErrors.statutoryDetails.statGSTIn.length > 0 ? 'form-field error' : 'form-field'} autoComplete='off' data-type='string' name='statGSTIn' value={statGSTIn} placeholder='Enter GSTIN' onChange={(e) => this.handleInputChange(e, formFields.statutoryDetails, formErrors.statutoryDetails)} maxLength='20' />
											{formErrors.statutoryDetails.statGSTIn.length > 0 && <div className='error-title'>*Please enter GSTIN number</div>}
										</div>																																																					                																	                									
									</div>
								</div>
								{/* LUT date */}
								<div className='input-container'>
									<div className='input-groups'>
										<label className='field-caption'>LUT Date*</label>
										<div className='out-ex-size'>
											<Datetime
												dateFormat={'DD/MM/YYYY'}
												timeFormat={false}
												className={formErrors.statutoryDetails.statLUTDate.length > 0 ? 'date form-field error' : 'date form-field'}
												name='statLUTDate'
												closeOnSelect
												closeOnTab
												inputProps={{ placeholder: 'DD/MM/YYYY', readOnly:true }}
												onChange={this.handleDateChange} />
											{formErrors.statutoryDetails.statLUTDate.length > 0 && <div className='error-title'>*Please select LUT date</div>}
										</div>																																																					                																	                									
									</div>
								</div>
							</div>
							<div className='flex-box'>
								{/* LUT no. */}
								<div className='input-container'>
									<div className='input-groups'>
										<label className='field-caption'>LUT No*</label>
										<div className='out-ex-size'>
											<input type='text' className={formErrors.statutoryDetails.statLUTNo.length > 0 ? 'form-field error' : 'form-field'} autoComplete='off' data-type='string' name='statLUTNo' value={statLUTNo} placeholder='Enter LUT No' onChange={(e) => this.handleInputChange(e, formFields.statutoryDetails, formErrors.statutoryDetails)} maxLength='20' />
											{formErrors.statutoryDetails.statLUTNo.length > 0 && <div className='error-title'>*Please enter LUT number</div>}
										</div>																																																					                																	                																	
									</div>
								</div>
								{/* MSME registration no. (if applicable) */}
								<div className='input-container'>
									<div className='input-groups'>
										<label className='field-caption no-pad'>MSME Registration No<br /> (if applicable)</label>
										<div className='out-ex-size'>
											<input type='text' className='form-field' autoComplete='off' data-type='string' name='statMSMERegNo' value={statMSMERegNo} placeholder='Enter MSME Registration Number' onChange={(e) => this.handleInputChange(e, formFields.statutoryDetails, formErrors.statutoryDetails)} maxLength='20' />
										</div>																																																					                																	                																									
									</div>
								</div>
							</div>
						</div>
						
						{/* Proprietor/Partner/CEO/MD contact details */}
						<h2 className='vrf-sub-title'>Proprietor/Partner/CEO/MD Contact Details </h2>
						<div className='comp-det-block'>
							{/* Name */}
							<div className='input-container'>
                <div className='input-groups'>
                  <label className='field-caption'>Name*</label>
									<div className='out-ex-size'>
	                  <input type='text' className={formErrors.partnerDetails.partnerName.length > 0 ? 'form-field ex-size error' : 'form-field ex-size'} autoComplete='off' data-type='string' name='partnerName' value={partnerName} placeholder='Enter Proprietor/Partner/CEO/MD Name' onChange={(e) => this.handleInputChange(e, formFields.partnerDetails, formErrors.partnerDetails)} maxLength='50' />
										{formErrors.partnerDetails.partnerName.length > 0 && <div className='error-title'>*Please enter Proprietor/Partner/CEO/MD name</div>}
									</div>																																																					                																	                																									                
								</div>
              </div>
							{/* Position  */}
							<div className='input-container'>
								<div className='input-groups'>
									<label className='field-caption'>Position*</label>
									<div className='out-ex-size'>
										<input type='text' className={formErrors.partnerDetails.partnerDesig.length > 0 ? 'form-field ex-size error' : 'form-field ex-size'} autoComplete='off' data-type='string' name='partnerDesig' value={partnerDesig} placeholder='Enter Proprietor/Partner/CEO/MD Position' onChange={(e) => this.handleInputChange(e, formFields.partnerDetails, formErrors.partnerDetails)} maxLength='50' />
										{formErrors.partnerDetails.partnerDesig.length > 0 && <div className='error-title'>*Please enter Proprietor/Partner/CEO/MD position</div>}
									</div>																																																					                																	                																									                							
								</div>
							</div>
							{/* Mobile */}
							<div className='input-container'>
								<div className='input-groups'>
									<label className='field-caption'>Mobile*</label>
									<div className='out-ex-size'>
										<input type='text' className={formErrors.partnerDetails.partnerMobile.length > 0 ? 'form-field ex-size error' : 'form-field ex-size'} autoComplete='off' data-type='mobile' name='partnerMobile' value={partnerMobile} placeholder='Enter Proprietor/Partner/CEO/MD Mobile Number' onChange={(e) => this.handleInputChange(e, formFields.partnerDetails, formErrors.partnerDetails)} onBlur={(e) => this.handleInputFocusExit(e, formFields.partnerDetails, formErrors.partnerDetails)} maxLength='10' />
										{formErrors.partnerDetails.partnerMobile.length > 0 && <div className='error-title'>{formErrors.partnerDetails.partnerMobile.length > 5 ? formErrors.partnerDetails.partnerMobile : '*Please enter Proprietor/Partner/CEO/MD mobile number'}</div>}
									</div>																																																					                																	                																									                															
								</div>
							</div>
							{/* Phone */}
							<div className='input-container'>
								<div className='input-groups'>
									<label className='field-caption'>Phone*</label>
									<div className='out-ex-size'>
										<input type='text' className={formErrors.partnerDetails.partnerPhone.length > 0 ? 'form-field ex-size error' : 'form-field ex-size'} autoComplete='off' data-type='phone' name='partnerPhone' value={partnerPhone} placeholder='Enter Proprietor/Partner/CEO/MD Phone Number' onChange={(e) => this.handleInputChange(e, formFields.partnerDetails, formErrors.partnerDetails)} onBlur={(e) => this.handleInputFocusExit(e, formFields.partnerDetails, formErrors.partnerDetails)} maxLength='15' />
										{formErrors.partnerDetails.partnerPhone.length > 0 && <div className='error-title'>{formErrors.partnerDetails.partnerPhone.length > 5 ? formErrors.partnerDetails.partnerPhone : '*Please enter Proprietor/Partner/CEO/MD phone number'}</div>}
									</div>																																																					                																	                																									                																								
								</div>
							</div>
							{/* E-mail */}
							<div className='input-container'>
								<div className='input-groups'>
									<label className='field-caption'>E-mail*</label>
									<div className='out-ex-size'>
										<input type='text' className={formErrors.partnerDetails.partnerEmail.length > 0 ? 'form-field ex-size error' : 'form-field ex-size'} autoComplete='off' data-type='email' name='partnerEmail' value={partnerEmail} placeholder='Enter Proprietor/Partner/CEO/MD Email Address' onChange={(e) => this.handleInputChange(e, formFields.partnerDetails, formErrors.partnerDetails)} onBlur={(e) => this.handleInputFocusExit(e, formFields.partnerDetails, formErrors.partnerDetails)} maxLength='50' />
										{formErrors.partnerDetails.partnerEmail.length > 0 && <div className='error-title'>{formErrors.partnerDetails.partnerEmail.length > 5 ? formErrors.partnerDetails.partnerEmail : '*Please enter Proprietor/Partner/CEO/MD email address'}</div>}
									</div>																																																					                																	                																									                																															
								</div>
							</div>
						</div>

						{/* Representative contact details */}
						<h2 className='vrf-sub-title'>Representative Contact Details</h2>
						<div className='comp-det-block'>
							{/* Name */}
							<div className='input-container'>
                <div className='input-groups'>
                  <label className='field-caption'>Name*</label>
									<div className='out-ex-size'>
	                  <input type='text' className={formErrors.repDetails.repName.length > 0 ? 'form-field ex-size error' : 'form-field ex-size'} autoComplete='off' data-type='string' name='repName' value={repName} placeholder='Enter Representative Name' onChange={(e) => this.handleInputChange(e, formFields.repDetails, formErrors.repDetails)} maxLength='50' />
										{formErrors.repDetails.repName.length > 0 && <div className='error-title'>*Please enter representative name</div>}
									</div>																																																					                																	                																									                																															                
								</div>
              </div>
							{/* Designation  */}
							<div className='input-container'>
								<div className='input-groups'>
									<label className='field-caption'>Designation*</label>
									<div className='out-ex-size'>
										<input type='text' className={formErrors.repDetails.repDesig.length > 0 ? 'form-field ex-size error' : 'form-field ex-size'} autoComplete='off' data-type='string' name='repDesig' value={repDesig} placeholder='Enter Representative Designation' onChange={(e) => this.handleInputChange(e, formFields.repDetails, formErrors.repDetails)} maxLength='50' />
										{formErrors.repDetails.repDesig.length > 0 && <div className='error-title'>*Please enter representative designation</div>}
									</div>																																																					                																	                																									                																															                								
								</div>
							</div>
							{/* Mobile */}
							<div className='input-container'>
								<div className='input-groups'>
									<label className='field-caption'>Mobile*</label>
									<div className='out-ex-size'>
										<input type='text' className={formErrors.repDetails.repMobile.length > 0 ? 'form-field ex-size error' : 'form-field ex-size'} autoComplete='off' data-type='mobile' name='repMobile' value={repMobile} placeholder='Enter Representative Mobile Number' onChange={(e) => this.handleInputChange(e, formFields.repDetails, formErrors.repDetails)} onBlur={(e) => this.handleInputFocusExit(e, formFields.repDetails, formErrors.repDetails)} maxLength='10' />
										{formErrors.repDetails.repMobile.length > 0 && <div className='error-title'>{formErrors.repDetails.repMobile.length > 5 ? formErrors.repDetails.repMobile : '*Please enter representative mobile number'}</div>}
									</div>																																																					                																	                																									                																															                																
								</div>
							</div>
							{/* Phone */}
							<div className='input-container'>
								<div className='input-groups'>
									<label className='field-caption'>Phone*</label>
									<div className='out-ex-size'>
										<input type='text' className={formErrors.repDetails.repPhone.length > 0 ? 'form-field ex-size error' : 'form-field ex-size'} autoComplete='off' data-type='phone' name='repPhone' value={repPhone} placeholder='Enter Representative Phone Number' onChange={(e) => this.handleInputChange(e, formFields.repDetails, formErrors.repDetails)} onBlur={(e) => this.handleInputFocusExit(e, formFields.repDetails, formErrors.repDetails)} maxLength='15' />
										{formErrors.repDetails.repPhone.length > 0 && <div className='error-title'>{formErrors.repDetails.repPhone.length > 5 ? formErrors.repDetails.repPhone : '*Please enter representative phone number'}</div>}
									</div>																																																					                																	                																									                																															                																								
								</div>
							</div>
							{/* E-mail */}
							<div className='input-container'>
								<div className='input-groups'>
									<label className='field-caption'>E-mail*</label>
									<div className='out-ex-size'>
										<input type='text' className={formErrors.repDetails.repEmail.length > 0 ? 'form-field ex-size error' : 'form-field ex-size'} autoComplete='off' data-type='email' name='repEmail' value={repEmail} placeholder='Enter Representative Email Address' onChange={(e) => this.handleInputChange(e, formFields.repDetails, formErrors.repDetails)} onBlur={(e) => this.handleInputFocusExit(e, formFields.repDetails, formErrors.repDetails)} maxLength='50' />
										{formErrors.repDetails.repEmail.length > 0 && <div className='error-title'>{formErrors.repDetails.repEmail.length > 5 ? formErrors.repDetails.repEmail : '*Please enter representative email address'}</div>}
									</div>																																																					                																	                																									                																															                																															
								</div>
							</div>
							<div className='flex-box'>
								{/* Working Days & Hours */}
								<div className='input-container mar-bot'>
									<div className='input-groups'>
										<label className='field-caption'>Working Days &amp; Hours*</label>
										<input type='text' className={formErrors.repDetails.repWorkDays.length > 0 ? 'form-field dayhr-field error' : 'form-field dayhr-field'} autoComplete='off' data-type='number' name='repWorkDays' value={repWorkDays} placeholder='Enter Working Days' onChange={(e) => this.handleInputChange(e, formFields.repDetails, formErrors.repDetails)} maxLength='3' />
									</div>
								</div>
								{/* Hours */}
								<div className='input-container mar-bot'>
									<div className='input-groups'>
										<input type='text' className={formErrors.repDetails.repWorkHours.length > 0 ? 'form-field dayhr-field error' : 'form-field dayhr-field'} autoComplete='off' data-type='number' name='repWorkHours' value={repWorkHours} placeholder='Enter Working Hours' onChange={(e) => this.handleInputChange(e, formFields.repDetails, formErrors.repDetails)} maxLength='3' />
									</div>
								</div>
								{/* AM */}
								<div className='input-container mar-bot'>
									<div className='input-groups'>
										<input type='text' className={formErrors.repDetails.repWorkFromTime.length > 0 ? 'form-field ampm-field error' : 'form-field ampm-field'} autoComplete='off' data-type='number' name='repWorkFromTime' value={repWorkFromTime} placeholder='From Time' onChange={(e) => this.handleInputChange(e, formFields.repDetails, formErrors.repDetails)} maxLength='5' />
										<label className='field-caption ampm-size'>AM</label>
									</div>
								</div>
								{/* PM */}
								<div className='input-container mar-bot'>
									<div className='input-groups'>
										<input type='text' className={formErrors.repDetails.repWorkToTime.length > 0 ? 'form-field ampm-field error' : 'form-field ampm-field'} autoComplete='off' data-type='number' name='repWorkToTime' value={repWorkToTime} placeholder='To Time' onChange={(e) => this.handleInputChange(e, formFields.repDetails, formErrors.repDetails)} maxLength='5' />
										<label className='field-caption ampm-size'>PM</label>
									</div>
								</div>
							</div>
							<div className='pad-lft'>
								{(formErrors.repDetails.repWorkDays.length > 0 && formErrors.repDetails.repWorkHours.length > 0 && formErrors.repDetails.repWorkFromTime.length > 0 && formErrors.repDetails.repWorkToTime.length > 0)
									? <div className='error-title'>*Please enter working days, hours, from time and to time</div>
									: formErrors.repDetails.repWorkDays.length > 0
										? <div className='error-title'>*Please enter working days</div>
										: formErrors.repDetails.repWorkHours.length > 0
											? <div className='error-title'>*Please enter working hours</div>
											: formErrors.repDetails.repWorkFromTime.length > 0
												? <div className='error-title'>*Please enter working from time</div>
												: formErrors.repDetails.repWorkToTime.length > 0
													? <div className='error-title'>*Please enter working to time</div>
													: ''
								}
							</div>																																																					                																	                																									                																															                																															
						</div>
						
						{/* Accounts and Finance Representative Contact Details */}
						<h2 className='vrf-sub-title'>Accounts and Finance Representative Contact Details</h2>
						<div className='comp-det-block'>
							{/* Name */}
							<div className='input-container'>
                <div className='input-groups'>
                  <label className='field-caption'>Name*</label>
									<div className='out-ex-size'>
	                  <input type='text' className={formErrors.afRepDetails.afRepName.length > 0 ? 'form-field ex-size error' : 'form-field ex-size'} autoComplete='off' data-type='string' name='afRepName' value={afRepName} placeholder='Enter Representative Name' onChange={(e) => this.handleInputChange(e, formFields.afRepDetails, formErrors.afRepDetails)} maxLength='50' />
										{formErrors.afRepDetails.afRepName.length > 0 && <div className='error-title'>*Please enter accounts and finance representative name</div>}
									</div>																																																					                																	                																									                																															                																															                
								</div>
              </div>
							{/* Designation  */}
							<div className='input-container'>
								<div className='input-groups'>
									<label className='field-caption'>Designation*</label>
									<div className='out-ex-size'>
										<input type='text' className={formErrors.afRepDetails.afRepDesig.length > 0 ? 'form-field ex-size error' : 'form-field ex-size'} autoComplete='off' data-type='string' name='afRepDesig' value={afRepDesig} placeholder='Enter Representative Designation' onChange={(e) => this.handleInputChange(e, formFields.afRepDetails, formErrors.afRepDetails)} maxLength='50' />
										{formErrors.afRepDetails.afRepDesig.length > 0 && <div className='error-title'>*Please enter accounts and finance representative designation</div>}
									</div>																																																					                																	                																									                																															                																															                								
								</div>
							</div>
							{/* Mobile */}
							<div className='input-container'>
								<div className='input-groups'>
									<label className='field-caption'>Mobile*</label>
									<div className='out-ex-size'>
										<input type='text' className={formErrors.afRepDetails.afRepMobile.length > 0 ? 'form-field ex-size error' : 'form-field ex-size'} autoComplete='off' data-type='mobile' name='afRepMobile' value={afRepMobile} placeholder='Enter Representative Mobile Number' onChange={(e) => this.handleInputChange(e, formFields.afRepDetails, formErrors.afRepDetails)} onBlur={(e) => this.handleInputFocusExit(e, formFields.afRepDetails, formErrors.afRepDetails)} maxLength='10' />
										{formErrors.afRepDetails.afRepMobile.length > 0 && <div className='error-title'>{formErrors.afRepDetails.afRepMobile.length > 5 ? formErrors.afRepDetails.afRepMobile : '*Please enter accounts and finance representative mobile number'}</div>}
									</div>																																																					                																	                																									                																															                																															                																
								</div>
							</div>
							{/* Phone */}
							<div className='input-container'>
								<div className='input-groups'>
									<label className='field-caption'>Phone*</label>
									<div className='out-ex-size'>
										<input type='text' className={formErrors.afRepDetails.afRepPhone.length > 0 ? 'form-field ex-size error' : 'form-field ex-size'} autoComplete='off' data-type='phone' name='afRepPhone' value={afRepPhone} placeholder='Enter Representative Phone' onChange={(e) => this.handleInputChange(e, formFields.afRepDetails, formErrors.afRepDetails)} onBlur={(e) => this.handleInputFocusExit(e, formFields.afRepDetails, formErrors.afRepDetails)} maxLength='15' />
										{formErrors.afRepDetails.afRepPhone.length > 0 && <div className='error-title'>{formErrors.afRepDetails.afRepPhone.length > 5 ? formErrors.afRepDetails.afRepPhone : '*Please enter accounts and finance representative phone number'}</div>}
									</div>																																																					                																	                																									                																															                																															                																								
								</div>
							</div>
							{/* E-mail */}
							<div className='input-container'>
								<div className='input-groups'>
									<label className='field-caption'>E-mail*</label>
									<div className='out-ex-size'>
										<input type='text' className={formErrors.afRepDetails.afRepEmail.length > 0 ? 'form-field ex-size error' : 'form-field ex-size'} autoComplete='off' data-type='email' name='afRepEmail' value={afRepEmail} placeholder='Enter Representative Email Address ' onChange={(e) => this.handleInputChange(e, formFields.afRepDetails, formErrors.afRepDetails)} onBlur={(e) => this.handleInputFocusExit(e, formFields.afRepDetails, formErrors.afRepDetails)} maxLength='50' />
										{formErrors.afRepDetails.afRepEmail.length > 0 && <div className='error-title'>{formErrors.afRepDetails.afRepEmail.length > 5 ? formErrors.afRepDetails.afRepEmail : '*Please enter accounts and finance representative email address'}</div>}
									</div>																																																					                																	                																									                																															                																															                																																
								</div>
							</div>
							<div className='flex-box'>
								{/* Working Days & Hours */}
								<div className='input-container mar-bot'>
									<div className='input-groups'>
										<label className='field-caption'>Working Days &amp; Hours*</label>
										<input type='text' className={formErrors.afRepDetails.afRepWorkDays.length > 0 ? 'form-field dayhr-field error' : 'form-field dayhr-field'} autoComplete='off' data-type='number' name='afRepWorkDays' value={afRepWorkDays} placeholder='Enter Working Days' onChange={(e) => this.handleInputChange(e, formFields.afRepDetails, formErrors.afRepDetails)} maxLength='3' />
									</div>
								</div>
								{/* Hours */}
								<div className='input-container mar-bot'>
									<div className='input-groups'>
										<input type='text' className={formErrors.afRepDetails.afRepWorkHours.length > 0 ? 'form-field dayhr-field error' : 'form-field dayhr-field'} autoComplete='off' data-type='number' name='afRepWorkHours' value={afRepWorkHours} placeholder='Enter Working Hours' onChange={(e) => this.handleInputChange(e, formFields.afRepDetails, formErrors.afRepDetails)} maxLength='3' />
									</div>
								</div>
								{/* AM */}
								<div className='input-container mar-bot'>
									<div className='input-groups'>
										<input type='text' className={formErrors.afRepDetails.afRepWorkFromTime.length > 0 ? 'form-field ampm-field error' : 'form-field ampm-field'} autoComplete='off' data-type='number' name='afRepWorkFromTime' value={afRepWorkFromTime} placeholder='From Time' onChange={(e) => this.handleInputChange(e, formFields.afRepDetails, formErrors.afRepDetails)} maxLength='5' />
										<label className='field-caption ampm-size'>AM</label>
									</div>
								</div>
								{/* PM */}
								<div className='input-container mar-bot'>
									<div className='input-groups'>
										<input type='text' className={formErrors.afRepDetails.afRepWorkToTime.length > 0 ? 'form-field ampm-field error' : 'form-field ampm-field'} autoComplete='off' data-type='number' name='afRepWorkToTime' value={afRepWorkToTime} placeholder='To Time' onChange={(e) => this.handleInputChange(e, formFields.afRepDetails, formErrors.afRepDetails)} maxLength='5' />
										<label className='field-caption ampm-size'>PM</label>
									</div>
								</div>
							</div>
							<div className='pad-lft'>
								{(formErrors.afRepDetails.afRepWorkDays.length > 0 && formErrors.afRepDetails.afRepWorkHours.length > 0 && formErrors.afRepDetails.afRepWorkFromTime.length > 0 && formErrors.afRepDetails.afRepWorkToTime.length > 0)
									? <div className='error-title'>*Please enter working days, hours, from time and to time</div>
									: formErrors.afRepDetails.afRepWorkDays.length > 0
										? <div className='error-title'>*Please enter working days</div>
										: formErrors.afRepDetails.afRepWorkHours.length > 0
											? <div className='error-title'>*Please enter working hours</div>
											: formErrors.afRepDetails.afRepWorkFromTime.length > 0
												? <div className='error-title'>*Please enter working from time</div>
												: formErrors.afRepDetails.afRepWorkToTime.length > 0
													? <div className='error-title'>*Please enter working to time</div>
													: ''
								}
							</div>																																																					                																	                																									                																															                																															
						</div>

						{/* Customer Relationship / Compliance Manager Contact details */}
						<h2 className='vrf-sub-title'>Customer Relationship / Compliance Manager Contact details</h2>
						<div className='comp-det-block'>
							{/* Name */}
							<div className='input-container'>
                <div className='input-groups'>
                  <label className='field-caption'>Name*</label>
									<div className='out-ex-size'>
	                  <input type='text' className={formErrors.crcmDetails.crcmName.length > 0 ? 'form-field ex-size error' : 'form-field ex-size'} autoComplete='off' data-type='string' name='crcmName' value={crcmName} placeholder='Enter Representative Name' onChange={(e) => this.handleInputChange(e, formFields.crcmDetails, formErrors.crcmDetails)} maxLength='50' />
										{formErrors.crcmDetails.crcmName.length > 0 && <div className='error-title'>*Please enter customer relationship / compliance manager name</div>}
									</div>																																																					                																	                																									                																															                																															                              
								</div>
              </div>
							{/* Designation  */}
							<div className='input-container'>
								<div className='input-groups'>
									<label className='field-caption'>Designation*</label>
									<div className='out-ex-size'>
										<input type='text' className={formErrors.crcmDetails.crcmDesig.length > 0 ? 'form-field ex-size error' : 'form-field ex-size'} autoComplete='off' data-type='string' name='crcmDesig' value={crcmDesig} placeholder='Enter Representative Designation' onChange={(e) => this.handleInputChange(e, formFields.crcmDetails, formErrors.crcmDetails)} maxLength='50' />
										{formErrors.crcmDetails.crcmDesig.length > 0 && <div className='error-title'>*Please enter customer relationship / compliance manager designation</div>}
									</div>																																																					                																	                																									                																															                																															                              							
								</div>
							</div>
							{/* Mobile */}
							<div className='input-container'>
								<div className='input-groups'>
									<label className='field-caption'>Mobile*</label>
									<div className='out-ex-size'>
										<input type='text' className={formErrors.crcmDetails.crcmMobile.length > 0 ? 'form-field ex-size error' : 'form-field ex-size'} autoComplete='off' data-type='mobile' name='crcmMobile' value={crcmMobile} placeholder='Enter Representative Mobile' onChange={(e) => this.handleInputChange(e, formFields.crcmDetails, formErrors.crcmDetails)} onBlur={(e) => this.handleInputFocusExit(e, formFields.crcmDetails, formErrors.crcmDetails)} maxLength='10' />
										{formErrors.crcmDetails.crcmMobile.length > 0 && <div className='error-title'>{formErrors.crcmDetails.crcmMobile.length > 5 ? formErrors.crcmDetails.crcmMobile : '*Please enter customer relationship / compliance manager mobile number'}</div>}
									</div>																																																					                																	                																									                																															                																															                              														
								</div>
							</div>
							{/* Phone */}
							<div className='input-container'>
								<div className='input-groups'>
									<label className='field-caption'>Phone*</label>
									<div className='out-ex-size'>
										<input type='text' className={formErrors.crcmDetails.crcmPhone.length > 0 ? 'form-field ex-size error' : 'form-field ex-size'} autoComplete='off' data-type='phone' name='crcmPhone' value={crcmPhone} placeholder='Enter Representative Phone Number' onChange={(e) => this.handleInputChange(e, formFields.crcmDetails, formErrors.crcmDetails)} onBlur={(e) => this.handleInputFocusExit(e, formFields.crcmDetails, formErrors.crcmDetails)} maxLength='15' />
										{formErrors.crcmDetails.crcmPhone.length > 0 && <div className='error-title'>{formErrors.crcmDetails.crcmPhone.length > 5 ? formErrors.crcmDetails.crcmPhone : '*Please enter customer relationship / compliance manager phone number'}</div>}
									</div>																																																					                																	                																									                																															                																															                              														
								</div>
							</div>
							{/* E-mail */}
							<div className='input-container'>
								<div className='input-groups'>
									<label className='field-caption'>E-mail*</label>
									<div className='out-ex-size'>
										<input type='text' className={formErrors.crcmDetails.crcmEmail.length > 0 ? 'form-field ex-size error' : 'form-field ex-size'} autoComplete='off' data-type='email' name='crcmEmail' value={crcmEmail} placeholder='Enter Representative Email Address' onChange={(e) => this.handleInputChange(e, formFields.crcmDetails, formErrors.crcmDetails)} onBlur={(e) => this.handleInputFocusExit(e, formFields.crcmDetails, formErrors.crcmDetails)} maxLength='50' />
										{formErrors.crcmDetails.crcmEmail.length > 0 && <div className='error-title'>{formErrors.crcmDetails.crcmEmail.length > 5 ? formErrors.crcmDetails.crcmEmail : '*Please enter customer relationship / compliance manager email address'}</div>}
									</div>
								</div>
							</div>
							<div className='flex-box'>
								{/* Working Days & Hours */}
								<div className='input-container mar-bot'>
									<div className='input-groups'>
										<label className='field-caption'>Working Days &amp; Hours*</label>
										<input type='text' className={formErrors.crcmDetails.crcmWorkDays.length > 0 ? 'form-field dayhr-field error' : 'form-field dayhr-field'} autoComplete='off' data-type='number' name='crcmWorkDays' value={crcmWorkDays} placeholder='Enter Working Days' onChange={(e) => this.handleInputChange(e, formFields.crcmDetails, formErrors.crcmDetails)} maxLength='3' />
									</div>
								</div>
								{/* Hours */}
								<div className='input-container mar-bot'>
									<div className='input-groups'>
										<input type='text' className={formErrors.crcmDetails.crcmWorkHours.length > 0 ? 'form-field dayhr-field error' : 'form-field dayhr-field'} autoComplete='off' data-type='number' name='crcmWorkHours' value={crcmWorkHours} placeholder='Enter Working Hours' onChange={(e) => this.handleInputChange(e, formFields.crcmDetails, formErrors.crcmDetails)} maxLength='3' />
									</div>
								</div>
								{/* AM */}
								<div className='input-container mar-bot'>
									<div className='input-groups'>
										<input type='text' className={formErrors.crcmDetails.crcmWorkFromTime.length > 0 ? 'form-field ampm-field error' : 'form-field ampm-field'} autoComplete='off' data-type='number' name='crcmWorkFromTime' value={crcmWorkFromTime} placeholder='From Time' onChange={(e) => this.handleInputChange(e, formFields.crcmDetails, formErrors.crcmDetails)} maxLength='5' />
										<label className='field-caption ampm-size'>AM</label>
									</div>
								</div>
								{/* PM */}
								<div className='input-container mar-bot'>
									<div className='input-groups'>
										<input type='text' className={formErrors.crcmDetails.crcmWorkToTime.length > 0 ? 'form-field ampm-field error' : 'form-field ampm-field'} autoComplete='off' data-type='number' name='crcmWorkToTime' value={crcmWorkToTime} placeholder='To Time' onChange={(e) => this.handleInputChange(e, formFields.crcmDetails, formErrors.crcmDetails)} maxLength='5' />
										<label className='field-caption ampm-size'>PM</label>
									</div>
								</div>
							</div>
							<div className='pad-lft'>
								{(formErrors.crcmDetails.crcmWorkDays.length > 0 && formErrors.crcmDetails.crcmWorkHours.length > 0 && formErrors.crcmDetails.crcmWorkFromTime.length > 0 && formErrors.crcmDetails.crcmWorkToTime.length > 0)
									? <div className='error-title'>*Please enter working days, hours, from time and to time</div>
									: formErrors.crcmDetails.crcmWorkDays.length > 0
										? <div className='error-title'>*Please enter working days</div>
										: formErrors.crcmDetails.crcmWorkHours.length > 0
											? <div className='error-title'>*Please enter working hours</div>
											: formErrors.crcmDetails.crcmWorkFromTime.length > 0
												? <div className='error-title'>*Please enter working from time</div>
												: formErrors.crcmDetails.crcmWorkToTime.length > 0
													? <div className='error-title'>*Please enter working to time</div>
													: ''
								}
							</div>																																																					                																	                																									                																															                																															
						</div>
						
						{/* self-attested  block */}
						<div className='self-attest-title'>Kindly submit the self-attested copy of the following documents along with this form:</div>
						<div className='self-attest-block'>
							{/* Business Registration Certificate */}
							<div className='input-container'>
                <div className='input-groups'>
                  <label className='field-caption ex-size no-pad'>1. Business Registration Certificate*</label>
                  <input type='file' className='cls-file-upload' name='businessRegCert' onChange={this.handleFileUpload} />
                </div>
              </div>
							{/* PAN Card  */}
							<div className='input-container'>
								<div className='input-groups'>
									<label className='field-caption ex-size no-pad'>2. PAN card*</label>
									<input type='file' className='cls-file-upload' name='panCard' onChange={this.handleFileUpload} />
								</div>
							</div>
							{/* One Cancelled Cheque leaf */}
							<div className='input-container'>
								<div className='input-groups'>
									<label className='field-caption ex-size no-pad'>3. One cancelled cheque leaf*</label>
									<input type='file' className='cls-file-upload' name='chequeLeaf' onChange={this.handleFileUpload} />
								</div>
							</div>
							{/* GST certificate */}
							<div className='input-container'>
								<div className='input-groups'>
									<label className='field-caption ex-size no-pad'>4. GST certificate*</label>
									<input type='file' className='cls-file-upload' name='gstCert' onChange={this.handleFileUpload} />
								</div>
							</div>
							{/* MSME registration certificate (if any) */}
							<div className='input-container'>
								<div className='input-groups'>
									<label className='field-caption ex-size no-pad'>5. MSME registration certificate(if any)</label>
									<input type='file' className='cls-file-upload' name='msmeRegCert' onChange={this.handleFileUpload} />
								</div>
							</div>
							{/* TDS low deduction certificate (if any) */}
							<div className='input-container'>
								<div className='input-groups'>
									<label className='field-caption ex-size no-pad'>6. TDS low deduction certificate(if any)</label>
									<input type='file' className='cls-file-upload' name='tdsCert' onChange={this.handleFileUpload} maxLength='50' />
								</div>
							</div>
							{/* Checkbox */}
							<div className='input-container chk-box'>
								<div className='input-groups'>
									<input type='checkbox' className='self-attest-chk-box' name='declaration' value={formFields.declaration} onChange={this.handleCheckBoxChange} />
									<label className='self-attest-chk-box-title'>I hereby declare that the aforementioned information’s are true to the best of my knowledge. I undertake to inform you at the earliest any change in details mentioned above.</label>
								</div>
							</div>
						</div>
						{/* Error message */}
						<div className='cls-err-msg'>{this.state.errMsg}&nbsp;</div>
						{/* Please wait message */}
						{this.state.pleaseWait && <div className='cls-success-msg'>Please wait...</div>}
						{/* Submit button */}
						<div className='form-submit-section'>
							<div className={this.state.disablebtn}>
								<div className='btn register-booking' onClick={this.handleSubmit}>REGISTER</div>
							</div>
						</div>
					</div>
				</div>
			</div>
    )
  }
}
export default withRouter(VendorRegForm)
