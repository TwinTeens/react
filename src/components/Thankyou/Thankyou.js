import React from 'react'
import { withRouter } from 'react-router-dom'
import './Thankyou.css'

class Thankyou extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      title: '',
      subTitle: ''
    }
    this.handleRedirect = this.handleRedirect.bind(this)
  }

  handleRedirect () {
    localStorage.clear()
    this.props.history.push('/')  
  }

  componentDidMount () {
      if (localStorage.getItem('isResetPwd') === 'true') {
        this.setState({ title: 'Reset Password Success' })
        this.setState({ subTitle: 'Thank you! We have sent a reset password acknowledgement to your registered email.' })
      } else if (localStorage.getItem('isResetPwd') === 'false') {
        this.setState({ title: 'Reset Password Link Sent' })
        this.setState({ subTitle: 'Thank you! We have sent an email with password reset link to your registered email address.' })
      } else this.props.history.push('/')
      localStorage.clear()
  }

  render () {
    return (
      <div id='thankyou-block'>
        <div className='thankyou-box'>
          <div className='ic-success' />
          <div className='thankyou-succ-title'>{this.state.title}</div>
          <div className='thankyou-succ-sub-title'>{this.state.subTitle}</div>
          <div className='thankyou-go-back'>Please go back to <span onClick={this.handleRedirect}>Home</span> to login your account.</div>
        </div>
      </div>
    )
  }
}

export default withRouter(Thankyou)
