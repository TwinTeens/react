import React, { Component } from 'react'
import { withRouter, Link } from 'react-router-dom'
import axios from 'axios'
import Datetime from 'react-datetime'
import 'react-datetime/css/react-datetime.css'
import moment from 'moment'
import { Multiselect } from 'multiselect-react-dropdown'
import { validateEmail, serverURL } from '../../global'
import Header from '../Header/Header'

import hsnDataList from '../../hsn.json'
import unspscDataList from '../../unspsc.json'

import './RFPForm.css'

class RFPForm extends Component {
	constructor(props) {
		super(props)
		this.state = {
			accountType: localStorage.getItem('avatarType'),
			errMsg: '',
			disablebtn: 'outer-btn',
			isFormValidationSuccess: true,
			hsnList: [],
			unspscList: [],
			vendorsList: [],
			newVendorsList: [], // to get newly added vendors from input fields
			tempBidEndDate: '', // to get selected bid close date and time in IST format 'Tue Apr 14 2020 20:55:01 GMT+0530 (IST)'
			// to handle Form Fields
			formFields: {
				itemCode: '',
				itemName: '',
				HSNCode: '',
				UNSPSCCode: '',
				docType: 'no',
				itemDesc: '',
				quantity: '',
				bidType: '',
				bidStartDate: moment(new Date()).format('DD/MM/YYYY'),
				bidEndDate: '',
				oldVendorsList: [],
				newVendorsList: [],
				shipAddress: '',
				shipCity: '',
				shipPincode: '',
				shipMobile: '',
				shipAltCont: ''
			},
			// to handle Form Errors
			formErrors: {
				itemCode: '',
				itemName: '',
				HSNCode: '',
				UNSPSCCode: '',
				docType: '',
				itemDesc: '',
				quantity: '',
				bidType: '',
				bidEndDate: '',
				oldVendorsList: '',
				shipAddress: '',
				shipCity: '',
				shipPincode: '',
				shipMobile: '',
				shipAltCont: ''
			},
			proposalDocument: null
		}
		this.handleInputChange = this.handleInputChange.bind(this)
		this.handleInputFocusExit = this.handleInputFocusExit.bind(this)
		this.handleBidTypeOnChange = this.handleBidTypeOnChange.bind(this)
		this.handleDateChange = this.handleDateChange.bind(this)
		this.handleEmptyFormCheck = this.handleEmptyFormCheck.bind(this)
		this.handleNewVendorInputChange = this.handleNewVendorInputChange.bind(this)
		this.handleNewVendorOnBlurChange = this.handleNewVendorOnBlurChange.bind(this)
		this.handleAddNewVendor = this.handleAddNewVendor.bind(this)
		this.handleRemoveNewVendor = this.handleRemoveNewVendor.bind(this)
		this.onSelectVendorsFromList = this.onSelectVendorsFromList.bind(this)
		this.onRemoveVendorsFromList = this.onRemoveVendorsFromList.bind(this)
		this.handleSubmit = this.handleSubmit.bind(this)
		this.getVendorList = this.getVendorList.bind(this)
		this.onSelectHSNCodeFromList = this.onSelectHSNCodeFromList.bind(this)
		this.onRemoveHSNCodeFromList = this.onRemoveHSNCodeFromList.bind(this)
		this.onSelectUNSPSCCodeFromList = this.onSelectUNSPSCCodeFromList.bind(this)
		this.onRemoveUNSPSCCodeFromList = this.onRemoveUNSPSCCodeFromList.bind(this)
		this.onKeyDownListner = this.onKeyDownListner.bind(this)
		this.filterByValue = this.filterByValue.bind(this)
		this.handleFileUpload = this.handleFileUpload.bind(this)
	}

	// Upload proposal document.
	handleFileUpload (e) {
		let fileExt = e.target.files[0].name.split('.')
		let fileType = fileExt[1]
		// console.log('file type', fileType)
		if (fileType === 'pdf' || fileType === 'xlsx' || fileType === 'csv' || fileType === 'doc' || fileType === 'docx') {
			if(this.state.errMsg.length > 0) this.setState({ errMsg: '' })
			this.setState({ proposalDocument: e.target.files[0] })
		} else {
			this.setState({ proposalDocument: null })
			this.setState({ errMsg: '*Please upload a file in pdf/xlsx/docx or csv format.' })
		}
	}
	
	// to handle onblur event for new vendors email address fields.
	handleNewVendorOnBlurChange (e, idx) {
		const newVendors = this.state.newVendorsList.map((value, sidx) => {
			if (idx !== sidx) return value

			let errorMessage = ''

			// email validation
			if (!validateEmail(e.target.value)) errorMessage = 'Invalid email! Please enter valid vendor email address'
			else errorMessage = ''

			this.setState({ isFormValidationSuccess: errorMessage.length > 0 ? false : true })
			return { ...value, newVendorEmail: e.target.value, newVendorEmailError: errorMessage }
		})
		this.setState({ newVendorsList: newVendors })
	}

	// to handle new vendor input field 
	handleNewVendorInputChange (e, idx) {
		const newVendors = this.state.newVendorsList.map((value, sidx) => {
			if (idx !== sidx) return value			
			return { ...value, newVendorEmail: e.target.value, newVendorEmailError: '' }
		})
		this.setState({ newVendorsList: newVendors })
	}

	// to add new vendor
	handleAddNewVendor () {
		this.setState({ newVendorsList: this.state.newVendorsList.concat([{ newVendorEmail: '', newVendorEmailError: '' }]) })
	}
	
	// to remove new vendor
	handleRemoveNewVendor (e, idx) {
		this.setState({ newVendorsList: this.state.newVendorsList.filter((s, sidx) => idx !== sidx) })
  }

	// Date onChange event
	handleDateChange(objDate) {
		const formFieldsState = this.state.formFields
		const formErrorsState = this.state.formErrors

		if (this.state.errMsg.length > 0) this.setState({ errMsg: '' })

		formFieldsState['bidEndDate'] = moment(objDate).format('DD/MM/YYYY')
		formErrorsState['bidEndDate'] = ''

		this.setState({ formFieldsState, formErrorsState, tempBidEndDate: objDate._d })
	}

	// to form selected vendors from dropdown list
	onSelectVendorsFromList (selectedList, selectedItem) {
		let formErrors = this.state.formErrors
		formErrors['oldVendorsList'] = ''
		this.setState({ formErrors })
		this.state.formFields.oldVendorsList.push(selectedItem)
	}

	// to remove and form selected vendors from dropdown list
	onRemoveVendorsFromList (selectedList, removedItem) {
		this.state.formFields.oldVendorsList.splice( this.state.formFields.oldVendorsList.indexOf(removedItem), 1 )
	}

	componentDidUpdate () {
		document.getElementsByClassName('chip').value = 'RK'
	}

	filterByValue(array, string) {
    return array.filter(o => Object.keys(o).some(k => o[k].toLowerCase().includes(string.toLowerCase())));
	}

	onKeyDownListner (e) {
		if (e.target.id === 'hsnCode' || e.target.id === 'unspscCode') {
			let searchKey = ''
			// to allow only numbers, characters and space.
			if ((e.keyCode >=65 && e.keyCode <= 90) || (e.keyCode >=48 && e.keyCode <= 57) || (e.keyCode >=96 && e.keyCode <= 105) || e.keyCode === 32) {
				searchKey = e.target.value + e.key
			} else searchKey = e.target.value.substring(0, e.target.value.length - 1)

			if (e.target.id === 'hsnCode') {
				this.setState({ hsnList: [] })
				if (searchKey.length > 0) {
					const hsnList = this.filterByValue(hsnDataList, searchKey).slice(0, 100).map((value, idx) => {
						return { ...value, 'hsnCode': value.hsn_code, 'description': value.description, 'hsnCodeDesc': value.hsn_code + ' - '  +  value.description }
					})
					this.setState({ hsnList: hsnList })
				}
			} else if (e.target.id === 'unspscCode') {
				this.setState({ unspscList: [] })
				if (searchKey.length > 0) {
					const unspscList = this.filterByValue(unspscDataList, searchKey).slice(0, 100).map((value, idx) => {
						return { ...value, 'unspscCode': value.commodity, 'description': value.commodity_name, 'unspscCodeDesc': value.commodity + ' - '  +  value.commodity_name }					
					})
					this.setState({ unspscList: unspscList })
				}
			}
		}
	}

	onSelectHSNCodeFromList (selectedList, selectedItem) {
		let formFields = this.state.formFields
		let formErrors = this.state.formErrors
		formErrors['HSNCode'] = ''
		formFields['HSNCode'] = selectedItem.hsnCodeDesc
		this.setState({ formFields, formErrors })
	}

	onRemoveHSNCodeFromList (selectedList, removedItem) {
		let formFields = this.state.formFields
		let formErrors = this.state.formErrors
		formErrors['HSNCode'] = 'error'
		formFields['HSNCode'] = ''
		this.setState({ formFields, formErrors })
	}

	onSelectUNSPSCCodeFromList (selectedList, selectedItem) {
		let formFields = this.state.formFields
		let formErrors = this.state.formErrors
		formErrors['UNSPSCCode'] = ''
		formFields['UNSPSCCode'] = selectedItem.unspscCodeDesc
		this.setState({ formFields, formErrors })
	}

	onRemoveUNSPSCCodeFromList (selectedList, removedItem) {
		let formFields = this.state.formFields
		let formErrors = this.state.formErrors
		formErrors['UNSPSCCode'] = 'error'
		formFields['UNSPSCCode'] = ''
		this.setState({ formFields, formErrors })
	}

	// Radio button onchange event
	handleBidTypeOnChange(e, objName) {
		const { name, value } = e.target
		const formFieldsState = this.state.formFields
		const formErrorsState = this.state.formErrors

		if (this.state.errMsg.length > 0) this.setState({ errMsg: '' })

		if (typeof(name) !== 'undefined') {
			formFieldsState[name] = objName
			formErrorsState[name] = ''
			this.setState({ formFieldsState, formErrorsState })
		}
	}

	// Input focus exit
	handleInputFocusExit(e) {
		const { name, value } = e.target
		const dataType = e.target.getAttribute('data-type')

		let formErrorState = this.state.formErrors

		this.setState({ disablebtn: 'outer-btn' })
		this.setState({ isFormValidationSuccess: true })

		if (dataType === 'mobile') {
			// mobile/phone validation
			if (!(/^(\+(([0-9]){1,2})[-.])?((((([0-9]){2,3})[-.]){1,2}([0-9]{4,10}))|([0-9]{10}))$/.test(value))) {
				formErrorState[name] = 'Invalid mobile number! Please enter 10 digit mobile number'
				this.setState({ isFormValidationSuccess: false })
			}
		}
	}

	// Input change event
	handleInputChange(e) {
		const { name, value } = e.target
		const dataType = e.target.getAttribute('data-type')
		let isvalidValue = false

		let formFieldsState = this.state.formFields
		let formErrorState = this.state.formErrors

		if (this.state.errMsg.length > 0) this.setState({ errMsg: '' })

		// Form Handling
		if (dataType === 'number' || dataType === 'pincode' || dataType === 'mobile') {
			// to allow only numbers
			if (!isNaN(value)) isvalidValue = true
		} else if (dataType === 'city') {
			// to allow only alphabets and space.
			if (/^[a-zA-Z ]*$/.test(value)) isvalidValue = true
		} else isvalidValue = true

		if (isvalidValue) {
			formErrorState[name] = '' // Error handling
			formFieldsState[name] = value // Form field 
			this.setState({ formFieldsState, formErrorState })
		}
	}

	// to check all fields are entered or not.
	handleEmptyFormCheck() {
		let valid = true
		const formFields = this.state.formFields
		const errorFields = this.state.formErrors
		Object.keys(formFields).forEach(key => {
			let value = formFields[key]
			if (value === null || value.length <= 0) {
				if (Object.keys(errorFields).indexOf(key) >= 0) {
					if (key !== 'oldVendorsList' && key !== 'shipAltCont') {
						if (formFields.docType === 'no') {
							errorFields[key] = 'error'
							valid = false
						} else if (formFields.docType === 'yes') {
							if (key !== 'itemDesc' && key !== 'quantity')  {
								errorFields[key] = 'error'
								valid = false									
							}
						}
					}
					// this.setState({ errMsg: '*Please enter the mandatory fields.' })
				}
			}
		})
		this.setState({ errorFields })

		// to display error message when vendors not selected.
		if (this.state.newVendorsList.length === 0) {
			if (formFields.oldVendorsList.length === 0 ) {
				errorFields['oldVendorsList'] = 'Please select vendors from list'
				this.setState({ errorFields })
				valid = false
			}
		}

		// validation for newly added vendors via input fields.
		if (this.state.newVendorsList.length > 0) {
			const newVendors = this.state.newVendorsList.map((value, sidx) => {
				let errorMessage = ''
				// empty validation
				if (value.newVendorEmail.length === 0) {
					errorMessage = 'Please enter vendor email address'
					valid = false
				}
				else errorMessage = ''
				return { ...value, newVendorEmail: value.newVendorEmail, newVendorEmailError: errorMessage }
			})
			this.setState({ newVendorsList: newVendors })
		}

		return valid
	}

	// to handle submit
	handleSubmit() {
		let formFields = this.state.formFields // form fields state

		let isEmptyFormFields = true

		// this.setState({ disablebtn: 'outer-btn disablebtn' })

		if (!this.handleEmptyFormCheck()) isEmptyFormFields = false

		if (formFields.docType.toLowerCase() === 'yes' && this.state.proposalDocument === null) {
			this.setState({ errMsg: '*Please upload proposal document' })
			isEmptyFormFields = false
		}

		if (isEmptyFormFields && this.state.isFormValidationSuccess) {

			// to assign bid close date to form fields from temp state.
			formFields.bidStartDate = ((new Date().getTime()) / 1000).toString()
			formFields.bidEndDate = ((new Date(this.state.tempBidEndDate).getTime()) / 1000).toString()
			this.setState({ formFields })
			
			// reassign the new vendor email address.
			if (this.state.newVendorsList.length > 0) {
				const newVendors = this.state.newVendorsList.map((value, sidx) => {
					this.state.formFields.newVendorsList.push(value.newVendorEmail)
					return { ...value, value }
				})
				this.setState({ newVendorsList: newVendors })
			}
			
			// console.log('success data', this.state.formFields)
			
			const reqBody = JSON.stringify(formFields)
			const formData = new FormData()
			formData.append('data', reqBody)

			if (this.state.formFields.docType.toLowerCase() === 'yes') {
				formData.append('rfpFile', this.state.proposalDocument)
			}

			const reqURL = serverURL + '/rfp/create'
			let userInfo = JSON.parse(localStorage.getItem('userInfo'))
			const options = {
				headers: { 
					'Access-Control-Allow-Origin': "*",
					'access_key': userInfo.access_key,
				}
			}
			axios.post(reqURL, formData, options).then(res => {
				// console.log('data ', res.data)
				if (res.data.status.toLowerCase() === 'success') {
					localStorage.setItem('isSuccess', true)
					this.props.history.push('/buyer/dashboard')
				} else if (res.data.status.toLowerCase() === 'error') {
					this.setState({ errMsg: res.data.message })
					this.setState({ disablebtn: 'outer-btn' })
				}
			})
		} else this.setState({ disablebtn: 'outer-btn' })
	}

	// to get vendorslist
	getVendorList (objUserInfo) {
		const reqURL = serverURL + '/vendor/list'
    const options = {
      headers: { 
        'Access-Control-Allow-Origin': "*",
        'access_key': objUserInfo.access_key,
      }
    }		
		axios.get(reqURL, options).then(res => {
			// console.log('data ', res.data.data)
			let data = res.data.data
			if (data.length > 0) {
				const newVendors = data.map((value, sidx) => {
					this.state.vendorsList.push({ 'id': value.id, 'name': value.name, email: value.email })
				})
			}
		})
	}

	componentDidMount () {
		let userInfo = JSON.parse(localStorage.getItem('userInfo'))
    if (userInfo) {
      if (this.state.accountType !== 'buyer') this.props.history.push('/')
      else if (this.state.accountType === 'buyer') {
        this.setState({ profileName: userInfo.firstName + ' ' + userInfo.lastName })
        this.setState({ profileIcon: userInfo.firstName.charAt(0) + userInfo.lastName.charAt(0) })
      }
		} else this.props.history.push('/')
		
		this.getVendorList(userInfo)

		// to find the hsncode or unspsc code based on the user input.
		document.addEventListener('keydown', this.onKeyDownListner)
	}

	render() {	
		// form fields
		let { itemCode, itemName, HSNCode, UNSPSCCode, docType, itemDesc, quantity, shipAddress, shipCity, shipPincode, shipMobile, shipAltCont } = this.state.formFields
		// form errors
		let formErrors = this.state.formErrors

		// disable past dates for bid close date
		var yesterday = Datetime.moment().subtract( 1, 'day' )
		var disablePastDt = function( current ){
			return current.isAfter( yesterday )
		}

		return (
			<div id='rfp-form'>
        {/* Header */}
				<Header profileName={this.state.profileName} profileIcon={this.state.profileIcon} />

				{/* Main content */}
				<div className='rfp-container'>
					<div className='rfp-block'>
						<Link to='/buyer/dashboard'><img src='/assets/images/ic-back.png' /></Link>
						<h1 className='rfp-title'>Request for Proposal</h1>

						{/* RFP details block */}
						<div className='comp-det-block'>
							<div className='flex-box'>
								{/* Item Code */}
								<div className='input-container'>
									<div className='input-groups'>
										<label className='field-caption'>Item Code*</label>
										<div className='out-ex-size'>
											<input type='text' className={formErrors.itemCode.length > 0 ? 'form-field error' : 'form-field'} autoComplete='off' data-type='string' name='itemCode' value={itemCode} placeholder='Enter Item Code' onChange={this.handleInputChange} maxLength='20' />
											{formErrors.itemCode.length > 0 && <div className='error-title'>Please enter item code</div>}
										</div>
									</div>
								</div>
								{/* Item Name */}
								<div className='input-container'>
									<div className='input-groups'>
										<label className='field-caption'>Item Name*</label>
										<div className='out-ex-size'>
											<input type='text' className={formErrors.itemName.length > 0 ? 'form-field error' : 'form-field'} autoComplete='off' data-type='string' name='itemName' value={itemName} placeholder='Enter Item Name' onChange={this.handleInputChange} maxLength='30' />
											{formErrors.itemName.length > 0 && <div className='error-title'>Please enter item name</div>}
										</div>
									</div>
								</div>
							</div>
							{/* HSN Code */}
							<div className='input-container'>
								<div className='input-groups'>
									<label className='field-caption'>HSN Code*</label>
									<div className='out-ex-size'>
										{/* <input type='text' className={formErrors.HSNCode.length > 0 ? 'form-field error' : 'form-field'} autoComplete='off' data-type='string' name='HSNCode' value={HSNCode} placeholder='Enter HSN code' onChange={this.handleInputChange} maxLength='20' /> */}
										<Multiselect
										id='hsnCode'
										selectionLimit = '1'
										options={this.state.hsnList} // Options to display in the dropdown
										onSelect={this.onSelectHSNCodeFromList} // Function will trigger on select event
										onRemove={this.onRemoveHSNCodeFromList} // Function will trigger on remove event
										displayValue='hsnCodeDesc' // Property name to display in the dropdown options
										avoidHighlightFirstOption={true}
										placeholder= 'Enter to find HSN code'
										style={
											{ multiselectContainer: { width: '102%', fontFamily: 'OpenSans-Regular', fontSize: '14px', lineHeight: '19px', letterSpacing: '.2px' },
												searchBox: { border: formErrors.oldVendorsList.length > 0 ? '1px solid rgba(255,0,0,0.35)' : '1px solid #e3e3e3', borderRadius: '2px', padding: '10px' },
												chips: { margin: '0px 5px 0px 0px', background: '#cac6c6', whiteSpace: 'normal', maxWidth: '70%' },
												inputField: { fontFamily: 'OpenSans-Regular', fontSize: '14px', lineHeight: '19px', letterSpacing: '.2px', margin: '0px' },
												option: { fontFamily: 'OpenSans-Regular', fontSize: '14px', lineHeight: '19px', letterSpacing: '.2px', color: 'rgba(0,0,0,0.8)', display: 'flex', alignItems: 'center' }
											}
										}
									/>
									{formErrors.HSNCode.length > 0 && <div className='error-title'>Please enter HSNCode code</div>}
									</div>
								</div>
							</div>
							{/* UNBPSC Code */}
							<div className='input-container'>
								<div className='input-groups'>
									<label className='field-caption'>UNSPSC Code*</label>
									<div className='out-ex-size'>
										{/* <input type='text' className={formErrors.UNSPSCCode.length > 0 ? 'form-field error' : 'form-field'} autoComplete='off' data-type='string' name='UNSPSCCode' value={UNSPSCCode} placeholder='Enter UNSPSC code' onChange={this.handleInputChange} maxLength='20' /> */}
										<Multiselect
											id='unspscCode'
											selectionLimit = '1'
											options={this.state.unspscList} // Options to display in the dropdown
											onSelect={this.onSelectUNSPSCCodeFromList} // Function will trigger on select event
											onRemove={this.onRemoveUNSPSCCodeFromList} // Function will trigger on remove event
											displayValue='unspscCodeDesc' // Property name to display in the dropdown options
											avoidHighlightFirstOption={true}
											placeholder = 'Enter to find UNSPSC code'
											style={
												{ multiselectContainer: { width: '102%', fontFamily: 'OpenSans-Regular', fontSize: '14px', lineHeight: '19px', letterSpacing: '.2px' },
													searchBox: { border: formErrors.oldVendorsList.length > 0 ? '1px solid rgba(255,0,0,0.35)' : '1px solid #e3e3e3', borderRadius: '2px', padding: '10px' },
													chips: { margin: '0px 5px 0px 0px', background: '#cac6c6', whiteSpace: 'normal' },
													inputField: { fontFamily: 'OpenSans-Regular', fontSize: '14px', lineHeight: '19px', letterSpacing: '.2px', margin: '0px' },
													option: { fontFamily: 'OpenSans-Regular', fontSize: '14px', lineHeight: '19px', letterSpacing: '.2px', color: 'rgba(0,0,0,0.8)', display: 'flex', alignItems: 'center' }
												}
											}
										/>										
										{formErrors.UNSPSCCode.length > 0 && <div className='error-title'>Please enter UNSPSC code</div>}
									</div>
								</div>
							</div>
							{/* Document Type */}
							<div className='input-container radio-sec mar-bot'>
								<div className='input-groups radio-field'>
									<label className='field-caption no-pad'>Do you want to upload<br />proposal document?*</label>
									<div className='radio-input-section'>
										<label className='radio-container' onClick={(e) => this.handleBidTypeOnChange(e, 'yes')}><input type='radio' className='form-field-radio' value='yes' name='docType' checked={docType === 'yes' ? true : false } />Yes<span className={'checkmark ' + formErrors.docType} /></label>
										<label className='radio-container' onClick={(e) => this.handleBidTypeOnChange(e, 'no')}><input type='radio' className='form-field-radio' value='no' name='docType' checked={docType === 'no' ? true : false } />No<span className={'checkmark ' + formErrors.docType} /></label>
									</div>
								</div>
							</div>
							<div className='out-radio-ex-size'>
								{formErrors.docType.length > 0 && <div className='error-title'>Please choose document type</div>}
							</div>

							{docType === 'yes' 
								? <div className='self-attest-block'>
										{/* Proposal Document */}
										<div className='input-container no-mar'>
											<div className='input-groups'>
												<label className='field-caption no-pad'>Proposal Document*</label>
												<input type='file' className='cls-file-upload' name='proposalDocument' onChange={this.handleFileUpload} />
											</div>
										</div>
									</div>
								: <>
									{/* Description */}
									<div className='input-container textarea-sec'>
										<div className='input-groups input-textarea'>
											<label className='field-caption note-filed'>Description*</label>
											<div className='out-ex-size'>
												<textarea className={formErrors.itemDesc.length > 0 ? 'cls-notes error' : 'cls-notes'} autoComplete='false' data-type='string' name='itemDesc' value={itemDesc} placeholder='Enter Description' onChange={this.handleInputChange} maxLength='250' />
												{formErrors.itemDesc.length > 0 && <div className='error-title'>Please enter description</div>}
											</div>
										</div>
									</div>
									<div className='flex-box'>
										{/* Quantity  */}
										<div className='input-container'>
											<div className='input-groups'>
												<label className='field-caption'>Quantity*</label>
												<div className='out-ex-size'>
													<input type='text' className={formErrors.quantity.length > 0 ? 'form-field error' : 'form-field'} autoComplete='off' data-type='number' name='quantity' value={quantity} placeholder='Enter Quantity' onChange={this.handleInputChange} maxLength='10' />
													{formErrors.quantity.length > 0 && <div className='error-title'>{formErrors.quantity.length > 5 ? formErrors.quantity : 'Please enter quantity'}</div>}
												</div>
											</div>
										</div>
									</div>
								</>
							}
						</div>

						{/* Bidding details */}
						<h2 className='rfp-sub-title'>Bidding Details</h2>
						<div className='comp-det-block'>
							{/* Bid Type */}
							<div className='input-container radio-sec mar-bot'>
								<div className='input-groups radio-field'>
									<label className='field-caption no-pad'>Bid Type*</label>
									<div className='radio-input-section'>
										<label className='radio-container' onClick={(e) => this.handleBidTypeOnChange(e, 'open')}><input type='radio' className='form-field-radio' value='open' name='bidType' />Open<span className={'checkmark ' + formErrors.bidType} /></label>
										<label className='radio-container' onClick={(e) => this.handleBidTypeOnChange(e, 'close')}><input type='radio' className='form-field-radio' value='close' name='bidType' />Close<span className={'checkmark ' + formErrors.bidType} /></label>
									</div>
								</div>
							</div>
							<div className='out-radio-ex-size'>
								{formErrors.bidType.length > 0 && <div className='error-title'>Please choose bit type</div>}
							</div>
							<div className='flex-box'>
								{/* Bid start date */}
								<div className='input-container'>
									<div className='input-groups'>
										<label className='field-caption'>Bid Start Date*</label>
										<div className='out-ex-size'>
											<Datetime
												dateFormat={'DD/MM/YYYY'}
												timeFormat={false}
												className='date form-field'
												name='bidStartDate'
												defaultValue={new Date()}
												closeOnSelect
												closeOnTab
												inputProps={{ placeholder: 'DD/MM/YYYY', disabled: true }}
												onChange={this.handleDateChange} />
										</div>
									</div>
								</div>
								{/* Bid Close date */}
								<div className='input-container'>
									<div className='input-groups'>
										<label className='field-caption'>Bid Close Date &amp; Time*</label>
										<div className='out-ex-size'>
											<Datetime
												isValidDate={disablePastDt}
												dateFormat={'DD/MM/YYYY'}
												className={formErrors.bidEndDate.length > 0 ? 'date form-field error' : 'date form-field'}
												name='bidEndDate'
												closeOnSelect
												closeOnTab
												inputProps={{ placeholder: 'DD/MM/YYYY', readOnly: true }}
												onChange={this.handleDateChange} />
											{formErrors.bidEndDate.length > 0 && <div className='error-title'>Please select bid close date &amp; time</div>}
										</div>
									</div>
								</div>
							</div>
							{/* Bid to be send */}
							<div className='input-container'>
								<div className='input-groups'>
									<label className='field-caption'>Select Vendor's*</label>
									<div className='out-ex-size'>
										<Multiselect
											options={this.state.vendorsList} // Options to display in the dropdown
											selectedValues={this.state.selectedValue} // Preselected value to persist in dropdown
											onSelect={this.onSelectVendorsFromList} // Function will trigger on select event
											onRemove={this.onRemoveVendorsFromList} // Function will trigger on remove event
											displayValue='email' // Property name to display in the dropdown options
											showCheckbox={true}
											avoidHighlightFirstOption={true}
											style={
												{ multiselectContainer: { width: '102%', fontFamily: 'OpenSans-Regular', fontSize: '14px', lineHeight: '19px', letterSpacing: '.2px' },
													searchBox: { border: formErrors.oldVendorsList.length > 0 ? '1px solid rgba(255,0,0,0.35)' : '1px solid #e3e3e3', borderRadius: '2px', padding: '10px' },
													chips: { margin: '0px 5px 0px 0px', background: '#cac6c6' },
													inputField: { fontFamily: 'OpenSans-Regular', fontSize: '14px', lineHeight: '19px', letterSpacing: '.2px', margin: '0px' },
													option: { fontFamily: 'OpenSans-Regular', fontSize: '14px', lineHeight: '19px', letterSpacing: '.2px', color: 'rgba(0,0,0,0.8)', display: 'flex', alignItems: 'center' }
												}
											}
										/>
										{formErrors.oldVendorsList.length > 0 && <div className='error-title'>{formErrors.oldVendorsList.length > 5 ? formErrors.oldVendorsList : 'Please select vendors from list'}</div>}
									</div>
								</div>
							</div>
							{/* New Vendor Email */}
							{this.state.newVendorsList.map((value, idx) => (
								<div className='input-container'>
									<div className='input-groups'>
										<label className='field-caption'>New Vendor Email*</label>
										<div className='out-ex-size no-wrap'>
											<input type='text' className={value.newVendorEmailError.length > 0 ? 'form-field new-ven-ex-size error' : 'form-field new-ven-ex-size'} autoComplete='off' data-type='email' name='newVendorEmail' value={value.newVendorEmail} placeholder='Enter New Vendor Email Address' onChange={(e) => this.handleNewVendorInputChange(e, idx)} onBlur={(e) => this.handleNewVendorOnBlurChange(e, idx)} maxLength='50' /><span className='close-icon' onClick={(e) => this.handleRemoveNewVendor(e, idx)}>X</span>
											{value.newVendorEmailError.length > 0 && <div className='error-title'>{value.newVendorEmailError.length > 5 ? value.newVendorEmailError : 'Please enter vendor address'}</div>}
										</div>
									</div>
								</div>
							))}
							{/* Add Button */}
							<div className='form-submit-section btn-right'>
								<div className='outer-btn btn-add'>
									<div className='btn register-booking' onClick={this.handleAddNewVendor}>ADD NEW VENDOR</div>
								</div>
							</div>
						</div>

						{/* Shipping details */}
						<h2 className='rfp-sub-title'>Shipping Details</h2>
						<div className='comp-det-block'>
							{/* Address */}
							<div className='input-container textarea-sec'>
								<div className='input-groups input-textarea'>
									<label className='field-caption note-filed'>Shipping Address*</label>
									<div className='out-ex-size'>
										<textarea className={formErrors.shipAddress.length > 0 ? 'cls-notes error' : 'cls-notes'} autoComplete='false' data-type='string' name='shipAddress' value={shipAddress} placeholder='Enter Shipping Address' onChange={this.handleInputChange} maxLength='250' />
										{formErrors.shipAddress.length > 0 && <div className='error-title'>Please enter shipping address</div>}
									</div>
								</div>
							</div>
							<div className='flex-box'>
								{/* City */}
								<div className='input-container'>
									<div className='input-groups'>
										<label className='field-caption'>City*</label>
										<div className='out-ex-size'>
											<input type='text' className={formErrors.shipCity.length > 0 ? 'form-field error' : 'form-field'} autoComplete='off' data-type='city' name='shipCity' value={shipCity} placeholder='Enter City' onChange={this.handleInputChange} maxLength='30' />
											{formErrors.shipCity.length > 0 && <div className='error-title err-width'>Please enter city</div>}
										</div>
									</div>
								</div>
								{/* Pincode */}
								<div className='input-container'>
									<div className='input-groups'>
										<label className='field-caption'>Pincode*</label>
										<div className='out-ex-size'>
											<input type='text' className={formErrors.shipPincode.length > 0 ? 'form-field error' : 'form-field'} autoComplete='off' data-type='pincode' name='shipPincode' value={shipPincode} placeholder='Enter Pincode' onChange={this.handleInputChange} maxLength='10' />
											{formErrors.shipPincode.length > 0 && <div className='error-title err-width'>Please enter pincode</div>}
										</div>
									</div>
								</div>
							</div>
							<div className='flex-box'>
								{/* Mobile */}
								<div className='input-container'>
									<div className='input-groups'>
										<label className='field-caption'>Mobile*</label>
										<div className='out-ex-size'>
											<input type='text' className={formErrors.shipMobile.length > 0 ? 'form-field error' : 'form-field'} autoComplete='off' data-type='mobile' name='shipMobile' value={shipMobile} placeholder='Enter Mobile Number' onChange={this.handleInputChange} onBlur={this.handleInputFocusExit} maxLength='15' />
											{formErrors.shipMobile.length > 0 && <div className='error-title err-width'>{formErrors.shipMobile.length > 5 ? formErrors.shipMobile : 'Please enter mobile number'}</div>}
										</div>
									</div>
								</div>
								{/* Alternate Contact Number */}
								<div className='input-container'>
									<div className='input-groups'>
										<label className='field-caption'>Alternate Contact No.</label>
										<div className='out-ex-size'>
											<input type='text' className={formErrors.shipAltCont.length > 5 ? 'form-field error' : 'form-field'} autoComplete='off' data-type='mobile' name='shipAltCont' value={shipAltCont} placeholder='Enter Alternate Contact Number' onChange={this.handleInputChange} onBlur={this.handleInputFocusExit} maxLength='15' />
											{formErrors.shipAltCont.length > 5 && <div className='error-title err-width'>{formErrors.shipAltCont}</div>}
										</div>
									</div>
								</div>
							</div>
						</div>

						{/* Error message */}
						<div className='cls-err-msg'>{this.state.errMsg}&nbsp;</div>
						{/* Submit button */}
						<div className='form-submit-section'>
							<div className={this.state.disablebtn}>
								<div className='btn register-booking' onClick={this.handleSubmit}>CREATE</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		)
	}
}
export default withRouter(RFPForm)
