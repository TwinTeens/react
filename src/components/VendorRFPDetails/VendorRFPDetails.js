import React, { Component, Fragment } from 'react'
import { withRouter, Link } from 'react-router-dom'
import axios from 'axios'
import Datetime from 'react-datetime'
import 'react-datetime/css/react-datetime.css'
import moment from 'moment'
import { Multiselect } from 'multiselect-react-dropdown'
import { validateEmail, serverURL } from '../../global'
import FileSaver from 'file-saver'
import Header from '../Header/Header'

import './VendorRFPDetails.css'

class VendorRFPDetails extends Component {
	constructor(props) {
		super(props)
		this.state = {
			accountType: localStorage.getItem('avatarType'),
			isHide: true,
			isDisabled: true,
			errMsg: '',
			pleaseWait: false,
			disablebtn: 'outer-btn',
			vendorsList: [],
			isFormValidationSuccess: true,
			tempBidEndDate: '', // to get selected bid close date and time in IST format 'Tue Apr 14 2020 20:55:01 GMT+0530 (IST)'
			// to handle Form Fields
			formFields: {
				itemCode: '',
				itemName: '',
				HSNCode: '',
				UNSPSCCode: '',
				itemDesc: '',
				quantity: '',
				bidType: '',
				bidStartDate: '',
				bidEndDate: '',
				oldVendorsList: [],
				newVendorsList: [],
				shipAddress: '',
				shipCity: '',
				shipPincode: '',
				shipMobile: '',
				shipAltCont: ''
			},
			// to handle Form Errors
			formErrors: {
				itemCode: '',
				itemName: '',
				HSNCode: '',
				UNSPSCCode: '',
				itemDesc: '',
				quantity: '',
				bidType: '',
				bidEndDate: '',
				oldVendorsList: '',
				shipAddress: '',
				shipCity: '',
				shipPincode: '',
				shipMobile: '',
				shipAltCont: ''
			},
			bidDocument: null,
			bidDesc: '',
			bidQty: '',
			bidUnitPrice: '',
			bidTotal: '',
			bidDescErr: '',
			bidQtyErr: '',
			bidUnitPriceErr: '',
			uploadFilename: '',
			rfpFile: ''
		}
		this.handleInputChange = this.handleInputChange.bind(this)
		this.handleInputFocusExit = this.handleInputFocusExit.bind(this)
		this.handleBidTypeOnChange = this.handleBidTypeOnChange.bind(this)
		this.handleDateChange = this.handleDateChange.bind(this)
		this.handleEmptyFormCheck = this.handleEmptyFormCheck.bind(this)
		this.handleNewVendorInputChange = this.handleNewVendorInputChange.bind(this)
		this.handleNewVendorOnBlurChange = this.handleNewVendorOnBlurChange.bind(this)
		this.handleAddNewVendor = this.handleAddNewVendor.bind(this)
		this.handleRemoveNewVendor = this.handleRemoveNewVendor.bind(this)
		this.onSelectVendorsFromList = this.onSelectVendorsFromList.bind(this)
		this.onRemoveVendorsFromList = this.onRemoveVendorsFromList.bind(this)
		this.handleSubmit = this.handleSubmit.bind(this)
		this.handleCheckBox = this.handleCheckBox.bind(this)
		this.handleFileUpload = this.handleFileUpload.bind(this)
		this.handleDownload = this.handleDownload.bind(this)
	}

	// to download already uploaded document.
	handleDownload (obj) {
		try {
			let userInfo = JSON.parse(localStorage.getItem('userInfo'))
			const options = {
				headers: { 
					'Access-Control-Allow-Origin': "*",
					'Content-Type': 'application/json',
					'access_key': userInfo.access_key
				},
				responseType: 'arraybuffer'
			}
			let uploadedFileName = ''
			let reqURL = ''

			if(obj === 'vendor') {
				reqURL = serverURL + '/bid/file/download'
				uploadedFileName = this.state.uploadFilename
			} else if(obj === 'buyer') {
				reqURL = serverURL + '/rfp/file/download'
				uploadedFileName = this.state.rfpFile
			}

			const reqBody = { 'rfp_id': this.props.match.params.id }
			axios.post(reqURL, JSON.stringify(reqBody), options).then(res => {
				// console.log('response', res.data)
				let fileName = uploadedFileName.split('.')
				if (fileName[1].toLowerCase() === 'pdf') { // PDF
					var blob = new Blob([res.data], { type: 'application/pdf' })
				} else if (fileName[1].toLowerCase() === 'xlsx') { // XLSX
					var blob = new Blob([res.data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' })
				}
				FileSaver.saveAs(blob, uploadedFileName)
			})
		} catch (error) {
			console.log('try catch failure')
		}
	}

	// Upload file event
	handleFileUpload (e) {
		let fileExt = e.target.files[0].name.split('.')
		let fileType = fileExt[1]
		// console.log('file type', fileType)
		if (fileType === 'pdf' || fileType === 'xlsx' || fileType === 'csv') {
			if(this.state.errMsg.length > 0) this.setState({ errMsg: '' })
			this.setState({ bidDocument: e.target.files[0] })
		} else {
			this.setState({ bidDocument: null })
			this.setState({ errMsg: '*Please upload a file in pdf or xlsx or csv format.' })
		}
	}

	handleCheckBox (e, idx) {
		// console.log('inside', idx)
		const newVendorsList = this.state.vendorsList.map((value, sidx) => {
			if (idx !== sidx) return value
			if(value.isChecked === '') return { ...value, isChecked: 'checked' }
			else return { ...value, isChecked: '' }
		})
		this.setState({ vendorsList: newVendorsList })
	}

	// to handle onblur event for new vendors email address fields.
	handleNewVendorOnBlurChange (e, idx) {
		const newVendors = this.state.newVendorsList.map((value, sidx) => {
			if (idx !== sidx) return value

			let errorMessage = ''

			// email validation
			if (!validateEmail(e.target.value)) errorMessage = 'Invalid email! Please enter valid vendor email address'
			else errorMessage = ''

			this.setState({ isFormValidationSuccess: errorMessage.length > 0 ? false : true })
			return { ...value, newVendorEmail: e.target.value, newVendorEmailError: errorMessage }
		})
		this.setState({ newVendorsList: newVendors })
	}

	// to handle new vendor input field 
	handleNewVendorInputChange (e, idx) {
		const newVendors = this.state.newVendorsList.map((value, sidx) => {
			if (idx !== sidx) return value			
			return { ...value, newVendorEmail: e.target.value, newVendorEmailError: '' }
		})
		this.setState({ newVendorsList: newVendors })
	}

	// to add new vendor
	handleAddNewVendor () {
		this.setState({ newVendorsList: this.state.newVendorsList.concat([{ newVendorEmail: '', newVendorEmailError: '' }]) })
	}
	
	// to remove new vendor
	handleRemoveNewVendor (e, idx) {
		this.setState({ newVendorsList: this.state.newVendorsList.filter((s, sidx) => idx !== sidx) })
  }

	// Date onChange event
	handleDateChange(objDate) {
		const formFieldsState = this.state.formFields
		const formErrorsState = this.state.formErrors

		if (this.state.errMsg.length > 0) this.setState({ errMsg: '' })

		formFieldsState['bidEndDate'] = moment(objDate).format('DD/MM/YYYY')
		formErrorsState['bidEndDate'] = ''

		this.setState({ formFieldsState, formErrorsState, tempBidEndDate: objDate._d })
	}

	// to form selected vendors from dropdown list
	onSelectVendorsFromList (selectedList, selectedItem) {
		let formErrors = this.state.formErrors
		formErrors['oldVendorsList'] = ''
		this.setState({ formErrors })
		this.state.formFields.oldVendorsList.push(selectedItem)
	}

	// to remove and form selected vendors from dropdown list
	onRemoveVendorsFromList (selectedList, removedItem) {
		this.state.formFields.oldVendorsList.splice( this.state.formFields.oldVendorsList.indexOf(removedItem), 1 )
	}

	// Radio button onchange event
	handleBidTypeOnChange(objName) {
		const formFieldsState = this.state.formFields
		const formErrorsState = this.state.formErrors

		if (this.state.errMsg.length > 0) this.setState({ errMsg: '' })

		formFieldsState['bidType'] = objName
		formErrorsState['bidType'] = ''
		this.setState({ formFieldsState, formErrorsState })
	}

	// Input focus exit
	handleInputFocusExit(e) {
		const { name, value } = e.target
		const dataType = e.target.getAttribute('data-type')

		let formErrorState = this.state.formErrors

		this.setState({ disablebtn: 'outer-btn' })
		this.setState({ isFormValidationSuccess: true })

		if (dataType === 'mobile') {
			// mobile/phone validation
			if (!(/^(\+(([0-9]){1,2})[-.])?((((([0-9]){2,3})[-.]){1,2}([0-9]{4,10}))|([0-9]{10}))$/.test(value))) {
				formErrorState[name] = 'Invalid mobile number! Please enter 10 digit mobile number'
				this.setState({ isFormValidationSuccess: false })
			}
		}
	}

	// Input change event
	handleInputChange(e) {
		const { name, value } = e.target
		const dataType = e.target.getAttribute('data-type')
		let isvalidValue = false

		let formFieldsState = this.state
		// let formErrorState = this.state

		if (this.state.errMsg.length > 0) this.setState({ errMsg: '' })

		// Form Handling
		if (dataType === 'number') {
			// to allow only numbers
			if (!isNaN(value)) isvalidValue = true
		} else isvalidValue = true

		if (isvalidValue) {
			formFieldsState[name+'Err'] = '' // Error handling
			formFieldsState[name] = value // Form field 
			this.setState({ formFieldsState })
		}
	}

	// to check all fields are entered or not.
	handleEmptyFormCheck() {
		let valid = true
		const formFields = this.state.formFields
		const errorFields = this.state.formErrors
		Object.keys(formFields).forEach(key => {
			let value = formFields[key]
			if (value === null || value.length <= 0) {
				if (Object.keys(errorFields).indexOf(key) >= 0) {
					if (key !== 'oldVendorsList' && key !== 'shipAltCont') {
						errorFields[key] = 'error'
						valid = false
					}
					// this.setState({ errMsg: '*Please enter the mandatory fields.' })
				}
			}
		})
		this.setState({ errorFields })

		// to display error message when vendors not selected.
		if (this.state.newVendorsList.length === 0) {
			if (formFields.oldVendorsList.length === 0 ) {
				errorFields['oldVendorsList'] = 'Please select vendors from list'
				this.setState({ errorFields })
				valid = false
			}
		}

		// validation for newly added vendors via input fields.
		if (this.state.newVendorsList.length > 0) {
			const newVendors = this.state.newVendorsList.map((value, sidx) => {
				let errorMessage = ''
				// empty validation
				if (value.newVendorEmail.length === 0) {
					errorMessage = 'Please enter vendor email address'
					valid = false
				}
				else errorMessage = ''
				return { ...value, newVendorEmail: value.newVendorEmail, newVendorEmailError: errorMessage }
			})
			this.setState({ newVendorsList: newVendors })
		}

		return valid
	}

	// to handle submit
	handleSubmit() {
		let isFormEmpty = true
		this.setState({ disablebtn: 'outer-btn disablebtn' })

		if (this.state.formFields.bidType.toLowerCase() === 'close' && this.state.bidDocument === null) {
			this.setState({ errMsg: '*Please upload bid document' })
			isFormEmpty = false
		} else if(this.state.formFields.bidType.toLowerCase() === 'open') {
			if (this.state.bidDesc === '') {
				this.setState({ bidDescErr: 'error' })
				isFormEmpty = false
			}
			if (this.state.bidQty === '') {
				this.setState({ bidQtyErr: 'error' })
				isFormEmpty = false
			}
			if (this.state.bidUnitPrice === '') {
				this.setState({ bidUnitPriceErr: 'error' })
				isFormEmpty = false
			}
		}

		if (isFormEmpty) {
			try {
				const formData = new FormData()

				if (this.state.formFields.bidType.toLowerCase() === 'open') {
					const reqBody = {
						'rfp_id': this.props.match.params.id,
						'bidDesc': this.state.bidDesc,
						'bidQty': this.state.bidQty,
						'bidUnitPrice': this.state.bidUnitPrice,
						'bidTotal': (this.state.bidQty * this.state.bidUnitPrice).toString()	
					}
					formData.append('data', JSON.stringify(reqBody))
				} else if (this.state.formFields.bidType.toLowerCase() === 'close') {
					const reqBody = {'rfp_id': this.props.match.params.id}
					formData.append('data', JSON.stringify(reqBody))
					formData.append('bidFile', this.state.bidDocument)
				}

				const reqURL = serverURL + '/rfp/bid'
				let userInfo = JSON.parse(localStorage.getItem('userInfo'))
				const options = {
					headers: { 
						'Access-Control-Allow-Origin': "*",
						'access_key': userInfo.access_key
					}
				}
				this.setState({ pleaseWait: true})
				axios.post(reqURL, formData, options).then(res => {
					// console.log('response', res.data)
					localStorage.setItem('isRFPSuccess', true)
					this.props.history.push('/vendor/dashboard')
				})
			} catch (error) {
				console.log('try catch failure')
			}
		} else {
			this.setState({ disablebtn: 'outer-btn' })
		}	
	}

	componentDidMount () {
		let userInfo = JSON.parse(localStorage.getItem('userInfo'))
    if (userInfo) {
      if (this.state.accountType !== 'vendor') this.props.history.push('/')
      else if (this.state.accountType === 'vendor') {
        this.setState({ profileName: userInfo.firstName + ' ' + userInfo.lastName })
        this.setState({ profileIcon: userInfo.firstName.charAt(0) + userInfo.lastName.charAt(0) })
      }
		} else this.props.history.push('/')
		
		try {
			const reqURL = serverURL + '/rfp/details/' + this.props.match.params.id
			const options = {
				headers: {
					'Access-Control-Allow-Origin': "*",
					'access_key': userInfo.access_key,
				}
			}
			axios.get(reqURL, options).then(res => {
				console.log('RK data ', res.data.data)
				let data = res.data.data
				if (data) {
					const formFieldsState = this.state.formFields
					formFieldsState['itemCode'] = data.itemCode
					formFieldsState['itemName'] = data.itemName
					formFieldsState['HSNCode'] = data.HSNCode
					formFieldsState['UNSPSCCode'] = data.UNSPSCCode
					formFieldsState['itemDesc'] = data.itemDesc
					formFieldsState['quantity'] = data.quantity
					formFieldsState['bidType'] = data.bidType

					this.refs.bidStartDate.state.inputValue = moment(new Date((parseInt(data.bidStartDate) * 1000))).format('DD/MM/YYYY')
					this.refs.bidEndDate.state.inputValue = moment(new Date((parseInt(data.bidEndDate) * 1000))).format('DD/MM/YYYY hh:mm A')
					if (data.bidType.toLowerCase() === 'open') {
						let vendorsList = this.state.vendorsList
						if (data.vendorsList.length > 0) {
							const resVendorsList = data.vendorsList.map((value, sidx) => {
								return { ...value }
							})
							this.setState({ vendorsList: resVendorsList })
						}
	
						if (Object.keys(data.bidDetails).length > 0) {
							const formFields = this.state
							formFields['bidDesc'] = data.bidDetails.bidDesc
							formFields['bidQty'] = data.bidDetails.bidQty
							formFields['bidUnitPrice'] = data.bidDetails.bidUnitPrice
							formFields['bidTotal'] = data.bidDetails.bidTotal
						}
					} else if (data.bidType.toLowerCase() === 'close') {
						this.setState({ uploadFilename: data.uploadFilename })
					}
					this.setState({ formFieldsState, rfpFile: data.rfpFile })
				}
			})
		} catch (error) {
			console.log('try catch failure')
		}
	}

	render() {
		// form fields
		let { itemCode, itemName, HSNCode, UNSPSCCode, itemDesc, quantity, bidType, shipAddress, shipCity, shipPincode, shipMobile, shipAltCont } = this.state.formFields
		// form errors
		let formErrors = this.state.formErrors 

		return (
			<div id='vrfp-det-form'>
				{/* Header */}
				<Header profileName={this.state.profileName} profileIcon={this.state.profileIcon} />

				<div className='rfp-container'>
					<div className='rfp-block'>
						<Link to='/vendor/dashboard'><img src='/assets/images/ic-back.png' /></Link>
						<h1 className='rfp-title'>RFP Details</h1>
						{/* RFP details block */}
						<div className='comp-det-block'>
							<div className='flex-box'>
								{/* Item Code */}
								<div className='input-container'>
									<div className='input-groups'>
										<label className='field-caption'>Item Code*</label>
										<div className='out-ex-size'>
											<input type='text' className={formErrors.itemCode.length > 0 ? 'form-field error' : 'form-field'} autoComplete='off' data-type='string' name='itemCode' value={itemCode} placeholder='Enter Item Code' onChange={this.handleInputChange} maxLength='20' disabled={this.state.isDisabled} />
											{formErrors.itemCode.length > 0 && <div className='error-title'>Please enter item code</div>}
										</div>
									</div>
								</div>
								{/* Item Name */}
								<div className='input-container'>
									<div className='input-groups'>
										<label className='field-caption'>Item Name*</label>
										<div className='out-ex-size'>
											<input type='text' className={formErrors.itemName.length > 0 ? 'form-field error' : 'form-field'} autoComplete='off' data-type='string' name='itemName' value={itemName} placeholder='Enter Item Name' onChange={this.handleInputChange} maxLength='30' disabled={this.state.isDisabled} />
											{formErrors.itemName.length > 0 && <div className='error-title'>Please enter item name</div>}
										</div>
									</div>
								</div>
							</div>
							{/* HSN Code */}
							<div className='input-container'>
								<div className='input-groups'>
									<label className='field-caption'>HSN Code*</label>
									<div className='out-ex-size'>
										<input type='text' className={formErrors.HSNCode.length > 0 ? 'form-field ex-size error' : 'form-field ex-size'} autoComplete='off' data-type='string' name='HSNCode' value={HSNCode} title={HSNCode} placeholder='Enter HSN code' onChange={this.handleInputChange} maxLength='20' disabled={this.state.isDisabled} />
										{formErrors.HSNCode.length > 0 && <div className='error-title'>Please enter HSNCode code</div>}
									</div>
								</div>
							</div>
							{/* UNBPSC Code */}
							<div className='input-container'>
								<div className='input-groups'>
									<label className='field-caption'>UNSPSC Code*</label>
									<div className='out-ex-size'>
										<input type='text' className={formErrors.UNSPSCCode.length > 0 ? 'form-field ex-size error' : 'form-field ex-size'} autoComplete='off' data-type='string' name='UNSPSCCode' value={UNSPSCCode} title={UNSPSCCode} placeholder='Enter UNSPSC code' onChange={this.handleInputChange} maxLength='20' disabled={this.state.isDisabled} />
										{formErrors.UNSPSCCode.length > 0 && <div className='error-title'>Please enter UNSPSC code</div>}
									</div>
								</div>
							</div>

							{/* Download RFP from uploaded by buyer while RFP creation */}
							{this.state.rfpFile.length > 0 
								? <div className='doc-title'>*Please click <span onClick={() => this.handleDownload('buyer')}>here</span> to download buyer proposal document.</div>
								: <>
									{/* Description */}
									<div className='input-container textarea-sec'>
										<div className='input-groups input-textarea'>
											<label className='field-caption note-filed'>Description*</label>
											<div className='out-ex-size'>
												<textarea className={formErrors.itemDesc.length > 0 ? 'cls-notes error' : 'cls-notes'} autoComplete='false' data-type='string' name='itemDesc' value={itemDesc} placeholder='Enter Description' onChange={this.handleInputChange} maxLength='250' readOnly={this.state.isDisabled} />
												{formErrors.itemDesc.length > 0 && <div className='error-title'>Please enter description</div>}
											</div>
										</div>
									</div>
									<div className='flex-box'>
										{/* Quantity  */}
										<div className='input-container'>
											<div className='input-groups'>
												<label className='field-caption'>Quantity*</label>
												<div className='out-ex-size'>
													<input type='text' className={formErrors.quantity.length > 0 ? 'form-field error' : 'form-field'} autoComplete='off' data-type='number' name='quantity' value={quantity} placeholder='Enter Quantity' onChange={this.handleInputChange} maxLength='10' disabled={this.state.isDisabled} />
													{formErrors.quantity.length > 0 && <div className='error-title'>{formErrors.quantity.length > 5 ? formErrors.quantity : 'Please enter quantity'}</div>}
												</div>
											</div>
										</div>
									</div>
								</>
							}
						</div>

						{/* Bidding details */}
						<h2 className='rfp-sub-title'>Bidding Details</h2>
						<div className='comp-det-block'>
							{/* Bid Type */}
							<div className='input-container radio-sec mar-bot'>
								<div className='input-groups radio-field'>
									<label className='field-caption no-pad'>Bid Type*</label>
									<div className='radio-input-section'>
										{bidType === 'open' && <label className='radio-container' onClick={() => this.handleBidTypeOnChange('open')}><input type='radio' className='form-field-radio' value='open' name='bidType' checked={true} />Open<span className={'checkmark ' + formErrors.bidType} /></label>}
										{bidType === 'close' && <label className='radio-container' onClick={() => this.handleBidTypeOnChange('close')}><input type='radio' className='form-field-radio' value='close' name='bidType' checked={true} />Close<span className={'checkmark ' + formErrors.bidType} /></label>}
									</div>
								</div>
							</div>
							<div className='out-radio-ex-size'>
								{formErrors.bidType.length > 0 && <div className='error-title'>Please choose bit type</div>}
							</div>
							<div className='flex-box'>
								{/* Bid start date */}
								<div className='input-container'>
									<div className='input-groups'>
										<label className='field-caption'>Bid Start Date*</label>
										<div className='out-ex-size'>
											<Datetime
												ref='bidStartDate'
												dateFormat={'DD/MM/YYYY'}
												// timeFormat={false}
												className='date form-field'
												name='bidStartDate'
												defaultValue={moment(new Date())}
												closeOnSelect
												closeOnTab
												inputProps={{ placeholder: 'DD/MM/YYYY', disabled: this.state.isDisabled}}
												onChange={this.handleDateChange} />
										</div>
									</div>
								</div>
								{/* Bid Close date */}
								<div className='input-container'>
									<div className='input-groups'>
										<label className='field-caption'>Bid Close Date &amp; Time*</label>
										<div className='out-ex-size'>
											<Datetime
												ref='bidEndDate'
												dateFormat={'DD/MM/YYYY'}
												className={formErrors.bidEndDate.length > 0 ? 'date form-field error' : 'date form-field'}
												name='bidEndDate'
												defaultValue={moment(new Date())}
												closeOnSelect
												closeOnTab
												inputProps={{ placeholder: 'DD/MM/YYYY', readOnly: true, disabled: this.state.isDisabled}}
												onChange={this.handleDateChange} />
											{formErrors.bidEndDate.length > 0 && <div className='error-title'>Please select bid close date &amp; time</div>}
										</div>
									</div>
								</div>
							</div>
							{bidType.toLowerCase() === 'open' &&
								<Fragment>							
									{/* Description */}
									<div className='input-container textarea-sec bdr-tp-divider'>
										<div className='input-groups input-textarea'>
											<label className='field-caption note-filed'>Description*</label>
											<div className='out-ex-size'>
												<textarea className={this.state.bidDescErr.length > 0 ? 'cls-notes error' : 'cls-notes'} autoComplete='false' data-type='string' name='bidDesc' value={this.state.bidDesc} placeholder='Enter Description' onChange={this.handleInputChange} maxLength='250' />
												{this.state.bidDescErr.length > 0 && <div className='error-title'>Please enter description</div>}
											</div>
										</div>
									</div>
									<div className='flex-box'>
										{/* Quantity */}
										<div className='input-container'>
											<div className='input-groups'>
												<label className='field-caption'>Quantity*</label>
												<div className='out-ex-size'>
													<input type='text' className={this.state.bidQtyErr.length > 0 ? 'form-field error' : 'form-field'} autoComplete='off' data-type='number' name='bidQty' value={this.state.bidQty} placeholder='Enter Quantity' onChange={this.handleInputChange} maxLength='10' />
													{this.state.bidQtyErr.length > 0 && <div className='error-title'>Please enter quantity</div>}
												</div>
											</div>
										</div>
										{/* Unit Price */}
										<div className='input-container'>
											<div className='input-groups'>
												<label className='field-caption'>Unit Price (in dollars)*</label>
												<div className='out-ex-size'>
													<input type='text' className={this.state.bidUnitPriceErr.length > 0 ? 'form-field before error' : 'form-field before'} autoComplete='off' data-type='number' name='bidUnitPrice' value={this.state.bidUnitPrice} placeholder='Enter unit price' onChange={this.handleInputChange} maxLength='10' />
													{this.state.bidUnitPriceErr.length > 0 && <div className='error-title'>Please enter unit price</div>}
												</div>
											</div>
										</div>
									</div>
									{/* Total Price */}
									<div className='input-container'>
										<div className='input-groups'>
											<label className='field-caption'>Total Price*</label>
											<div className='out-ex-size'>
												<input type='text' className='form-field' autoComplete='off' value={'$' + this.state.bidQty * this.state.bidUnitPrice} placeholder='$0' maxLength='10' disabled={this.state.isDisabled} />
											</div>
										</div>
									</div>
									{/* Bidding vendors details */}
									<h3 className='bid-vendor-title'>Vendor's Details</h3>

									{/* Table content */}
									<div className='db-tbl-block'>
										{/* Table header */}
										<div className='tbl-header-block'>
											<div className='tbl-head col-11'>Sl.No</div>
											<div className='tbl-head col-22'>Vendor Name</div>
											<div className='tbl-head col-33'>Description</div>
											<div className='tbl-head col-44'>Quantity</div>
											<div className='tbl-head col-55'>Unit Price</div>
											<div className='tbl-head col-66'>Total</div>
										</div>
										{/* Table body */}
										<div className='tbl-body-container'>
											{this.state.vendorsList.length === 0
											? <div className='tbl-empty-row'>Vendors data not found</div>
											:	this.state.vendorsList.map((value, idx) => (
													<div className='tbl-body-block' key={idx}>
														<div className='tbl-body col-11'>{idx+1}</div>
														<div className='tbl-body col-22'>{value.name + (idx+1)}</div>
														<div className='tbl-body col-33'>{value.bidDesc}</div>
														<div className='tbl-body col-44'>{value.bidQty}</div>
														<div className='tbl-body col-55'>${value.bidUnitPrice}</div>
														<div className='tbl-body col-66'>${value.bidTotal}</div>
													</div>
												))
											}
										</div>
									</div>											
								</Fragment>
							}

							{!this.state.isHide &&
								// {/* Bidding vendors details */}
								<h3 className='bid-vendor-title'>Kindly submit the copy of the following documents along with this form:</h3>
							}

							{!this.state.isHide &&
								// {/* Table content */}
								<div className='db-tbl-block'>
									{/* Table header */}
									<div className='tbl-header-block'>
										<div className='tbl-head col-1'>Sl.No</div>
										<div className='tbl-head col-2'>Vendor Name</div>
										<div className='tbl-head col-3'>Vendor Email</div>
										<div className='tbl-head col-4'>Compare</div>
									</div>
									{/* Table body */}
									<div className='tbl-body-container'>
										{this.state.vendorsList.map((value, idx) => (
											<div className='tbl-body-block'>
												<div className='tbl-body col-1'>{idx+1}</div>
												<div className='tbl-body col-2'>{value.name}</div>
												<div className='tbl-body col-3'>{value.email}</div>
												<div className='tbl-body col-4'>
													<label class='chk-box-block'>
														<input type='checkbox' checked={value.isChecked} />
														<span class='checkbox-checkmark' onClick={(e) => this.handleCheckBox(e, idx)}>&nbsp;</span>
													</label>
												</div>
											</div>
										))}
									</div>
								</div>
							}

							{!this.state.isHide &&
								// {/* Bid to be send */}
								<div className='input-container'>
									<div className='input-groups'>
										<label className='field-caption'>Select Vendor's*</label>
										<div className='out-ex-size'>
											<Multiselect
												options={this.state.vendorsList} // Options to display in the dropdown
												selectedValues={this.state.selectedValue} // Preselected value to persist in dropdown
												onSelect={this.onSelectVendorsFromList} // Function will trigger on select event
												onRemove={this.onRemoveVendorsFromList} // Function will trigger on remove event
												displayValue='name' // Property name to display in the dropdown options
												showCheckbox={true}
												avoidHighlightFirstOption={true}
												style={
													{ multiselectContainer: { width: '102%', fontFamily: 'OpenSans-Regular', fontSize: '14px', lineHeight: '19px', letterSpacing: '.2px' },
														searchBox: { border: formErrors.oldVendorsList.length > 0 ? '1px solid rgba(255,0,0,0.35)' : '1px solid #e3e3e3', borderRadius: '2px', padding: '10px' },
														chips: { margin: '0px 5px 0px 0px', background: '#cac6c6' },
														inputField: { fontFamily: 'OpenSans-Regular', fontSize: '14px', lineHeight: '19px', letterSpacing: '.2px', margin: '0px' },
														option: { fontFamily: 'OpenSans-Regular', fontSize: '14px', lineHeight: '19px', letterSpacing: '.2px', color: 'rgba(0,0,0,0.8)', display: 'flex', alignItems: 'center' }
													}
												}
											/>
											{formErrors.oldVendorsList.length > 0 && <div className='error-title'>{formErrors.oldVendorsList.length > 5 ? formErrors.oldVendorsList : 'Please select vendors from list'}</div>}
										</div>
									</div>
								</div>
							}

							{!this.state.isHide &&
								// {/* New Vendor Email */}
								this.state.newVendorsList.map((value, idx) => (
									<div className='input-container'>
										<div className='input-groups'>
											<label className='field-caption'>New Vendor Email*</label>
											<div className='out-ex-size no-wrap'>
												<input type='text' className={value.newVendorEmailError.length > 0 ? 'form-field new-ven-ex-size error' : 'form-field new-ven-ex-size'} autoComplete='off' data-type='email' name='newVendorEmail' value={value.newVendorEmail} placeholder='Enter New Vendor Email Address' onChange={(e) => this.handleNewVendorInputChange(e, idx)} onBlur={(e) => this.handleNewVendorOnBlurChange(e, idx)} maxLength='50' /><span className='close-icon' onClick={(e) => this.handleRemoveNewVendor(e, idx)}>X</span>
												{value.newVendorEmailError.length > 0 && <div className='error-title'>{value.newVendorEmailError.length > 5 ? value.newVendorEmailError : 'Please enter vendor address'}</div>}
											</div>
										</div>
									</div>
								))
							}

							{!this.state.isHide &&
								// {/* Add Button */}
								<div className='form-submit-section btn-right'>
									<div className='outer-btn btn-add'>
										<div className='btn register-booking' onClick={this.handleAddNewVendor}>DOWNLOAD CSV</div>
									</div>
								</div>
							}
						</div>

						{!this.state.isHide &&
							// {/* Shipping details */}
							// <h2 className='rfp-sub-title'>Shipping Details</h2>
							<div className='comp-det-block'>
								{/* Address */}
								<div className='input-container textarea-sec'>
									<div className='input-groups input-textarea'>
										<label className='field-caption note-filed'>Shipping Address*</label>
										<div className='out-ex-size'>
											<textarea className={formErrors.shipAddress.length > 0 ? 'cls-notes error' : 'cls-notes'} autoComplete='false' data-type='string' name='shipAddress' value={shipAddress} placeholder='Enter Shipping Address' onChange={this.handleInputChange} maxLength='250' />
											{formErrors.shipAddress.length > 0 && <div className='error-title'>Please enter shipping address</div>}
										</div>
									</div>
								</div>
								<div className='flex-box'>
									{/* City */}
									<div className='input-container'>
										<div className='input-groups'>
											<label className='field-caption'>City*</label>
											<div className='out-ex-size'>
												<input type='text' className={formErrors.shipCity.length > 0 ? 'form-field error' : 'form-field'} autoComplete='off' data-type='city' name='shipCity' value={shipCity} placeholder='Enter City' onChange={this.handleInputChange} maxLength='30' />
												{formErrors.shipCity.length > 0 && <div className='error-title err-width'>Please enter city</div>}
											</div>
										</div>
									</div>
									{/* Pincode */}
									<div className='input-container'>
										<div className='input-groups'>
											<label className='field-caption'>Pincode*</label>
											<div className='out-ex-size'>
												<input type='text' className={formErrors.shipPincode.length > 0 ? 'form-field error' : 'form-field'} autoComplete='off' data-type='pincode' name='shipPincode' value={shipPincode} placeholder='Enter Pincode' onChange={this.handleInputChange} maxLength='10' />
												{formErrors.shipPincode.length > 0 && <div className='error-title err-width'>Please enter pincode</div>}
											</div>
										</div>
									</div>
								</div>
								<div className='flex-box'>
									{/* Mobile */}
									<div className='input-container'>
										<div className='input-groups'>
											<label className='field-caption'>Mobile*</label>
											<div className='out-ex-size'>
												<input type='text' className={formErrors.shipMobile.length > 0 ? 'form-field error' : 'form-field'} autoComplete='off' data-type='mobile' name='shipMobile' value={shipMobile} placeholder='Enter Mobile Number' onChange={this.handleInputChange} onBlur={this.handleInputFocusExit} maxLength='15' />
												{formErrors.shipMobile.length > 0 && <div className='error-title err-width'>{formErrors.shipMobile.length > 5 ? formErrors.shipMobile : 'Please enter mobile number'}</div>}
											</div>
										</div>
									</div>
									{/* Alternate Contact Number */}
									<div className='input-container'>
										<div className='input-groups'>
											<label className='field-caption'>Alternate Contact No.</label>
											<div className='out-ex-size'>
												<input type='text' className={formErrors.shipAltCont.length > 5 ? 'form-field error' : 'form-field'} autoComplete='off' data-type='mobile' name='shipAltCont' value={shipAltCont} placeholder='Enter Alternate Contact Number' onChange={this.handleInputChange} onBlur={this.handleInputFocusExit} maxLength='15' />
												{formErrors.shipAltCont.length > 5 && <div className='error-title err-width'>{formErrors.shipAltCont}</div>}
											</div>
										</div>
									</div>
								</div>
							</div>
						}
						
						{/* self-attested  block */}
						{bidType.toLowerCase() === 'close' &&
							<Fragment>
								<div className='self-attest-title'>Kindly upload and submit the copy of bid proposal documents:</div>
								<div className='doc-flex-box'>
									<div className='self-attest-block'>
										{/* Bid proposal Document */}
										<div className='input-container no-mar'>
											<div className='input-groups'>
												<label className='field-caption ex-size no-pad'>1. Bid Proposal Document*</label>
												<input type='file' className='cls-file-upload' name='bidDocument' onChange={this.handleFileUpload} />
											</div>
										</div>
									</div>
									{this.state.uploadFilename.length > 0 &&
										<div className='doc-title'>*Please click <span onClick={() => this.handleDownload('vendor')}>here</span> to download bid proposal document already uploaded by you.</div>
									}
									</div>
							</Fragment>
						}

						{/* Error message */}
						<div className='cls-err-msg'>{this.state.errMsg}&nbsp;</div>
						{/* Please wait message */}
						{this.state.pleaseWait && <div className='cls-success-msg'>Please wait...</div>}
						{/* Submit button */}
						<div className='form-submit-section'>
							<div className={this.state.disablebtn}>
								<div className='btn register-booking' onClick={this.handleSubmit}>SUBMIT</div>
							</div>
						</div>						
					</div>
				</div>
			</div>
		)
	}
}
export default withRouter(VendorRFPDetails)
