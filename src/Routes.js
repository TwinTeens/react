import React, { Suspense } from 'react'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'

import Home from './components/Home/Home'
import VendorRegForm from './components/VendorRegForm/VendorRegForm'
import RFPForm from './components/RFPForm/RFPForm'
import BuyerDashboard from './components/BuyerDashboard/BuyerDashboard'
import VendorDashboard from './components/VendorDashboard/VendorDashboard'
import BuyerRFPDetails from './components/BuyerRFPDetails/BuyerRFPDetails'
import VendorRFPDetails from './components/VendorRFPDetails/VendorRFPDetails'
import Login from './components/Login/Login'
import Signup from './components/Signup/Signup'
import ChangePassword from './components/ChangePassword/ChangePassword'
import ForgetPassword from './components/ForgetPassword/ForgetPassword'
import ResetPassword from './components/ResetPassword/ResetPassword'
import Thankyou from './components/Thankyou/Thankyou'
import MailConfirmation from './components/MailConfirmation/MailConfirmation'
import Notfound from './NotFound/NotFound.js'

const Routes = props => {
  return (
    <Router>
      <Suspense fallback={<div>Loading...</div>}>
        <Switch>
          <Route exact path='/' component={Home} />
          <Route exact path='/login' component={Login} />
          <Route exact path='/signup' component={Signup} />
          <Route exact path='/change-password' component={ChangePassword} />
          <Route exact path='/forget-password' component={ForgetPassword} />
          <Route exact path='/reset-password/:id' component={ResetPassword} />

          {/* Buyer Routes */}
          <Route exact path='/buyer/dashboard' component={BuyerDashboard} />
          <Route exact path='/buyer/rfp-form' component={RFPForm} />
          <Route exact path='/buyer/bid-info' component={BuyerRFPDetails} />

          {/* Vendor Routes */}
          <Route exact path='/vendor/dashboard' component={VendorDashboard} />
          <Route exact path='/vendor/registration' component={VendorRegForm} />
          <Route exact path='/vendor/rfp/:id' component={VendorRFPDetails} />
          
          <Route exact path='/thank-you' component={Thankyou} />
          <Route exact path='/confirmation/:id' component={MailConfirmation} />
          <Route path='*' exact component={Notfound} />
        </Switch>
      </Suspense>
    </Router>
  )
}

export default Routes
